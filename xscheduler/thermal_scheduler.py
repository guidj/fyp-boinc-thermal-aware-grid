#!/usr/bin/python

import sys

n = len(sys.argv)

if (n != 2):
	print("""Bad usage. 
		program outputfile
	 	""")
	quit()

filename = sys.argv[1]

f = open(filename, 'w')

f.write(
"""<xs_output>
</xs_output>
""")

f.close()

quit()
