################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../mac_installer/AddRemoveUser.cpp \
../mac_installer/CustomInstall.cpp \
../mac_installer/Installer.cpp \
../mac_installer/PostInstall.cpp \
../mac_installer/WaitPermissions.cpp \
../mac_installer/uninstall.cpp 

C_SRCS += \
../mac_installer/LoginItemAPI.c 

OBJS += \
./mac_installer/AddRemoveUser.o \
./mac_installer/CustomInstall.o \
./mac_installer/Installer.o \
./mac_installer/LoginItemAPI.o \
./mac_installer/PostInstall.o \
./mac_installer/WaitPermissions.o \
./mac_installer/uninstall.o 

C_DEPS += \
./mac_installer/LoginItemAPI.d 

CPP_DEPS += \
./mac_installer/AddRemoveUser.d \
./mac_installer/CustomInstall.d \
./mac_installer/Installer.d \
./mac_installer/PostInstall.d \
./mac_installer/WaitPermissions.d \
./mac_installer/uninstall.d 


# Each subdirectory must supply rules for building sources it contributes
mac_installer/%.o: ../mac_installer/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

mac_installer/%.o: ../mac_installer/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


