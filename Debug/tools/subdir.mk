################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../tools/cancel_jobs.o \
../tools/create_work.o \
../tools/dir_hier_move.o \
../tools/dir_hier_path.o \
../tools/sign_executable.o 

CPP_SRCS += \
../tools/backend_lib.cpp \
../tools/cancel_jobs.cpp \
../tools/create_work.cpp \
../tools/dir_hier_move.cpp \
../tools/dir_hier_path.cpp \
../tools/hr_db_convert.cpp \
../tools/kill_wu.cpp \
../tools/poll_wu.cpp \
../tools/process_input_template.cpp \
../tools/process_result_template.cpp \
../tools/sign_executable.cpp \
../tools/updater.cpp 

OBJS += \
./tools/backend_lib.o \
./tools/cancel_jobs.o \
./tools/create_work.o \
./tools/dir_hier_move.o \
./tools/dir_hier_path.o \
./tools/hr_db_convert.o \
./tools/kill_wu.o \
./tools/poll_wu.o \
./tools/process_input_template.o \
./tools/process_result_template.o \
./tools/sign_executable.o \
./tools/updater.o 

CPP_DEPS += \
./tools/backend_lib.d \
./tools/cancel_jobs.d \
./tools/create_work.d \
./tools/dir_hier_move.d \
./tools/dir_hier_path.d \
./tools/hr_db_convert.d \
./tools/kill_wu.d \
./tools/poll_wu.d \
./tools/process_input_template.d \
./tools/process_result_template.d \
./tools/sign_executable.d \
./tools/updater.d 


# Each subdirectory must supply rules for building sources it contributes
tools/%.o: ../tools/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


