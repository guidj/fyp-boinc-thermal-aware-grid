################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../sched/assimilator.o \
../sched/census.o \
../sched/credit.o \
../sched/credit_test.o \
../sched/db_dump.o \
../sched/db_purge.o \
../sched/delete_file.o \
../sched/edf_sim.o \
../sched/feeder.o \
../sched/file_deleter.o \
../sched/file_upload_handler.o \
../sched/get_file.o \
../sched/handle_request.o \
../sched/hr.o \
../sched/hr_info.o \
../sched/libsched_la-backend_lib.o \
../sched/libsched_la-boinc_db.o \
../sched/libsched_la-credit.o \
../sched/libsched_la-db_base.o \
../sched/libsched_la-process_input_template.o \
../sched/libsched_la-process_result_template.o \
../sched/libsched_la-sched_config.o \
../sched/libsched_la-sched_limit.o \
../sched/libsched_la-sched_msgs.o \
../sched/libsched_la-sched_shmem.o \
../sched/libsched_la-sched_util.o \
../sched/make_work.o \
../sched/message_handler.o \
../sched/put_file.o \
../sched/sample_assimilator.o \
../sched/sample_bitwise_validator.o \
../sched/sample_dummy_assimilator.o \
../sched/sample_trivial_validator.o \
../sched/sample_work_generator.o \
../sched/sched_array.o \
../sched/sched_assign.o \
../sched/sched_customize.o \
../sched/sched_driver.o \
../sched/sched_hr.o \
../sched/sched_limit.o \
../sched/sched_locality.o \
../sched/sched_main.o \
../sched/sched_resend.o \
../sched/sched_result.o \
../sched/sched_score.o \
../sched/sched_send.o \
../sched/sched_timezone.o \
../sched/sched_types.o \
../sched/sched_version.o \
../sched/show_shmem.o \
../sched/single_job_assimilator.o \
../sched/synch.o \
../sched/time_stats_log.o \
../sched/transitioner.o \
../sched/trickle_credit.o \
../sched/trickle_echo.o \
../sched/trickle_handler.o \
../sched/update_stats.o \
../sched/validate_util.o \
../sched/validate_util2.o \
../sched/validator.o \
../sched/wu_check.o 

CPP_SRCS += \
../sched/assimilator.cpp \
../sched/census.cpp \
../sched/credit.cpp \
../sched/credit_test.cpp \
../sched/db_dump.cpp \
../sched/db_purge.cpp \
../sched/delete_file.cpp \
../sched/edf_sim.cpp \
../sched/feeder.cpp \
../sched/file_deleter.cpp \
../sched/file_upload_handler.cpp \
../sched/get_file.cpp \
../sched/handle_request.cpp \
../sched/hr.cpp \
../sched/hr_info.cpp \
../sched/make_work.cpp \
../sched/message_handler.cpp \
../sched/put_file.cpp \
../sched/sample_assimilator.cpp \
../sched/sample_bitwise_validator.cpp \
../sched/sample_dummy_assimilator.cpp \
../sched/sample_trivial_validator.cpp \
../sched/sample_work_generator.cpp \
../sched/sched_array.cpp \
../sched/sched_assign.cpp \
../sched/sched_config.cpp \
../sched/sched_customize.cpp \
../sched/sched_driver.cpp \
../sched/sched_hr.cpp \
../sched/sched_limit.cpp \
../sched/sched_locality.cpp \
../sched/sched_main.cpp \
../sched/sched_msgs.cpp \
../sched/sched_resend.cpp \
../sched/sched_result.cpp \
../sched/sched_score.cpp \
../sched/sched_send.cpp \
../sched/sched_shmem.cpp \
../sched/sched_timezone.cpp \
../sched/sched_types.cpp \
../sched/sched_util.cpp \
../sched/sched_version.cpp \
../sched/show_shmem.cpp \
../sched/single_job_assimilator.cpp \
../sched/time_stats_log.cpp \
../sched/transitioner.cpp \
../sched/trickle_credit.cpp \
../sched/trickle_echo.cpp \
../sched/trickle_handler.cpp \
../sched/update_stats.cpp \
../sched/validate_util.cpp \
../sched/validate_util2.cpp \
../sched/validator.cpp \
../sched/wu_check.cpp 

OBJS += \
./sched/assimilator.o \
./sched/census.o \
./sched/credit.o \
./sched/credit_test.o \
./sched/db_dump.o \
./sched/db_purge.o \
./sched/delete_file.o \
./sched/edf_sim.o \
./sched/feeder.o \
./sched/file_deleter.o \
./sched/file_upload_handler.o \
./sched/get_file.o \
./sched/handle_request.o \
./sched/hr.o \
./sched/hr_info.o \
./sched/make_work.o \
./sched/message_handler.o \
./sched/put_file.o \
./sched/sample_assimilator.o \
./sched/sample_bitwise_validator.o \
./sched/sample_dummy_assimilator.o \
./sched/sample_trivial_validator.o \
./sched/sample_work_generator.o \
./sched/sched_array.o \
./sched/sched_assign.o \
./sched/sched_config.o \
./sched/sched_customize.o \
./sched/sched_driver.o \
./sched/sched_hr.o \
./sched/sched_limit.o \
./sched/sched_locality.o \
./sched/sched_main.o \
./sched/sched_msgs.o \
./sched/sched_resend.o \
./sched/sched_result.o \
./sched/sched_score.o \
./sched/sched_send.o \
./sched/sched_shmem.o \
./sched/sched_timezone.o \
./sched/sched_types.o \
./sched/sched_util.o \
./sched/sched_version.o \
./sched/show_shmem.o \
./sched/single_job_assimilator.o \
./sched/time_stats_log.o \
./sched/transitioner.o \
./sched/trickle_credit.o \
./sched/trickle_echo.o \
./sched/trickle_handler.o \
./sched/update_stats.o \
./sched/validate_util.o \
./sched/validate_util2.o \
./sched/validator.o \
./sched/wu_check.o 

CPP_DEPS += \
./sched/assimilator.d \
./sched/census.d \
./sched/credit.d \
./sched/credit_test.d \
./sched/db_dump.d \
./sched/db_purge.d \
./sched/delete_file.d \
./sched/edf_sim.d \
./sched/feeder.d \
./sched/file_deleter.d \
./sched/file_upload_handler.d \
./sched/get_file.d \
./sched/handle_request.d \
./sched/hr.d \
./sched/hr_info.d \
./sched/make_work.d \
./sched/message_handler.d \
./sched/put_file.d \
./sched/sample_assimilator.d \
./sched/sample_bitwise_validator.d \
./sched/sample_dummy_assimilator.d \
./sched/sample_trivial_validator.d \
./sched/sample_work_generator.d \
./sched/sched_array.d \
./sched/sched_assign.d \
./sched/sched_config.d \
./sched/sched_customize.d \
./sched/sched_driver.d \
./sched/sched_hr.d \
./sched/sched_limit.d \
./sched/sched_locality.d \
./sched/sched_main.d \
./sched/sched_msgs.d \
./sched/sched_resend.d \
./sched/sched_result.d \
./sched/sched_score.d \
./sched/sched_send.d \
./sched/sched_shmem.d \
./sched/sched_timezone.d \
./sched/sched_types.d \
./sched/sched_util.d \
./sched/sched_version.d \
./sched/show_shmem.d \
./sched/single_job_assimilator.d \
./sched/time_stats_log.d \
./sched/transitioner.d \
./sched/trickle_credit.d \
./sched/trickle_echo.d \
./sched/trickle_handler.d \
./sched/update_stats.d \
./sched/validate_util.d \
./sched/validate_util2.d \
./sched/validator.d \
./sched/wu_check.d 


# Each subdirectory must supply rules for building sources it contributes
sched/%.o: ../sched/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


