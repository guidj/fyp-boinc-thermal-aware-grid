################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../clientgui/mac/MacAccessiblity.cpp \
../clientgui/mac/MacBitmapComboBox.cpp \
../clientgui/mac/MacSysMenu.cpp \
../clientgui/mac/Mac_GUI.cpp \
../clientgui/mac/SecurityUtility.cpp \
../clientgui/mac/SetVersion.cpp \
../clientgui/mac/SetupSecurity.cpp 

OBJS += \
./clientgui/mac/MacAccessiblity.o \
./clientgui/mac/MacBitmapComboBox.o \
./clientgui/mac/MacSysMenu.o \
./clientgui/mac/Mac_GUI.o \
./clientgui/mac/SecurityUtility.o \
./clientgui/mac/SetVersion.o \
./clientgui/mac/SetupSecurity.o 

CPP_DEPS += \
./clientgui/mac/MacAccessiblity.d \
./clientgui/mac/MacBitmapComboBox.d \
./clientgui/mac/MacSysMenu.d \
./clientgui/mac/Mac_GUI.d \
./clientgui/mac/SecurityUtility.d \
./clientgui/mac/SetVersion.d \
./clientgui/mac/SetupSecurity.d 


# Each subdirectory must supply rules for building sources it contributes
clientgui/mac/%.o: ../clientgui/mac/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


