################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../clientgui/boincmgr-AccountInfoPage.o \
../clientgui/boincmgr-AccountManagerInfoPage.o \
../clientgui/boincmgr-AccountManagerProcessingPage.o \
../clientgui/boincmgr-AccountManagerPropertiesPage.o \
../clientgui/boincmgr-AdvancedFrame.o \
../clientgui/boincmgr-AlreadyExistsPage.o \
../clientgui/boincmgr-AsyncRPC.o \
../clientgui/boincmgr-BOINCBaseFrame.o \
../clientgui/boincmgr-BOINCBaseView.o \
../clientgui/boincmgr-BOINCBaseWizard.o \
../clientgui/boincmgr-BOINCBitmapComboBox.o \
../clientgui/boincmgr-BOINCClientManager.o \
../clientgui/boincmgr-BOINCDialupManager.o \
../clientgui/boincmgr-BOINCGUIApp.o \
../clientgui/boincmgr-BOINCHtmlLBox.o \
../clientgui/boincmgr-BOINCInternetFSHandler.o \
../clientgui/boincmgr-BOINCListCtrl.o \
../clientgui/boincmgr-BOINCTaskBar.o \
../clientgui/boincmgr-BOINCTaskCtrl.o \
../clientgui/boincmgr-BOINCVListBox.o \
../clientgui/boincmgr-CompletionErrorPage.o \
../clientgui/boincmgr-CompletionPage.o \
../clientgui/boincmgr-DlgAbout.o \
../clientgui/boincmgr-DlgAdvPreferences.o \
../clientgui/boincmgr-DlgAdvPreferencesBase.o \
../clientgui/boincmgr-DlgEventLog.o \
../clientgui/boincmgr-DlgEventLogListCtrl.o \
../clientgui/boincmgr-DlgExitMessage.o \
../clientgui/boincmgr-DlgGenericMessage.o \
../clientgui/boincmgr-DlgItemProperties.o \
../clientgui/boincmgr-DlgOptions.o \
../clientgui/boincmgr-DlgSelectComputer.o \
../clientgui/boincmgr-LogBOINC.o \
../clientgui/boincmgr-MainDocument.o \
../clientgui/boincmgr-NoInternetConnectionPage.o \
../clientgui/boincmgr-NotDetectedPage.o \
../clientgui/boincmgr-NotFoundPage.o \
../clientgui/boincmgr-NoticeListCtrl.o \
../clientgui/boincmgr-ProjectInfoPage.o \
../clientgui/boincmgr-ProjectListCtrl.o \
../clientgui/boincmgr-ProjectProcessingPage.o \
../clientgui/boincmgr-ProjectPropertiesPage.o \
../clientgui/boincmgr-ProxyInfoPage.o \
../clientgui/boincmgr-ProxyPage.o \
../clientgui/boincmgr-SkinManager.o \
../clientgui/boincmgr-TermsOfUsePage.o \
../clientgui/boincmgr-UnavailablePage.o \
../clientgui/boincmgr-ValidateAccountKey.o \
../clientgui/boincmgr-ValidateEmailAddress.o \
../clientgui/boincmgr-ValidateURL.o \
../clientgui/boincmgr-ViewNotices.o \
../clientgui/boincmgr-ViewProjects.o \
../clientgui/boincmgr-ViewResources.o \
../clientgui/boincmgr-ViewStatistics.o \
../clientgui/boincmgr-ViewTransfers.o \
../clientgui/boincmgr-ViewWork.o \
../clientgui/boincmgr-WelcomePage.o \
../clientgui/boincmgr-WizardAttach.o \
../clientgui/boincmgr-browser.o \
../clientgui/boincmgr-sg_BoincSimpleFrame.o \
../clientgui/boincmgr-sg_CustomControls.o \
../clientgui/boincmgr-sg_DlgMessages.o \
../clientgui/boincmgr-sg_DlgPreferences.o \
../clientgui/boincmgr-sg_PanelBase.o \
../clientgui/boincmgr-sg_ProjectCommandPopup.o \
../clientgui/boincmgr-sg_ProjectPanel.o \
../clientgui/boincmgr-sg_ProjectWebSitesPopup.o \
../clientgui/boincmgr-sg_TaskCommandPopup.o \
../clientgui/boincmgr-sg_TaskPanel.o \
../clientgui/boincmgr-stdwx.o \
../clientgui/boincmgr-taskbarex.o \
../clientgui/boincmgr-wizardex.o \
../clientgui/boincmgr-wxPieCtrl.o 

CPP_SRCS += \
../clientgui/AccountInfoPage.cpp \
../clientgui/AccountManagerInfoPage.cpp \
../clientgui/AccountManagerProcessingPage.cpp \
../clientgui/AccountManagerPropertiesPage.cpp \
../clientgui/AdvancedFrame.cpp \
../clientgui/AlreadyExistsPage.cpp \
../clientgui/AsyncRPC.cpp \
../clientgui/BOINCBaseFrame.cpp \
../clientgui/BOINCBaseView.cpp \
../clientgui/BOINCBaseWizard.cpp \
../clientgui/BOINCBitmapComboBox.cpp \
../clientgui/BOINCClientManager.cpp \
../clientgui/BOINCDialupManager.cpp \
../clientgui/BOINCGUIApp.cpp \
../clientgui/BOINCHtmlLBox.cpp \
../clientgui/BOINCInternetFSHandler.cpp \
../clientgui/BOINCListCtrl.cpp \
../clientgui/BOINCTaskBar.cpp \
../clientgui/BOINCTaskCtrl.cpp \
../clientgui/BOINCVListBox.cpp \
../clientgui/CompletionErrorPage.cpp \
../clientgui/CompletionPage.cpp \
../clientgui/DlgAbout.cpp \
../clientgui/DlgAdvPreferences.cpp \
../clientgui/DlgAdvPreferencesBase.cpp \
../clientgui/DlgEventLog.cpp \
../clientgui/DlgEventLogListCtrl.cpp \
../clientgui/DlgExitMessage.cpp \
../clientgui/DlgGenericMessage.cpp \
../clientgui/DlgItemProperties.cpp \
../clientgui/DlgOptions.cpp \
../clientgui/DlgSelectComputer.cpp \
../clientgui/Localization.cpp \
../clientgui/LogBOINC.cpp \
../clientgui/MainDocument.cpp \
../clientgui/NoInternetConnectionPage.cpp \
../clientgui/NotDetectedPage.cpp \
../clientgui/NotFoundPage.cpp \
../clientgui/NoticeListCtrl.cpp \
../clientgui/ProjectInfoPage.cpp \
../clientgui/ProjectListCtrl.cpp \
../clientgui/ProjectProcessingPage.cpp \
../clientgui/ProjectPropertiesPage.cpp \
../clientgui/ProxyInfoPage.cpp \
../clientgui/ProxyPage.cpp \
../clientgui/SkinManager.cpp \
../clientgui/TermsOfUsePage.cpp \
../clientgui/UnavailablePage.cpp \
../clientgui/ValidateAccountKey.cpp \
../clientgui/ValidateEmailAddress.cpp \
../clientgui/ValidateURL.cpp \
../clientgui/ViewMessages.cpp \
../clientgui/ViewNotices.cpp \
../clientgui/ViewProjects.cpp \
../clientgui/ViewResources.cpp \
../clientgui/ViewStatistics.cpp \
../clientgui/ViewTransfers.cpp \
../clientgui/ViewWork.cpp \
../clientgui/WelcomePage.cpp \
../clientgui/WizardAttach.cpp \
../clientgui/browser.cpp \
../clientgui/sg_BoincSimpleFrame.cpp \
../clientgui/sg_BoincSimpleGUI.cpp \
../clientgui/sg_ClientStateIndicator.cpp \
../clientgui/sg_CustomControls.cpp \
../clientgui/sg_DlgMessages.cpp \
../clientgui/sg_DlgPreferences.cpp \
../clientgui/sg_ImageButton.cpp \
../clientgui/sg_ImageLoader.cpp \
../clientgui/sg_PanelBase.cpp \
../clientgui/sg_ProgressBar.cpp \
../clientgui/sg_ProjectCommandPopup.cpp \
../clientgui/sg_ProjectPanel.cpp \
../clientgui/sg_ProjectWebSitesPopup.cpp \
../clientgui/sg_ProjectsComponent.cpp \
../clientgui/sg_StatImageLoader.cpp \
../clientgui/sg_TaskCommandPopup.cpp \
../clientgui/sg_TaskPanel.cpp \
../clientgui/sg_ViewTabPage.cpp \
../clientgui/stdwx.cpp \
../clientgui/wizardex.cpp 

C_SRCS += \
../clientgui/sqlite3.c 

OBJS += \
./clientgui/AccountInfoPage.o \
./clientgui/AccountManagerInfoPage.o \
./clientgui/AccountManagerProcessingPage.o \
./clientgui/AccountManagerPropertiesPage.o \
./clientgui/AdvancedFrame.o \
./clientgui/AlreadyExistsPage.o \
./clientgui/AsyncRPC.o \
./clientgui/BOINCBaseFrame.o \
./clientgui/BOINCBaseView.o \
./clientgui/BOINCBaseWizard.o \
./clientgui/BOINCBitmapComboBox.o \
./clientgui/BOINCClientManager.o \
./clientgui/BOINCDialupManager.o \
./clientgui/BOINCGUIApp.o \
./clientgui/BOINCHtmlLBox.o \
./clientgui/BOINCInternetFSHandler.o \
./clientgui/BOINCListCtrl.o \
./clientgui/BOINCTaskBar.o \
./clientgui/BOINCTaskCtrl.o \
./clientgui/BOINCVListBox.o \
./clientgui/CompletionErrorPage.o \
./clientgui/CompletionPage.o \
./clientgui/DlgAbout.o \
./clientgui/DlgAdvPreferences.o \
./clientgui/DlgAdvPreferencesBase.o \
./clientgui/DlgEventLog.o \
./clientgui/DlgEventLogListCtrl.o \
./clientgui/DlgExitMessage.o \
./clientgui/DlgGenericMessage.o \
./clientgui/DlgItemProperties.o \
./clientgui/DlgOptions.o \
./clientgui/DlgSelectComputer.o \
./clientgui/Localization.o \
./clientgui/LogBOINC.o \
./clientgui/MainDocument.o \
./clientgui/NoInternetConnectionPage.o \
./clientgui/NotDetectedPage.o \
./clientgui/NotFoundPage.o \
./clientgui/NoticeListCtrl.o \
./clientgui/ProjectInfoPage.o \
./clientgui/ProjectListCtrl.o \
./clientgui/ProjectProcessingPage.o \
./clientgui/ProjectPropertiesPage.o \
./clientgui/ProxyInfoPage.o \
./clientgui/ProxyPage.o \
./clientgui/SkinManager.o \
./clientgui/TermsOfUsePage.o \
./clientgui/UnavailablePage.o \
./clientgui/ValidateAccountKey.o \
./clientgui/ValidateEmailAddress.o \
./clientgui/ValidateURL.o \
./clientgui/ViewMessages.o \
./clientgui/ViewNotices.o \
./clientgui/ViewProjects.o \
./clientgui/ViewResources.o \
./clientgui/ViewStatistics.o \
./clientgui/ViewTransfers.o \
./clientgui/ViewWork.o \
./clientgui/WelcomePage.o \
./clientgui/WizardAttach.o \
./clientgui/browser.o \
./clientgui/sg_BoincSimpleFrame.o \
./clientgui/sg_BoincSimpleGUI.o \
./clientgui/sg_ClientStateIndicator.o \
./clientgui/sg_CustomControls.o \
./clientgui/sg_DlgMessages.o \
./clientgui/sg_DlgPreferences.o \
./clientgui/sg_ImageButton.o \
./clientgui/sg_ImageLoader.o \
./clientgui/sg_PanelBase.o \
./clientgui/sg_ProgressBar.o \
./clientgui/sg_ProjectCommandPopup.o \
./clientgui/sg_ProjectPanel.o \
./clientgui/sg_ProjectWebSitesPopup.o \
./clientgui/sg_ProjectsComponent.o \
./clientgui/sg_StatImageLoader.o \
./clientgui/sg_TaskCommandPopup.o \
./clientgui/sg_TaskPanel.o \
./clientgui/sg_ViewTabPage.o \
./clientgui/sqlite3.o \
./clientgui/stdwx.o \
./clientgui/wizardex.o 

C_DEPS += \
./clientgui/sqlite3.d 

CPP_DEPS += \
./clientgui/AccountInfoPage.d \
./clientgui/AccountManagerInfoPage.d \
./clientgui/AccountManagerProcessingPage.d \
./clientgui/AccountManagerPropertiesPage.d \
./clientgui/AdvancedFrame.d \
./clientgui/AlreadyExistsPage.d \
./clientgui/AsyncRPC.d \
./clientgui/BOINCBaseFrame.d \
./clientgui/BOINCBaseView.d \
./clientgui/BOINCBaseWizard.d \
./clientgui/BOINCBitmapComboBox.d \
./clientgui/BOINCClientManager.d \
./clientgui/BOINCDialupManager.d \
./clientgui/BOINCGUIApp.d \
./clientgui/BOINCHtmlLBox.d \
./clientgui/BOINCInternetFSHandler.d \
./clientgui/BOINCListCtrl.d \
./clientgui/BOINCTaskBar.d \
./clientgui/BOINCTaskCtrl.d \
./clientgui/BOINCVListBox.d \
./clientgui/CompletionErrorPage.d \
./clientgui/CompletionPage.d \
./clientgui/DlgAbout.d \
./clientgui/DlgAdvPreferences.d \
./clientgui/DlgAdvPreferencesBase.d \
./clientgui/DlgEventLog.d \
./clientgui/DlgEventLogListCtrl.d \
./clientgui/DlgExitMessage.d \
./clientgui/DlgGenericMessage.d \
./clientgui/DlgItemProperties.d \
./clientgui/DlgOptions.d \
./clientgui/DlgSelectComputer.d \
./clientgui/Localization.d \
./clientgui/LogBOINC.d \
./clientgui/MainDocument.d \
./clientgui/NoInternetConnectionPage.d \
./clientgui/NotDetectedPage.d \
./clientgui/NotFoundPage.d \
./clientgui/NoticeListCtrl.d \
./clientgui/ProjectInfoPage.d \
./clientgui/ProjectListCtrl.d \
./clientgui/ProjectProcessingPage.d \
./clientgui/ProjectPropertiesPage.d \
./clientgui/ProxyInfoPage.d \
./clientgui/ProxyPage.d \
./clientgui/SkinManager.d \
./clientgui/TermsOfUsePage.d \
./clientgui/UnavailablePage.d \
./clientgui/ValidateAccountKey.d \
./clientgui/ValidateEmailAddress.d \
./clientgui/ValidateURL.d \
./clientgui/ViewMessages.d \
./clientgui/ViewNotices.d \
./clientgui/ViewProjects.d \
./clientgui/ViewResources.d \
./clientgui/ViewStatistics.d \
./clientgui/ViewTransfers.d \
./clientgui/ViewWork.d \
./clientgui/WelcomePage.d \
./clientgui/WizardAttach.d \
./clientgui/browser.d \
./clientgui/sg_BoincSimpleFrame.d \
./clientgui/sg_BoincSimpleGUI.d \
./clientgui/sg_ClientStateIndicator.d \
./clientgui/sg_CustomControls.d \
./clientgui/sg_DlgMessages.d \
./clientgui/sg_DlgPreferences.d \
./clientgui/sg_ImageButton.d \
./clientgui/sg_ImageLoader.d \
./clientgui/sg_PanelBase.d \
./clientgui/sg_ProgressBar.d \
./clientgui/sg_ProjectCommandPopup.d \
./clientgui/sg_ProjectPanel.d \
./clientgui/sg_ProjectWebSitesPopup.d \
./clientgui/sg_ProjectsComponent.d \
./clientgui/sg_StatImageLoader.d \
./clientgui/sg_TaskCommandPopup.d \
./clientgui/sg_TaskPanel.d \
./clientgui/sg_ViewTabPage.d \
./clientgui/stdwx.d \
./clientgui/wizardex.d 


# Each subdirectory must supply rules for building sources it contributes
clientgui/%.o: ../clientgui/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

clientgui/%.o: ../clientgui/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


