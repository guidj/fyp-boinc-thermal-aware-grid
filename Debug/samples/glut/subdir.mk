################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../samples/glut/glut_roman.c \
../samples/glut/glut_stroke.c \
../samples/glut/glut_swidth.c \
../samples/glut/win32_glx.c \
../samples/glut/win32_util.c \
../samples/glut/win32_x11.c 

OBJS += \
./samples/glut/glut_roman.o \
./samples/glut/glut_stroke.o \
./samples/glut/glut_swidth.o \
./samples/glut/win32_glx.o \
./samples/glut/win32_util.o \
./samples/glut/win32_x11.o 

C_DEPS += \
./samples/glut/glut_roman.d \
./samples/glut/glut_stroke.d \
./samples/glut/glut_swidth.d \
./samples/glut/win32_glx.d \
./samples/glut/win32_util.d \
./samples/glut/win32_x11.d 


# Each subdirectory must supply rules for building sources it contributes
samples/glut/%.o: ../samples/glut/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


