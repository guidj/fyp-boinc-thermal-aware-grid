################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../samples/wrappture/fermi.cpp \
../samples/wrappture/wrappture.cpp \
../samples/wrappture/wrappture_example.cpp 

OBJS += \
./samples/wrappture/fermi.o \
./samples/wrappture/wrappture.o \
./samples/wrappture/wrappture_example.o 

CPP_DEPS += \
./samples/wrappture/fermi.d \
./samples/wrappture/wrappture.d \
./samples/wrappture/wrappture_example.d 


# Each subdirectory must supply rules for building sources it contributes
samples/wrappture/%.o: ../samples/wrappture/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


