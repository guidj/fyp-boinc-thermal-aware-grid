################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../samples/jpeglib/jcapimin.c \
../samples/jpeglib/jcapistd.c \
../samples/jpeglib/jccoefct.c \
../samples/jpeglib/jccolor.c \
../samples/jpeglib/jcdctmgr.c \
../samples/jpeglib/jchuff.c \
../samples/jpeglib/jcinit.c \
../samples/jpeglib/jcmainct.c \
../samples/jpeglib/jcmarker.c \
../samples/jpeglib/jcmaster.c \
../samples/jpeglib/jcomapi.c \
../samples/jpeglib/jcparam.c \
../samples/jpeglib/jcphuff.c \
../samples/jpeglib/jcprepct.c \
../samples/jpeglib/jcsample.c \
../samples/jpeglib/jctrans.c \
../samples/jpeglib/jdapimin.c \
../samples/jpeglib/jdapistd.c \
../samples/jpeglib/jdatadst.c \
../samples/jpeglib/jdatasrc.c \
../samples/jpeglib/jdcoefct.c \
../samples/jpeglib/jdcolor.c \
../samples/jpeglib/jddctmgr.c \
../samples/jpeglib/jdhuff.c \
../samples/jpeglib/jdinput.c \
../samples/jpeglib/jdmainct.c \
../samples/jpeglib/jdmarker.c \
../samples/jpeglib/jdmaster.c \
../samples/jpeglib/jdmerge.c \
../samples/jpeglib/jdphuff.c \
../samples/jpeglib/jdpostct.c \
../samples/jpeglib/jdsample.c \
../samples/jpeglib/jdtrans.c \
../samples/jpeglib/jerror.c \
../samples/jpeglib/jfdctflt.c \
../samples/jpeglib/jfdctfst.c \
../samples/jpeglib/jfdctint.c \
../samples/jpeglib/jidctflt.c \
../samples/jpeglib/jidctfst.c \
../samples/jpeglib/jidctint.c \
../samples/jpeglib/jidctred.c \
../samples/jpeglib/jmemmgr.c \
../samples/jpeglib/jmemnobs.c \
../samples/jpeglib/jquant1.c \
../samples/jpeglib/jquant2.c \
../samples/jpeglib/jutils.c \
../samples/jpeglib/rdbmp.c \
../samples/jpeglib/rdcolmap.c \
../samples/jpeglib/rdgif.c \
../samples/jpeglib/rdppm.c \
../samples/jpeglib/rdrle.c \
../samples/jpeglib/rdswitch.c \
../samples/jpeglib/rdtarga.c 

OBJS += \
./samples/jpeglib/jcapimin.o \
./samples/jpeglib/jcapistd.o \
./samples/jpeglib/jccoefct.o \
./samples/jpeglib/jccolor.o \
./samples/jpeglib/jcdctmgr.o \
./samples/jpeglib/jchuff.o \
./samples/jpeglib/jcinit.o \
./samples/jpeglib/jcmainct.o \
./samples/jpeglib/jcmarker.o \
./samples/jpeglib/jcmaster.o \
./samples/jpeglib/jcomapi.o \
./samples/jpeglib/jcparam.o \
./samples/jpeglib/jcphuff.o \
./samples/jpeglib/jcprepct.o \
./samples/jpeglib/jcsample.o \
./samples/jpeglib/jctrans.o \
./samples/jpeglib/jdapimin.o \
./samples/jpeglib/jdapistd.o \
./samples/jpeglib/jdatadst.o \
./samples/jpeglib/jdatasrc.o \
./samples/jpeglib/jdcoefct.o \
./samples/jpeglib/jdcolor.o \
./samples/jpeglib/jddctmgr.o \
./samples/jpeglib/jdhuff.o \
./samples/jpeglib/jdinput.o \
./samples/jpeglib/jdmainct.o \
./samples/jpeglib/jdmarker.o \
./samples/jpeglib/jdmaster.o \
./samples/jpeglib/jdmerge.o \
./samples/jpeglib/jdphuff.o \
./samples/jpeglib/jdpostct.o \
./samples/jpeglib/jdsample.o \
./samples/jpeglib/jdtrans.o \
./samples/jpeglib/jerror.o \
./samples/jpeglib/jfdctflt.o \
./samples/jpeglib/jfdctfst.o \
./samples/jpeglib/jfdctint.o \
./samples/jpeglib/jidctflt.o \
./samples/jpeglib/jidctfst.o \
./samples/jpeglib/jidctint.o \
./samples/jpeglib/jidctred.o \
./samples/jpeglib/jmemmgr.o \
./samples/jpeglib/jmemnobs.o \
./samples/jpeglib/jquant1.o \
./samples/jpeglib/jquant2.o \
./samples/jpeglib/jutils.o \
./samples/jpeglib/rdbmp.o \
./samples/jpeglib/rdcolmap.o \
./samples/jpeglib/rdgif.o \
./samples/jpeglib/rdppm.o \
./samples/jpeglib/rdrle.o \
./samples/jpeglib/rdswitch.o \
./samples/jpeglib/rdtarga.o 

C_DEPS += \
./samples/jpeglib/jcapimin.d \
./samples/jpeglib/jcapistd.d \
./samples/jpeglib/jccoefct.d \
./samples/jpeglib/jccolor.d \
./samples/jpeglib/jcdctmgr.d \
./samples/jpeglib/jchuff.d \
./samples/jpeglib/jcinit.d \
./samples/jpeglib/jcmainct.d \
./samples/jpeglib/jcmarker.d \
./samples/jpeglib/jcmaster.d \
./samples/jpeglib/jcomapi.d \
./samples/jpeglib/jcparam.d \
./samples/jpeglib/jcphuff.d \
./samples/jpeglib/jcprepct.d \
./samples/jpeglib/jcsample.d \
./samples/jpeglib/jctrans.d \
./samples/jpeglib/jdapimin.d \
./samples/jpeglib/jdapistd.d \
./samples/jpeglib/jdatadst.d \
./samples/jpeglib/jdatasrc.d \
./samples/jpeglib/jdcoefct.d \
./samples/jpeglib/jdcolor.d \
./samples/jpeglib/jddctmgr.d \
./samples/jpeglib/jdhuff.d \
./samples/jpeglib/jdinput.d \
./samples/jpeglib/jdmainct.d \
./samples/jpeglib/jdmarker.d \
./samples/jpeglib/jdmaster.d \
./samples/jpeglib/jdmerge.d \
./samples/jpeglib/jdphuff.d \
./samples/jpeglib/jdpostct.d \
./samples/jpeglib/jdsample.d \
./samples/jpeglib/jdtrans.d \
./samples/jpeglib/jerror.d \
./samples/jpeglib/jfdctflt.d \
./samples/jpeglib/jfdctfst.d \
./samples/jpeglib/jfdctint.d \
./samples/jpeglib/jidctflt.d \
./samples/jpeglib/jidctfst.d \
./samples/jpeglib/jidctint.d \
./samples/jpeglib/jidctred.d \
./samples/jpeglib/jmemmgr.d \
./samples/jpeglib/jmemnobs.d \
./samples/jpeglib/jquant1.d \
./samples/jpeglib/jquant2.d \
./samples/jpeglib/jutils.d \
./samples/jpeglib/rdbmp.d \
./samples/jpeglib/rdcolmap.d \
./samples/jpeglib/rdgif.d \
./samples/jpeglib/rdppm.d \
./samples/jpeglib/rdrle.d \
./samples/jpeglib/rdswitch.d \
./samples/jpeglib/rdtarga.d 


# Each subdirectory must supply rules for building sources it contributes
samples/jpeglib/%.o: ../samples/jpeglib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


