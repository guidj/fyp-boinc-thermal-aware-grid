################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../samples/vboxwrapper/floppyio.cpp \
../samples/vboxwrapper/vbox.cpp \
../samples/vboxwrapper/vboxwrapper.cpp 

OBJS += \
./samples/vboxwrapper/floppyio.o \
./samples/vboxwrapper/vbox.o \
./samples/vboxwrapper/vboxwrapper.o 

CPP_DEPS += \
./samples/vboxwrapper/floppyio.d \
./samples/vboxwrapper/vbox.d \
./samples/vboxwrapper/vboxwrapper.d 


# Each subdirectory must supply rules for building sources it contributes
samples/vboxwrapper/%.o: ../samples/vboxwrapper/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


