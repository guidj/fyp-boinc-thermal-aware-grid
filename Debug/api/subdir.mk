################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../api/boinc_api.o \
../api/boinc_opencl.o \
../api/graphics2_util.o \
../api/libboinc_graphics2_la-graphics2.o \
../api/libboinc_graphics2_la-graphics2_unix.o \
../api/libboinc_graphics2_la-gutil.o \
../api/libboinc_graphics2_la-gutil_text.o \
../api/libboinc_graphics2_la-reduce_lib.o \
../api/libboinc_graphics2_la-texfont.o \
../api/libboinc_graphics2_la-texture.o \
../api/libboinc_graphics2_la-txf_util.o \
../api/reduce_main.o 

CPP_SRCS += \
../api/bmplib.cpp \
../api/boinc_api.cpp \
../api/boinc_api_fortran.cpp \
../api/boinc_opencl.cpp \
../api/graphics2.cpp \
../api/graphics2_unix.cpp \
../api/graphics2_util.cpp \
../api/graphics2_win.cpp \
../api/graphics_api.cpp \
../api/graphics_data.cpp \
../api/graphics_impl.cpp \
../api/graphics_impl_lib.cpp \
../api/graphics_lib.cpp \
../api/gutil.cpp \
../api/gutil_text.cpp \
../api/mac_icon.cpp \
../api/make_app_icon_h.cpp \
../api/reduce_lib.cpp \
../api/reduce_main.cpp \
../api/static_graphics.cpp \
../api/texfont.cpp \
../api/texture.cpp \
../api/tgalib.cpp \
../api/ttfont.cpp \
../api/txf_util.cpp \
../api/windows_opengl.cpp \
../api/x_opengl.cpp 

OBJS += \
./api/bmplib.o \
./api/boinc_api.o \
./api/boinc_api_fortran.o \
./api/boinc_opencl.o \
./api/graphics2.o \
./api/graphics2_unix.o \
./api/graphics2_util.o \
./api/graphics2_win.o \
./api/graphics_api.o \
./api/graphics_data.o \
./api/graphics_impl.o \
./api/graphics_impl_lib.o \
./api/graphics_lib.o \
./api/gutil.o \
./api/gutil_text.o \
./api/mac_icon.o \
./api/make_app_icon_h.o \
./api/reduce_lib.o \
./api/reduce_main.o \
./api/static_graphics.o \
./api/texfont.o \
./api/texture.o \
./api/tgalib.o \
./api/ttfont.o \
./api/txf_util.o \
./api/windows_opengl.o \
./api/x_opengl.o 

CPP_DEPS += \
./api/bmplib.d \
./api/boinc_api.d \
./api/boinc_api_fortran.d \
./api/boinc_opencl.d \
./api/graphics2.d \
./api/graphics2_unix.d \
./api/graphics2_util.d \
./api/graphics2_win.d \
./api/graphics_api.d \
./api/graphics_data.d \
./api/graphics_impl.d \
./api/graphics_impl_lib.d \
./api/graphics_lib.d \
./api/gutil.d \
./api/gutil_text.d \
./api/mac_icon.d \
./api/make_app_icon_h.d \
./api/reduce_lib.d \
./api/reduce_main.d \
./api/static_graphics.d \
./api/texfont.d \
./api/texture.d \
./api/tgalib.d \
./api/ttfont.d \
./api/txf_util.d \
./api/windows_opengl.d \
./api/x_opengl.d 


# Each subdirectory must supply rules for building sources it contributes
api/%.o: ../api/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


