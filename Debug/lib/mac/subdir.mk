################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/mac/mac_backtrace.cpp 

C_SRCS += \
../lib/mac/QBacktrace.c \
../lib/mac/QCrashReport.c \
../lib/mac/QMachOImage.c \
../lib/mac/QMachOImageList.c \
../lib/mac/QSymbols.c \
../lib/mac/QTaskMemory.c 

OBJS += \
./lib/mac/QBacktrace.o \
./lib/mac/QCrashReport.o \
./lib/mac/QMachOImage.o \
./lib/mac/QMachOImageList.o \
./lib/mac/QSymbols.o \
./lib/mac/QTaskMemory.o \
./lib/mac/mac_backtrace.o 

C_DEPS += \
./lib/mac/QBacktrace.d \
./lib/mac/QCrashReport.d \
./lib/mac/QMachOImage.d \
./lib/mac/QMachOImageList.d \
./lib/mac/QSymbols.d \
./lib/mac/QTaskMemory.d 

CPP_DEPS += \
./lib/mac/mac_backtrace.d 


# Each subdirectory must supply rules for building sources it contributes
lib/mac/%.o: ../lib/mac/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/mac/%.o: ../lib/mac/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


