################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../lib/crypt_prog-crypt_prog.o \
../lib/libboinc_crypt_la-crypt.o \
../lib/libboinc_la-app_ipc.o \
../lib/libboinc_la-base64.o \
../lib/libboinc_la-cc_config.o \
../lib/libboinc_la-cert_sig.o \
../lib/libboinc_la-coproc.o \
../lib/libboinc_la-diagnostics.o \
../lib/libboinc_la-filesys.o \
../lib/libboinc_la-gui_rpc_client.o \
../lib/libboinc_la-gui_rpc_client_ops.o \
../lib/libboinc_la-gui_rpc_client_print.o \
../lib/libboinc_la-hostinfo.o \
../lib/libboinc_la-mac_address.o \
../lib/libboinc_la-md5.o \
../lib/libboinc_la-md5_file.o \
../lib/libboinc_la-mem_usage.o \
../lib/libboinc_la-mfile.o \
../lib/libboinc_la-miofile.o \
../lib/libboinc_la-msg_log.o \
../lib/libboinc_la-network.o \
../lib/libboinc_la-notice.o \
../lib/libboinc_la-parse.o \
../lib/libboinc_la-prefs.o \
../lib/libboinc_la-proc_control.o \
../lib/libboinc_la-procinfo.o \
../lib/libboinc_la-procinfo_unix.o \
../lib/libboinc_la-proxy_info.o \
../lib/libboinc_la-shmem.o \
../lib/libboinc_la-str_util.o \
../lib/libboinc_la-synch.o \
../lib/libboinc_la-unix_util.o \
../lib/libboinc_la-url.o \
../lib/libboinc_la-util.o \
../lib/parse_test-parse_test.o 

CPP_SRCS += \
../lib/app_ipc.cpp \
../lib/average.cpp \
../lib/base64.cpp \
../lib/boinc_fcgi.cpp \
../lib/boinc_win.cpp \
../lib/cc_config.cpp \
../lib/cert_sig.cpp \
../lib/coproc.cpp \
../lib/crypt.cpp \
../lib/crypt_prog.cpp \
../lib/daemonmgt_win.cpp \
../lib/diagnostics.cpp \
../lib/diagnostics_win.cpp \
../lib/filesys.cpp \
../lib/gui_rpc_client.cpp \
../lib/gui_rpc_client_ops.cpp \
../lib/gui_rpc_client_print.cpp \
../lib/hostinfo.cpp \
../lib/idlemon_win.cpp \
../lib/mac_address.cpp \
../lib/md5_file.cpp \
../lib/md5_test.cpp \
../lib/mem_usage.cpp \
../lib/mfile.cpp \
../lib/miofile.cpp \
../lib/msg_log.cpp \
../lib/msg_queue.cpp \
../lib/msg_test.cpp \
../lib/network.cpp \
../lib/notice.cpp \
../lib/parse.cpp \
../lib/parse_test.cpp \
../lib/prefs.cpp \
../lib/proc_control.cpp \
../lib/procinfo.cpp \
../lib/procinfo_mac.cpp \
../lib/procinfo_unix.cpp \
../lib/procinfo_win.cpp \
../lib/proxy_info.cpp \
../lib/run_app_windows.cpp \
../lib/shmem.cpp \
../lib/shmem_test.cpp \
../lib/stackwalker_win.cpp \
../lib/str_util.cpp \
../lib/susp.cpp \
../lib/synch.cpp \
../lib/synch_test.cpp \
../lib/thread.cpp \
../lib/unix_util.cpp \
../lib/url.cpp \
../lib/util.cpp \
../lib/win_util.cpp \
../lib/x_util.cpp 

C_SRCS += \
../lib/md5.c 

OBJS += \
./lib/app_ipc.o \
./lib/average.o \
./lib/base64.o \
./lib/boinc_fcgi.o \
./lib/boinc_win.o \
./lib/cc_config.o \
./lib/cert_sig.o \
./lib/coproc.o \
./lib/crypt.o \
./lib/crypt_prog.o \
./lib/daemonmgt_win.o \
./lib/diagnostics.o \
./lib/diagnostics_win.o \
./lib/filesys.o \
./lib/gui_rpc_client.o \
./lib/gui_rpc_client_ops.o \
./lib/gui_rpc_client_print.o \
./lib/hostinfo.o \
./lib/idlemon_win.o \
./lib/mac_address.o \
./lib/md5.o \
./lib/md5_file.o \
./lib/md5_test.o \
./lib/mem_usage.o \
./lib/mfile.o \
./lib/miofile.o \
./lib/msg_log.o \
./lib/msg_queue.o \
./lib/msg_test.o \
./lib/network.o \
./lib/notice.o \
./lib/parse.o \
./lib/parse_test.o \
./lib/prefs.o \
./lib/proc_control.o \
./lib/procinfo.o \
./lib/procinfo_mac.o \
./lib/procinfo_unix.o \
./lib/procinfo_win.o \
./lib/proxy_info.o \
./lib/run_app_windows.o \
./lib/shmem.o \
./lib/shmem_test.o \
./lib/stackwalker_win.o \
./lib/str_util.o \
./lib/susp.o \
./lib/synch.o \
./lib/synch_test.o \
./lib/thread.o \
./lib/unix_util.o \
./lib/url.o \
./lib/util.o \
./lib/win_util.o \
./lib/x_util.o 

C_DEPS += \
./lib/md5.d 

CPP_DEPS += \
./lib/app_ipc.d \
./lib/average.d \
./lib/base64.d \
./lib/boinc_fcgi.d \
./lib/boinc_win.d \
./lib/cc_config.d \
./lib/cert_sig.d \
./lib/coproc.d \
./lib/crypt.d \
./lib/crypt_prog.d \
./lib/daemonmgt_win.d \
./lib/diagnostics.d \
./lib/diagnostics_win.d \
./lib/filesys.d \
./lib/gui_rpc_client.d \
./lib/gui_rpc_client_ops.d \
./lib/gui_rpc_client_print.d \
./lib/hostinfo.d \
./lib/idlemon_win.d \
./lib/mac_address.d \
./lib/md5_file.d \
./lib/md5_test.d \
./lib/mem_usage.d \
./lib/mfile.d \
./lib/miofile.d \
./lib/msg_log.d \
./lib/msg_queue.d \
./lib/msg_test.d \
./lib/network.d \
./lib/notice.d \
./lib/parse.d \
./lib/parse_test.d \
./lib/prefs.d \
./lib/proc_control.d \
./lib/procinfo.d \
./lib/procinfo_mac.d \
./lib/procinfo_unix.d \
./lib/procinfo_win.d \
./lib/proxy_info.d \
./lib/run_app_windows.d \
./lib/shmem.d \
./lib/shmem_test.d \
./lib/stackwalker_win.d \
./lib/str_util.d \
./lib/susp.d \
./lib/synch.d \
./lib/synch_test.d \
./lib/thread.d \
./lib/unix_util.d \
./lib/url.d \
./lib/util.d \
./lib/win_util.d \
./lib/x_util.d 


# Each subdirectory must supply rules for building sources it contributes
lib/%.o: ../lib/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/%.o: ../lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


