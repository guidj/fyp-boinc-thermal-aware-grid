################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../apps/1sec-1sec.o \
../apps/concat-concat.o \
../apps/upper_case-upper_case.o 

CPP_SRCS += \
../apps/1sec.cpp \
../apps/concat.cpp \
../apps/error.cpp \
../apps/upper_case.cpp 

OBJS += \
./apps/1sec.o \
./apps/concat.o \
./apps/error.o \
./apps/upper_case.o 

CPP_DEPS += \
./apps/1sec.d \
./apps/concat.d \
./apps/error.d \
./apps/upper_case.d 


# Each subdirectory must supply rules for building sources it contributes
apps/%.o: ../apps/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


