################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../client/boinc_client-acct_mgr.o \
../client/boinc_client-acct_setup.o \
../client/boinc_client-app.o \
../client/boinc_client-app_control.o \
../client/boinc_client-app_start.o \
../client/boinc_client-check_state.o \
../client/boinc_client-client_msgs.o \
../client/boinc_client-client_state.o \
../client/boinc_client-client_types.o \
../client/boinc_client-coproc_detect.o \
../client/boinc_client-cpu_sched.o \
../client/boinc_client-cs_account.o \
../client/boinc_client-cs_apps.o \
../client/boinc_client-cs_benchmark.o \
../client/boinc_client-cs_cmdline.o \
../client/boinc_client-cs_files.o \
../client/boinc_client-cs_notice.o \
../client/boinc_client-cs_platforms.o \
../client/boinc_client-cs_prefs.o \
../client/boinc_client-cs_proxy.o \
../client/boinc_client-cs_scheduler.o \
../client/boinc_client-cs_statefile.o \
../client/boinc_client-cs_trickle.o \
../client/boinc_client-current_version.o \
../client/boinc_client-dhrystone.o \
../client/boinc_client-dhrystone2.o \
../client/boinc_client-file_names.o \
../client/boinc_client-file_xfer.o \
../client/boinc_client-gui_http.o \
../client/boinc_client-gui_rpc_server.o \
../client/boinc_client-gui_rpc_server_ops.o \
../client/boinc_client-hostinfo_network.o \
../client/boinc_client-hostinfo_unix.o \
../client/boinc_client-http_curl.o \
../client/boinc_client-log_flags.o \
../client/boinc_client-main.o \
../client/boinc_client-net_stats.o \
../client/boinc_client-pers_file_xfer.o \
../client/boinc_client-rr_sim.o \
../client/boinc_client-sandbox.o \
../client/boinc_client-scheduler_op.o \
../client/boinc_client-time_stats.o \
../client/boinc_client-whetstone.o \
../client/boinc_client-work_fetch.o \
../client/boinccmd-boinc_cmd.o \
../client/switcher.o 

CPP_SRCS += \
../client/acct_mgr.cpp \
../client/acct_setup.cpp \
../client/app.cpp \
../client/app_control.cpp \
../client/app_graphics.cpp \
../client/app_start.cpp \
../client/app_stats_mac.cpp \
../client/auto_update.cpp \
../client/boinc_cmd.cpp \
../client/boinc_log.cpp \
../client/check_security.cpp \
../client/check_state.cpp \
../client/client_msgs.cpp \
../client/client_state.cpp \
../client/client_types.cpp \
../client/coproc_detect.cpp \
../client/cpu_sched.cpp \
../client/cs_account.cpp \
../client/cs_apps.cpp \
../client/cs_benchmark.cpp \
../client/cs_cmdline.cpp \
../client/cs_files.cpp \
../client/cs_notice.cpp \
../client/cs_platforms.cpp \
../client/cs_prefs.cpp \
../client/cs_proxy.cpp \
../client/cs_scheduler.cpp \
../client/cs_statefile.cpp \
../client/cs_trickle.cpp \
../client/current_version.cpp \
../client/dhrystone.cpp \
../client/dhrystone2.cpp \
../client/file_names.cpp \
../client/file_xfer.cpp \
../client/gui_http.cpp \
../client/gui_rpc_server.cpp \
../client/gui_rpc_server_ops.cpp \
../client/hostinfo_network.cpp \
../client/hostinfo_unix.cpp \
../client/hostinfo_unix_test.cpp \
../client/hostinfo_win.cpp \
../client/http_curl.cpp \
../client/log_flags.cpp \
../client/main.cpp \
../client/net_stats.cpp \
../client/pers_file_xfer.cpp \
../client/rr_sim.cpp \
../client/rrsim_test.cpp \
../client/sandbox.cpp \
../client/scheduler_op.cpp \
../client/setprojectgrp.cpp \
../client/sim.cpp \
../client/sim_util.cpp \
../client/stream.cpp \
../client/switcher.cpp \
../client/sysmon_win.cpp \
../client/time_stats.cpp \
../client/whetstone.cpp \
../client/work_fetch.cpp 

OBJS += \
./client/acct_mgr.o \
./client/acct_setup.o \
./client/app.o \
./client/app_control.o \
./client/app_graphics.o \
./client/app_start.o \
./client/app_stats_mac.o \
./client/auto_update.o \
./client/boinc_cmd.o \
./client/boinc_log.o \
./client/check_security.o \
./client/check_state.o \
./client/client_msgs.o \
./client/client_state.o \
./client/client_types.o \
./client/coproc_detect.o \
./client/cpu_sched.o \
./client/cs_account.o \
./client/cs_apps.o \
./client/cs_benchmark.o \
./client/cs_cmdline.o \
./client/cs_files.o \
./client/cs_notice.o \
./client/cs_platforms.o \
./client/cs_prefs.o \
./client/cs_proxy.o \
./client/cs_scheduler.o \
./client/cs_statefile.o \
./client/cs_trickle.o \
./client/current_version.o \
./client/dhrystone.o \
./client/dhrystone2.o \
./client/file_names.o \
./client/file_xfer.o \
./client/gui_http.o \
./client/gui_rpc_server.o \
./client/gui_rpc_server_ops.o \
./client/hostinfo_network.o \
./client/hostinfo_unix.o \
./client/hostinfo_unix_test.o \
./client/hostinfo_win.o \
./client/http_curl.o \
./client/log_flags.o \
./client/main.o \
./client/net_stats.o \
./client/pers_file_xfer.o \
./client/rr_sim.o \
./client/rrsim_test.o \
./client/sandbox.o \
./client/scheduler_op.o \
./client/setprojectgrp.o \
./client/sim.o \
./client/sim_util.o \
./client/stream.o \
./client/switcher.o \
./client/sysmon_win.o \
./client/time_stats.o \
./client/whetstone.o \
./client/work_fetch.o 

CPP_DEPS += \
./client/acct_mgr.d \
./client/acct_setup.d \
./client/app.d \
./client/app_control.d \
./client/app_graphics.d \
./client/app_start.d \
./client/app_stats_mac.d \
./client/auto_update.d \
./client/boinc_cmd.d \
./client/boinc_log.d \
./client/check_security.d \
./client/check_state.d \
./client/client_msgs.d \
./client/client_state.d \
./client/client_types.d \
./client/coproc_detect.d \
./client/cpu_sched.d \
./client/cs_account.d \
./client/cs_apps.d \
./client/cs_benchmark.d \
./client/cs_cmdline.d \
./client/cs_files.d \
./client/cs_notice.d \
./client/cs_platforms.d \
./client/cs_prefs.d \
./client/cs_proxy.d \
./client/cs_scheduler.d \
./client/cs_statefile.d \
./client/cs_trickle.d \
./client/current_version.d \
./client/dhrystone.d \
./client/dhrystone2.d \
./client/file_names.d \
./client/file_xfer.d \
./client/gui_http.d \
./client/gui_rpc_server.d \
./client/gui_rpc_server_ops.d \
./client/hostinfo_network.d \
./client/hostinfo_unix.d \
./client/hostinfo_unix_test.d \
./client/hostinfo_win.d \
./client/http_curl.d \
./client/log_flags.d \
./client/main.d \
./client/net_stats.d \
./client/pers_file_xfer.d \
./client/rr_sim.d \
./client/rrsim_test.d \
./client/sandbox.d \
./client/scheduler_op.d \
./client/setprojectgrp.d \
./client/sim.d \
./client/sim_util.d \
./client/stream.d \
./client/switcher.d \
./client/sysmon_win.d \
./client/time_stats.d \
./client/whetstone.d \
./client/work_fetch.d 


# Each subdirectory must supply rules for building sources it contributes
client/%.o: ../client/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


