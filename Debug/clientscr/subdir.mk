################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../clientscr/gfx_switcher.cpp \
../clientscr/mac_saver_module.cpp \
../clientscr/screensaver.cpp \
../clientscr/screensaver_win.cpp \
../clientscr/screensaver_x11.cpp \
../clientscr/ss_app.cpp 

OBJS += \
./clientscr/gfx_switcher.o \
./clientscr/mac_saver_module.o \
./clientscr/screensaver.o \
./clientscr/screensaver_win.o \
./clientscr/screensaver_x11.o \
./clientscr/ss_app.o 

CPP_DEPS += \
./clientscr/gfx_switcher.d \
./clientscr/mac_saver_module.d \
./clientscr/screensaver.d \
./clientscr/screensaver_win.d \
./clientscr/screensaver_x11.d \
./clientscr/ss_app.d 


# Each subdirectory must supply rules for building sources it contributes
clientscr/%.o: ../clientscr/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


