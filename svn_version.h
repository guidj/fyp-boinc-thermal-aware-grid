#ifndef SVN_VERSION_H
#define SVN_VERSION_H

#define SVN_VERSION "44298b7110ce28bbfefcae4eddce0cb0639907fa [https://WilliamIK@bitbucket.org/WilliamIK/fyp_tag.git] (myserver:/user/dinis/FYP/src/boinc [master])"
#define GIT_REVISION 0x44298b71
#define GIT_DATE 1391395005
#define REPOSITORY_BITBUCKET_ORG_WILLIAMIK_FYP_TAG 1

#endif
