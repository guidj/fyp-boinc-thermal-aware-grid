// This file is part of BOINC.
// http://boinc.berkeley.edu
// Copyright (C) 2008 University of California
//
// BOINC is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// BOINC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with BOINC.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"
#include <cstdlib>
#include <cassert>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <time.h>
#include <stdlib.h>

#include "parse.h"
#include "error_numbers.h"
#include "str_util.h"
#include "util.h"
#include "boinc_db.h"

#include "sched_main.h"
#include "sched_util.h"
#include "sched_msgs.h"
#include "sched_send.h"
#include "time_stats_log.h"
#include "sched_types.h"

#ifdef _USING_FCGI_
#include "boinc_fcgi.h"
#endif

using std::string;

SCHEDULER_REQUEST* g_request;
SCHEDULER_REPLY* g_reply;
WORK_REQ* g_wreq;
//std::vector<SCHEDULER_REQUEST>* g_request_queue = new std::vector<SCHEDULER_REQUEST>();
//const int REQUEST_QUEUE_THRESHOLD = 12;

// remove (by truncating) any quotes from the given string.
// This is for things (e.g. authenticator) that will be used in
// a SQL query, to prevent SQL injection attacks
//
void remove_quotes(char* p) {
    int i, n=strlen(p);
    for (i=0; i<n; i++) {
        if (p[i]=='\'' || p[i]=='"') {
            p[i] = 0;
            return;
        }
    }
}

int CLIENT_APP_VERSION::parse(XML_PARSER& xp) {
    double x;

    memset(this, 0, sizeof(*this));
    host_usage.avg_ncpus = 1;
    while (!xp.get_tag()) {
        if (xp.match_tag("/app_version")) {
            app = ssp->lookup_app_name(app_name);
            if (!app) return ERR_NOT_FOUND;

            double pf = host_usage.avg_ncpus * g_reply->host.p_fpops;
            if (host_usage.ncudas && g_request->coprocs.nvidia.count) {
                pf += host_usage.ncudas*g_request->coprocs.nvidia.peak_flops;
            }
            if (host_usage.natis && g_request->coprocs.ati.count) {
                pf += host_usage.natis*g_request->coprocs.ati.peak_flops;
            }
            host_usage.peak_flops = pf;
            return 0;
        }
        if (xp.parse_str("app_name", app_name, 256)) continue;
        if (xp.parse_str("platform", platform, 256)) continue;
        if (xp.parse_str("plan_class", plan_class, 256)) continue;
        if (xp.parse_int("version_num", version_num)) continue;
        if (xp.parse_double("avg_ncpus", x)) {
            if (x>0) host_usage.avg_ncpus = x;
            continue;
        }
        if (xp.parse_double("flops", x)) {
            if (x>0) host_usage.projected_flops = x;
            continue;
        }
        if (xp.match_tag("coproc")) {
            COPROC_REQ coproc_req;
            int retval = coproc_req.parse(xp);
            if (!retval && !strcmp(coproc_req.type, "CUDA")) {
                host_usage.ncudas = coproc_req.count;
            }
            if (!retval && !strcmp(coproc_req.type, "NVIDIA")) {
                host_usage.ncudas = coproc_req.count;
            }
            if (!retval && !strcmp(coproc_req.type, "ATI")) {
                host_usage.natis = coproc_req.count;
            }
            continue;
        }
    }
    return ERR_XML_PARSE;
}

int FILE_INFO::parse(XML_PARSER& xp) {
    memset(this, 0, sizeof(*this));
    while (!xp.get_tag()) {
        if (xp.match_tag("/file_info")) {
            if (!strlen(name)) return ERR_XML_PARSE;
            return 0;//write
        }
        if (xp.parse_str("name", name, 256)) continue;
    }
    return ERR_XML_PARSE;
}

int OTHER_RESULT::parse(XML_PARSER& xp) {
    strcpy(name, "");
    have_plan_class = false;
    app_version = -1;
    while (!xp.get_tag()) {
        if (xp.match_tag("/other_result")) {
            if (!strcmp(name, "")) return ERR_XML_PARSE;
            return 0;
        }
        if (xp.parse_str("name", name, sizeof(name))) continue;
        if (xp.parse_int("app_version", app_version)) continue;
        if (xp.parse_str("plan_class", plan_class, sizeof(plan_class))) {
            have_plan_class = true;
            continue;
        }
    }
    return ERR_XML_PARSE;
}

int IP_RESULT::parse(XML_PARSER& xp) {
    report_deadline = 0;
    cpu_time_remaining = 0;
    strcpy(name, "");
    while (!xp.get_tag()) {
        if (xp.match_tag("/ip_result")) return 0;
        if (xp.parse_str("name", name, sizeof(name))) continue;
        if (xp.parse_double("report_deadline", report_deadline)) continue;
        if (xp.parse_double("cpu_time_remaining", cpu_time_remaining)) continue;
    }
    return ERR_XML_PARSE;
}

int CLIENT_PLATFORM::parse(XML_PARSER& xp) {
    strcpy(name, "");
    while (!xp.get_tag()) {
        if (xp.match_tag("/alt_platform")) return 0;
        if (xp.parse_str("name", name, sizeof(name))) continue;
    }
    return ERR_XML_PARSE;
}


void WORK_REQ::add_no_work_message(const char* message) {
    for (unsigned int i=0; i<no_work_messages.size(); i++) {
        if (!strcmp(message, no_work_messages.at(i).message.c_str())){
            return;
        }
    }
    no_work_messages.push_back(USER_MESSAGE(message, "notice"));
}

// return an error message or NULL
//
/*const char* SCHEDULER_REQUEST::parse(XML_PARSER& xp) {
    SCHED_DB_RESULT result;
    int retval;

    strcpy(authenticator, "");
    strcpy(platform.name, "");
    strcpy(cross_project_id, "");
    hostid = 0;
    core_client_major_version = 0;
    core_client_minor_version = 0;
    core_client_release = 0;
    core_client_version = 0;
    rpc_seqno = 0;
    work_req_seconds = 0;
    cpu_req_secs = 0;
    cpu_req_instances = 0;
    resource_share_fraction = 1.0;
    rrs_fraction = 1.0;
    prrs_fraction = 1.0;
    cpu_estimated_delay = 0;
    strcpy(global_prefs_xml, "");
    strcpy(working_global_prefs_xml, "");
    strcpy(code_sign_key, "");
    memset(&global_prefs, 0, sizeof(global_prefs));
    memset(&host, 0, sizeof(host));
    have_other_results_list = false;
    have_ip_results_list = false;
    have_time_stats_log = false;
    client_cap_plan_class = false;
    sandbox = -1;
    allow_multiple_clients = -1;

    if (xp.get_tag()) {
        return "xp.get_tag() failed";
    }
    if (xp.match_tag("?xml")) {
        xp.get_tag();
    }
    if (!xp.match_tag("scheduler_request")) return "no start tag";
    while (!xp.get_tag()) {
        if (xp.match_tag("/scheduler_request")) {
            core_client_version = 10000*core_client_major_version + 100*core_client_minor_version + core_client_release;
            return NULL;
        }
        if (xp.parse_str("authenticator", authenticator, sizeof(authenticator))) {
            remove_quotes(authenticator);
            continue;
        }
        if (xp.parse_str("cross_project_id", cross_project_id, sizeof(cross_project_id))) continue;
        if (xp.parse_int("hostid", hostid)) continue;
        if (xp.parse_int("rpc_seqno", rpc_seqno)) continue;
        if (xp.parse_str("platform_name", platform.name, sizeof(platform.name))) continue;
        if (xp.match_tag("alt_platform")) {
            CLIENT_PLATFORM cp;
            retval = cp.parse(xp);
            if (!retval) {
                alt_platforms.push_back(cp);
            }
            continue;
        }
        if (xp.match_tag("app_versions")) {
            while (!xp.get_tag()) {
                if (xp.match_tag("/app_versions")) break;
                if (xp.match_tag("app_version")) {
                    CLIENT_APP_VERSION cav;
                    retval = cav.parse(xp);
                    if (retval) {
                        if (!strcmp(platform.name, "anonymous")) {
                            if (retval == ERR_NOT_FOUND) {
                                g_reply->insert_message(
                                    _("Unknown app name in app_info.xml"),
                                    "notice"
                                );
                            } else {
                                g_reply->insert_message(
                                    _("Syntax error in app_info.xml"),
                                    "notice"
                                );
                            }
                        } else {
                            // this case happens if the app version
                            // refers to a deprecated app
                        }
                        cav.app = 0;
                    }
                    // store the CLIENT_APP_VERSION even if it didn't parse.
                    // This is necessary to maintain the correspondence
                    // with result.app_version
                    //
                    client_app_versions.push_back(cav);
                }
            }
            continue;
        }
        if (xp.parse_int("core_client_major_version", core_client_major_version)) continue;
        if (xp.parse_int("core_client_minor_version", core_client_minor_version)) continue;
        if (xp.parse_int("core_client_release", core_client_release)) continue;
        if (xp.parse_double("work_req_seconds", work_req_seconds)) continue;
        if (xp.parse_double("cpu_req_secs", cpu_req_secs)) continue;
        if (xp.parse_double("cpu_req_instances", cpu_req_instances)) continue;
        if (xp.parse_double("resource_share_fraction", resource_share_fraction)) continue;
        if (xp.parse_double("rrs_fraction", rrs_fraction)) continue;
        if (xp.parse_double("prrs_fraction", prrs_fraction)) continue;
        if (xp.parse_double("estimated_delay", cpu_estimated_delay)) continue;
        if (xp.parse_double("duration_correction_factor", host.duration_correction_factor)) continue;
        if (xp.match_tag("global_preferences")) {
            strcpy(global_prefs_xml, "<global_preferences>\n");
            char buf[BLOB_SIZE];
            retval = xp.element_contents(
                "</global_preferences>", buf, sizeof(buf)
            );
            if (retval) return "error copying global prefs";
            safe_strcat(global_prefs_xml, buf);
            safe_strcat(global_prefs_xml, "</global_preferences>\n");
            continue;
        }
        if (xp.match_tag("working_global_preferences")) {
            retval = xp.element_contents(
                "</working_global_preferences>",
                working_global_prefs_xml,
                sizeof(working_global_prefs_xml)
            );
            if (retval) return "error copying working global prefs";
            continue;
        }
        if (xp.parse_str("global_prefs_source_email_hash", global_prefs_source_email_hash, sizeof(global_prefs_source_email_hash))) continue;
        if (xp.match_tag("host_info")) {
            host.parse(xp);
            continue;
        }
        if (xp.match_tag("time_stats")) {
            host.parse_time_stats(xp);
            continue;
        }
        if (xp.match_tag("time_stats_log")) {
            handle_time_stats_log(xp.f->f);
            have_time_stats_log = true;
            continue;
        }
        if (xp.match_tag("net_stats")) {
            host.parse_net_stats(xp);
            continue;
        }
        if (xp.match_tag("disk_usage")) {
            host.parse_disk_usage(xp);
            continue;
        }
        if (xp.match_tag("result")) {
            retval = result.parse_from_client(xp);
            if (retval) continue;
            if (strstr(result.name, "download") || strstr(result.name, "upload")) {
                file_xfer_results.push_back(result);
                continue;
            }
#if 0   // enable if you need to limit CGI memory size
            if (results.size() >= 1024) {
                continue;
            }
#endif
            // check if client is sending the same result twice.
            // Shouldn't happen, but if it does bad things will happen
            //
            bool found = false;
            for (unsigned int i=0; i<results.size(); i++) {
                if (!strcmp(results[i].name, result.name)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                results.push_back(result);
            }
            continue;
        }
        if (xp.match_tag("code_sign_key")) {
            copy_element_contents(xp.f->f, "</code_sign_key>", code_sign_key, sizeof(code_sign_key));
            strip_whitespace(code_sign_key);
            continue;
        }
        if (xp.match_tag("msg_from_host")) {
            MSG_FROM_HOST_DESC md;
            retval = md.parse(xp);
            if (!retval) {
                msgs_from_host.push_back(md);
            }
            continue;
        }
        if (xp.match_tag("file_info")) {
            FILE_INFO fi;
            retval = fi.parse(xp);
            if (!retval) {
                file_infos.push_back(fi);
            }
            continue;
        }
        if (xp.match_tag("host_venue")) {
            continue;
        }
        if (xp.match_tag("other_results")) {
            have_other_results_list = true;
            while (!xp.get_tag()) {
                if (xp.match_tag("/other_results")) break;
                if (xp.match_tag("other_result")) {
                    OTHER_RESULT o_r;
                    retval = o_r.parse(xp);
                    if (!retval) {
                        other_results.push_back(o_r);
                    }
                }
            }
            continue;
        }
        if (xp.match_tag("in_progress_results")) {
            have_ip_results_list = true;
            int i = 0;
            double now = time(0);
            while (!xp.get_tag()) {
                if (xp.match_tag("/in_progress_results")) break;
                if (xp.match_tag("ip_result")) {
                    IP_RESULT ir;
                    retval = ir.parse(xp);
                    if (!retval) {
                        if (!strlen(ir.name)) {
                            sprintf(ir.name, "ip%d", i++);
                        }
                        ir.report_deadline -= now;
                        ip_results.push_back(ir);
                    }
                }
            }
            continue;
        }
        if (xp.match_tag("coprocs")) {
            coprocs.parse(xp);
            continue;
        }
        if (xp.parse_bool("client_cap_plan_class", client_cap_plan_class)) continue;
        if (xp.parse_int("sandbox", sandbox)) continue;
        if (xp.parse_int("allow_multiple_clients", allow_multiple_clients)) continue;
        if (xp.parse_string("client_opaque", client_opaque)) continue;

        if (xp.match_tag("active_task_set")) continue;
        if (xp.match_tag("app")) continue;
        if (xp.match_tag("app_version")) continue;
        if (xp.match_tag("duration_variability")) continue;
        if (xp.match_tag("new_version_check_time")) continue;
        if (xp.match_tag("newer_version")) continue;
        if (xp.match_tag("project")) continue;
        if (xp.match_tag("project_files")) continue;
        if (xp.match_tag("proxy_info")) continue;
        if (xp.match_tag("user_network_request")) continue;
        if (xp.match_tag("user_run_request")) continue;
        if (xp.match_tag("master_url")) continue;
        if (xp.match_tag("project_name")) continue;
        if (xp.match_tag("user_name")) continue;
        if (xp.match_tag("team_name")) continue;
        if (xp.match_tag("email_hash")) continue;
        if (xp.match_tag("user_total_credit")) continue;
        if (xp.match_tag("user_expavg_credit")) continue;
        if (xp.match_tag("user_create_time")) continue;
        if (xp.match_tag("host_total_credit")) continue;
        if (xp.match_tag("host_expavg_credit")) continue;
        if (xp.match_tag("host_create_time")) continue;
        if (xp.match_tag("nrpc_failures")) continue;
        if (xp.match_tag("master_fetch_failures")) continue;
        if (xp.match_tag("min_rpc_time")) continue;
        if (xp.match_tag("short_term_debt")) continue;
        if (xp.match_tag("long_term_debt")) continue;
        if (xp.match_tag("resource_share")) continue;
        if (xp.match_tag("scheduler_url")) continue;
        if (xp.match_tag("/project")) continue;
        if (xp.match_tag("?xml")) continue;

        log_messages.printf(MSG_NORMAL,
            "SCHEDULER_REQUEST::parse(): unexpected: %s\n", xp.parsed_tag
        );
        xp.skip_unexpected();
    }
    return "no end tag";
}*/

extern void handle_host_availability();
extern void handle_host_availability(SCHEDULER_REQUEST*);

void testThermalReading(){

	char req_path[256] = "test.xml";

	FILE* fin;
	fin = fopen(req_path, "r");

	SCHEDULER_REQUEST sreq;

    MIOFILE mf;
    XML_PARSER xp(&mf);
    mf.init_file(fin);

    const char* p = sreq.parse(xp);

    if (!p){
        log_messages.printf(MSG_NORMAL,
            "Records parsed correctly");

        for (int i = 0; i < sreq.host_availability_records.size(); i++){

        	log_messages.printf(MSG_NORMAL,
        	            "\nRecord %i: \n"
        	            "\nYear: %i | Month: %i | Day: %i | Hour: %i | Minutes: %i | Availability: %f \n", i,
        	            sreq.host_availability_records[i].year,
        	            sreq.host_availability_records[i].month,
        	            sreq.host_availability_records[i].day,
        	            sreq.host_availability_records[i].hour,
        	            sreq.host_availability_records[i].minutes,
        	            sreq.host_availability_records[i].availability);
        }

        log_messages.printf(MSG_NORMAL,
            "Inserting records: \n");

        handle_host_availability(&sreq);

    }else{
        log_messages.printf(MSG_NORMAL,
            "Records parsed correctly");
    }
}

void SCHEDULER_REQUEST::clone(SCHEDULER_REQUEST& source, SCHEDULER_REQUEST& destination){

    std::strncpy(destination.authenticator,
    		source.authenticator,
    		std::char_traits<char>::length(source.authenticator));
    destination.platform = source.platform;
    destination.alt_platforms = source.alt_platforms;
    destination.platforms = source.platforms;
    std::strncpy(destination.cross_project_id,
    		source.cross_project_id,
    		std::char_traits<char>::length(source.cross_project_id));
    destination.hostid = source.hostid;  // zero if first RPC

    destination.core_client_major_version = source.core_client_major_version;
    destination.core_client_minor_version = source.core_client_minor_version;
    destination.core_client_release = source.core_client_release;
    destination.core_client_version = source.core_client_version;    // 10000*major + 100*minor + release
    destination.rpc_seqno = source.rpc_seqno;
    destination.work_req_seconds = source.work_req_seconds;
        // in "normalized CPU seconds" (see work_req.php)
    destination.cpu_req_secs = source.cpu_req_secs;
    destination.cpu_req_instances = source.cpu_req_instances;
    destination.resource_share_fraction = source.resource_share_fraction;
        // this project's fraction of total resource share
    destination.rrs_fraction = source.rrs_fraction;
        // ... of runnable resource share
    destination.prrs_fraction = source.prrs_fraction;
        // ... of potentially runnable resource share
    destination.cpu_estimated_delay = source.cpu_estimated_delay;
        // currently queued jobs saturate the CPU for this long;
        // used for crude deadline check
    destination.duration_correction_factor = source.duration_correction_factor;
    std::strncpy(destination.global_prefs_xml,
    		source.global_prefs_xml,
    		std::char_traits<char>::length(source.global_prefs_xml));
    std::strncpy(destination.working_global_prefs_xml,
    		source.working_global_prefs_xml,
    		std::char_traits<char>::length(source.working_global_prefs_xml));
    std::strncpy(destination.code_sign_key,
    		source.code_sign_key,
    		std::char_traits<char>::length(source.code_sign_key));

    destination.client_app_versions = source.client_app_versions;

    destination.global_prefs = source.global_prefs;
    std::strncpy(destination.global_prefs_source_email_hash,
    		source.global_prefs_source_email_hash,
    		std::char_traits<char>::length(source.global_prefs_source_email_hash));

    destination.host = source.host;      // request message is parsed into here.
                    // does NOT contain the full host record.
    destination.coprocs = source.coprocs;
    destination.results = source.results;
        // completed results being reported
    destination.file_xfer_results = source.file_xfer_results;
    destination.msgs_from_host = source.msgs_from_host;
    destination.file_infos = source.file_infos;
        // sticky files reported by host for locality scheduling
    destination.file_delete_candidates = source.file_delete_candidates;
        // sticky files reported by host, deletion candidates
    destination.files_not_needed = source.files_not_needed;
        // sticky files reported by host, no longer needed
    destination.other_results = source.other_results;
        // in-progress results from this project
    destination.ip_results = source.ip_results;
        // in-progress results from all projects
    destination.have_other_results_list = source.have_other_results_list;
    destination.have_ip_results_list = source.have_ip_results_list;
    destination.have_time_stats_log = source.have_time_stats_log;
    destination.client_cap_plan_class = source.client_cap_plan_class;
    destination.sandbox = source.sandbox;
        // whether client uses account-based sandbox.  -1 = don't know
    destination.allow_multiple_clients = source.allow_multiple_clients;
        // whether client allows multiple clients per host, -1 don't know
    destination.using_weak_auth = source.using_weak_auth;
        // Request uses weak authenticator.
        // Don't modify user prefs or CPID
    destination.last_rpc_dayofyear = source.last_rpc_dayofyear;
    destination.current_rpc_dayofyear = source.current_rpc_dayofyear;

    if (!source.client_opaque.empty()){
    	//destination.client_opaque = source.client_opaque;
    }

    //@Host read information
    //add
    destination.host_availability_records = source.host_availability_records;
}

void SCHEDULER_REQUEST::clear(){
    //this->authenticator = '\0';
    //this->platform = 0;
    //this->alt_platforms = source.alt_platforms;
    //this->platforms = source.platforms;
    //this->cross_project_id = source.cross_project_id;
    this->hostid = 0;  // zero if first RPC

    //this->core_client_major_version = source.core_client_major_version;
    //this->core_client_minor_version = source.core_client_minor_version;
    //this->core_client_release = source.core_client_release;
    //this->core_client_version = core_client_version;    // 10000*major + 100*minor + release
    //this->rpc_seqno = source.rpc_seqno;
    //this->work_req_seconds = source.work_req_seconds;
        // in "normalized CPU seconds" (see work_req.php)
    //this->cpu_req_secs = source.cpu_req_secs;
    //this->cpu_req_instances = source.cpu_req_instances;
    //this->resource_share_fraction = source.resource_share_fraction;
        // this project's fraction of total resource share
    //this->rrs_fraction = source.rrs_fraction;
        // ... of runnable resource share
    //this->prrs_fraction = source.prrs_fraction;
        // ... of potentially runnable resource share
    //this->cpu_estimated_delay = source.cpu_estimated_delay;
        // currently queued jobs saturate the CPU for this long;
        // used for crude deadline check
    //this->duration_correction_factor = source.duration_correction_factor;
    //this->global_prefs_xml = source.global_prefs_xml;
    //this->working_global_prefs_xml = source.working_global_prefs_xml;
    //this->code_sign_key = source.code_sign_key;

    this->client_app_versions.clear();

    //this->global_prefs = source.global_prefs;
    //this->global_prefs_source_email_hash = source.global_prefs_source_email_hash;

    //this->host = source.host;      // request message is parsed into here.
                    // does NOT contain the full host record.
    //this->coprocs = source.coprocs;
    this->results.clear();
        // completed results being reported
    this->file_xfer_results.clear();
    this->msgs_from_host.clear();
    this->file_infos.clear();
        // sticky files reported by host for locality scheduling
    this->file_delete_candidates.clear();
        // sticky files reported by host, deletion candidates
    this->files_not_needed.clear();
        // sticky files reported by host, no longer needed
    this->other_results.clear();
        // in-progress results from this project
    this->ip_results.clear();
        // in-progress results from all projects
    this->have_other_results_list = false;
    this->have_ip_results_list = false;
    this->have_time_stats_log = false;
    this->client_cap_plan_class = false;
    //this->sandbox = source.sandbox;
        // whether client uses account-based sandbox.  -1 = don't know
    //this->allow_multiple_clients = source.allow_multiple_clients;
        // whether client allows multiple clients per host, -1 don't know
    this->using_weak_auth = false;
        // Request uses weak authenticator.
        // Don't modify user prefs or CPID
    //this->last_rpc_dayofyear = source.last_rpc_dayofyear;
    //this->current_rpc_dayofyear = source.current_rpc_dayofyear;
    //this->client_opaque = source.client_opaque;

    //@Host read information
    //add
    this->host_availability_records.clear();
}


const char* SCHEDULER_REQUEST::parse(XML_PARSER& xp) {
    SCHED_DB_RESULT result;
    int retval;

    strcpy(authenticator, "");
    strcpy(platform.name, "");
    strcpy(cross_project_id, "");
    hostid = 0;
    core_client_major_version = 0;
    core_client_minor_version = 0;
    core_client_release = 0;
    core_client_version = 0;
    rpc_seqno = 0;
    work_req_seconds = 0;
    cpu_req_secs = 0;
    cpu_req_instances = 0;
    resource_share_fraction = 1.0;
    rrs_fraction = 1.0;
    prrs_fraction = 1.0;
    cpu_estimated_delay = 0;
    strcpy(global_prefs_xml, "");
    strcpy(working_global_prefs_xml, "");
    strcpy(code_sign_key, "");
    memset(&global_prefs, 0, sizeof(global_prefs));
    memset(&host, 0, sizeof(host));
    have_other_results_list = false;
    have_ip_results_list = false;
    have_time_stats_log = false;
    client_cap_plan_class = false;
    sandbox = -1;
    allow_multiple_clients = -1;

    if (xp.get_tag()) {
        return "xp.get_tag() failed";
    }
    if (xp.match_tag("?xml")) {
        xp.get_tag();
    }
    if (!xp.match_tag("scheduler_request")) return "no start tag";
    while (!xp.get_tag()) {
        if (xp.match_tag("/scheduler_request")) {	//End of request file

        	log_messages.printf(MSG_NORMAL,
        	            "Got the closing tag: %s \n", xp.parsed_tag
        	        );

            core_client_version = 10000*core_client_major_version + 100*core_client_minor_version + core_client_release;
            return NULL;
        }
        if (xp.parse_str("authenticator", authenticator, sizeof(authenticator))) {
            remove_quotes(authenticator);
            continue;
        }
        if (xp.parse_str("cross_project_id", cross_project_id, sizeof(cross_project_id))) continue;
        if (xp.parse_int("hostid", hostid)) continue;
        if (xp.parse_int("rpc_seqno", rpc_seqno)) continue;
        if (xp.parse_str("platform_name", platform.name, sizeof(platform.name))) continue;
        if (xp.match_tag("alt_platform")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing alt_platforms\n");

            CLIENT_PLATFORM cp;
            retval = cp.parse(xp);
            if (!retval) {
                alt_platforms.push_back(cp);
            }

            log_messages.printf(MSG_NORMAL,  "Parsing alt_platforms Complete\n");

            continue;
        }
        if (xp.match_tag("app_versions")) {
            while (!xp.get_tag()) {

            	log_messages.printf(MSG_NORMAL,  "Parsing app_versions\n");

                if (xp.match_tag("/app_versions")) break;
                if (xp.match_tag("app_version")) {
                    CLIENT_APP_VERSION cav;
                    retval = cav.parse(xp);
                    if (retval) {
                        if (!strcmp(platform.name, "anonymous")) {
                            if (retval == ERR_NOT_FOUND) {
                                g_reply->insert_message(
                                    _("Unknown app name in app_info.xml"),
                                    "notice"
                                );
                            } else {
                                g_reply->insert_message(
                                    _("Syntax error in app_info.xml"),
                                    "notice"
                                );
                            }
                        } else {
                            // this case happens if the app version
                            // refers to a deprecated app
                        }
                        cav.app = 0;
                    }
                    // store the CLIENT_APP_VERSION even if it didn't parse.
                    // This is necessary to maintain the correspondence
                    // with result.app_version
                    //
                    client_app_versions.push_back(cav);
                }
            }

            log_messages.printf(MSG_NORMAL,  "Parsing app_versions complete\n");

            continue;
        }
        if (xp.parse_int("core_client_major_version", core_client_major_version)) continue;
        if (xp.parse_int("core_client_minor_version", core_client_minor_version)) continue;
        if (xp.parse_int("core_client_release", core_client_release)) continue;
        if (xp.parse_double("work_req_seconds", work_req_seconds)) continue;
        if (xp.parse_double("cpu_req_secs", cpu_req_secs)) continue;
        if (xp.parse_double("cpu_req_instances", cpu_req_instances)) continue;
        if (xp.parse_double("resource_share_fraction", resource_share_fraction)) continue;
        if (xp.parse_double("rrs_fraction", rrs_fraction)) continue;
        if (xp.parse_double("prrs_fraction", prrs_fraction)) continue;
        if (xp.parse_double("estimated_delay", cpu_estimated_delay)) continue;
        if (xp.parse_double("duration_correction_factor", host.duration_correction_factor)) continue;
        if (xp.match_tag("global_preferences")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing global prefs\n");

            strcpy(global_prefs_xml, "<global_preferences>\n");
            char buf[BLOB_SIZE];
            retval = xp.element_contents(
                "</global_preferences>", buf, sizeof(buf)
            );
            if (retval) return "error copying global prefs";
            safe_strcat(global_prefs_xml, buf);
            safe_strcat(global_prefs_xml, "</global_preferences>\n");

            log_messages.printf(MSG_NORMAL,  "Parsing global prefs complete\n");

            continue;
        }
        if (xp.match_tag("working_global_preferences")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing working global prefs\n");

            retval = xp.element_contents(
                "</working_global_preferences>",
                working_global_prefs_xml,
                sizeof(working_global_prefs_xml)
            );
            if (retval) return "error copying working global prefs";

            log_messages.printf(MSG_NORMAL,  "Parsing working global prefs complete\n");

            continue;
        }
        if (xp.parse_str("global_prefs_source_email_hash", global_prefs_source_email_hash, sizeof(global_prefs_source_email_hash))) continue;
        if (xp.match_tag("host_info")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing host_info\n");

            host.parse(xp);

            log_messages.printf(MSG_NORMAL,  "Parsing global host_info complete\n");

            continue;
        }
        if (xp.match_tag("time_stats")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing time_stats\n");

            host.parse_time_stats(xp);

            log_messages.printf(MSG_NORMAL,  "Parsing time_stats complete\n");

            continue;
        }
        if (xp.match_tag("time_stats_log")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing time_stats_log\n");

            handle_time_stats_log(xp.f->f);
            have_time_stats_log = true;

            log_messages.printf(MSG_NORMAL,  "Parsing time_stats_log complete\n");

            continue;
        }
        if (xp.match_tag("net_stats")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing net_stats\n");

            host.parse_net_stats(xp);

            log_messages.printf(MSG_NORMAL,  "Parsing net_stats complete\n");

            continue;
        }
        if (xp.match_tag("disk_usage")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing disk_usage\n");

            host.parse_disk_usage(xp);

            log_messages.printf(MSG_NORMAL,  "Parsing disk_usage complete\n");

            continue;
        }
        if (xp.match_tag("result")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing result\n");

            retval = result.parse_from_client(xp);
            if (retval) continue;
            if (strstr(result.name, "download") || strstr(result.name, "upload")) {
                file_xfer_results.push_back(result);
                continue;
            }
#if 0   // enable if you need to limit CGI memory size
            if (results.size() >= 1024) {
                continue;
            }
#endif
            // check if client is sending the same result twice.
            // Shouldn't happen, but if it does bad things will happen
            //
            bool found = false;
            for (unsigned int i=0; i<results.size(); i++) {
                if (!strcmp(results[i].name, result.name)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                results.push_back(result);
            }

            log_messages.printf(MSG_NORMAL,  "Parsing result\n");

            continue;
        }
        if (xp.match_tag("code_sign_key")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing code_sign_key\n");

            copy_element_contents(xp.f->f, "</code_sign_key>", code_sign_key, sizeof(code_sign_key));
            strip_whitespace(code_sign_key);

            log_messages.printf(MSG_NORMAL,  "Parsing code_sign_key\n");

            continue;
        }
        if (xp.match_tag("msg_from_host")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing msg_from_host\n");

            MSG_FROM_HOST_DESC md;
            retval = md.parse(xp);
            if (!retval) {
                msgs_from_host.push_back(md);
            }

            log_messages.printf(MSG_NORMAL,  "Parsing msg_from_host complete\n");

            continue;
        }
        if (xp.match_tag("file_info")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing file_info\n");

            FILE_INFO fi;
            retval = fi.parse(xp);
            if (!retval) {
                file_infos.push_back(fi);
            }

            log_messages.printf(MSG_NORMAL,  "Parsing file_info complete\n");

            continue;
        }
        if (xp.match_tag("host_venue")) {
            continue;
        }
        if (xp.match_tag("other_results")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing other_results\n");

            have_other_results_list = true;
            while (!xp.get_tag()) {
                if (xp.match_tag("/other_results")) break;
                if (xp.match_tag("other_result")) {
                    OTHER_RESULT o_r;
                    retval = o_r.parse(xp);
                    if (!retval) {
                        other_results.push_back(o_r);
                    }
                }
            }

            log_messages.printf(MSG_NORMAL,  "Parsing other_results complete\n");

            continue;
        }
        if (xp.match_tag("in_progress_results")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing ip_results\n");

            have_ip_results_list = true;
            int i = 0;
            double now = time(0);
            while (!xp.get_tag()) {
                if (xp.match_tag("/in_progress_results")) break;
                if (xp.match_tag("ip_result")) {
                    IP_RESULT ir;
                    retval = ir.parse(xp);
                    if (!retval) {
                        if (!strlen(ir.name)) {
                            sprintf(ir.name, "ip%d", i++);
                        }
                        ir.report_deadline -= now;
                        ip_results.push_back(ir);
                    }
                }
            }

            log_messages.printf(MSG_NORMAL,  "Parsing ip_results complete\n");

            continue;
        }
        if (xp.match_tag("coprocs")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing coprocs\n");

            coprocs.parse(xp);

            log_messages.printf(MSG_NORMAL,  "Parsing coprocs complete\n");

            continue;
        }
        if (xp.parse_bool("client_cap_plan_class", client_cap_plan_class)) continue;
        if (xp.parse_int("sandbox", sandbox)) continue;
        if (xp.parse_int("allow_multiple_clients", allow_multiple_clients)) continue;
        if (xp.parse_string("client_opaque", client_opaque)) continue;

        if (xp.match_tag("active_task_set")) continue;
        if (xp.match_tag("app")) continue;
        if (xp.match_tag("app_version")) continue;
        if (xp.match_tag("duration_variability")) continue;
        if (xp.match_tag("new_version_check_time")) continue;
        if (xp.match_tag("newer_version")) continue;
        if (xp.match_tag("project")) continue;
        if (xp.match_tag("project_files")) continue;
        if (xp.match_tag("proxy_info")) continue;
        if (xp.match_tag("user_network_request")) continue;
        if (xp.match_tag("user_run_request")) continue;
        if (xp.match_tag("master_url")) continue;
        if (xp.match_tag("project_name")) continue;
        if (xp.match_tag("user_name")) continue;
        if (xp.match_tag("team_name")) continue;
        if (xp.match_tag("email_hash")) continue;
        if (xp.match_tag("user_total_credit")) continue;
        if (xp.match_tag("user_expavg_credit")) continue;
        if (xp.match_tag("user_create_time")) continue;
        if (xp.match_tag("host_total_credit")) continue;
        if (xp.match_tag("host_expavg_credit")) continue;
        if (xp.match_tag("host_create_time")) continue;
        if (xp.match_tag("nrpc_failures")) continue;
        if (xp.match_tag("master_fetch_failures")) continue;
        if (xp.match_tag("min_rpc_time")) continue;
        if (xp.match_tag("short_term_debt")) continue;
        if (xp.match_tag("long_term_debt")) continue;
        if (xp.match_tag("resource_share")) continue;
        if (xp.match_tag("scheduler_url")) continue;
        if (xp.match_tag("/project")) continue;
        if (xp.match_tag("?xml")) continue;

        if (xp.match_tag("host_availability")) {

        	log_messages.printf(MSG_NORMAL,  "Parsing host availability\n");

				void* xresult = AVAILABILITY_RECORD::parse(xp);

				if(xresult == NULL) {
					//error
				}else{

					int integerResult = *((int*)xresult);

					if (integerResult == ERR_XML_PARSE) {
						return "failure reading host information";
					}else{
						//convert pointer to results array
						//add array of readings to list
						 this->host_availability_records = *((std::vector<AVAILABILITY_RECORD>*)xresult);
					}
				}
				log_messages.printf(MSG_NORMAL,  "Parsing host availability complete\n");
				continue;
        }

        log_messages.printf(MSG_NORMAL,
            "SCHEDULER_REQUEST::parse(): unexpected: %s\n", xp.parsed_tag
        );
        xp.skip_unexpected();
    }

    log_messages.printf(MSG_NORMAL,
        "FAILED XP1. Parsed tag: %s\n", xp.parsed_tag
    );

    return "no end tag ";
}


// I'm not real sure why this is here.
// Why not copy the request message directly?
//
int SCHEDULER_REQUEST::write(FILE* fout) {
    unsigned int i;

    fprintf(fout,
        "<scheduler_request>\n"
        "  <authenticator>%s</authentiicator>\n"
        "  <platform_name>%s</platform_name>\n"
        "  <cross_project_id>%s</cross_project_id>\n"
        "  <hostid>%d</hostid>\n"
        "  <core_client_major_version>%d</core_client_major_version>\n"
        "  <core_client_minor_version>%d</core_client_minor_version>\n"
        "  <core_client_release>%d</core_client_release>\n"
        "  <rpc_seqno>%d</rpc_seqno>\n"
        "  <work_req_seconds>%.15f</work_req_seconds>\n"
        "  <resource_share_fraction>%.15f</resource_share_fraction>\n"
        "  <rrs_fraction>%.15f</rrs_fraction>\n"
        "  <prrs_fraction>%.15f</prrs_fraction>\n"
        "  <estimated_delay>%.15f</estimated_delay>\n"
        "  <code_sign_key>%s</code_sign_key>\n"
        "  <anonymous_platform>%s</anonymous_platform>\n",
        authenticator,
        platform.name,
        cross_project_id,
        hostid,
        core_client_major_version,
        core_client_minor_version,
        core_client_release,
        rpc_seqno,
        work_req_seconds,
        resource_share_fraction,
        rrs_fraction,
        prrs_fraction,
        cpu_estimated_delay,
        code_sign_key,
        is_anonymous(platforms.list[0])?"true":"false"
    );

    for (i=0; i<client_app_versions.size(); i++) {
        fprintf(fout,
            "  <app_version>\n"
            "    <app_name>%s</app_name>\n"
            "    <version_num>%d</version_num>\n"
            "  </app_version>\n",
            client_app_versions[i].app_name,
            client_app_versions[i].version_num
        );
    }

    fprintf(fout,
        "  <global_prefs_xml>\n"
        "    %s"
        "  </globals_prefs_xml>\n",
        global_prefs_xml
    );
  
    fprintf(fout,
        "  <global_prefs_source_email_hash>%s</global_prefs_source_email_hash>\n",
        global_prefs_source_email_hash
    );
  
    fprintf(fout,
        "  <host>\n"
        "    <id>%d</id>\n"
        "    <rpc_time>%d</rpc_time>\n"
        "    <timezone>%d</timezone>\n"
        "    <d_total>%.15f</d_total>\n"
        "    <d_free>%.15f</d_free>\n"
        "    <d_boinc_used_total>%.15f</d_boinc_used_total>\n"
        "    <d_boinc_used_project>%.15f</d_boinc_used_project>\n"
        "    <d_boinc_max>%.15f</d_boinc_max>\n",
        host.id,
        host.rpc_time,
        host.timezone,
        host.d_total,
        host.d_free,
        host.d_boinc_used_total,
        host.d_boinc_used_project,
        host.d_boinc_max
    );

    for (i=0; i<results.size(); i++) {
        fprintf(fout,
            "  <result>\n"
            "    <name>%s</name>\n"
            "    <client_state>%d</client_state>\n"
            "    <cpu_time>%.15f</cpu_time>\n"
            "    <exit_status>%d</exit_status>\n"
            "    <app_version_num>%d</app_version_num>\n"
            "  </result>\n",
            results[i].name,
            results[i].client_state,
            results[i].cpu_time,
            results[i].exit_status,
            results[i].app_version_num
        );
    }
  
    for (i=0; i<msgs_from_host.size(); i++) {
        fprintf(fout,
            "  <msg_from_host>\n"
            "    <variety>%s</variety>\n"
            "    <msg_text>%s</msg_text>\n"
            "  </msg_from_host>\n",
            msgs_from_host[i].variety,
            msgs_from_host[i].msg_text.c_str()
        );
    }

    for (i=0; i<file_infos.size(); i++) {
        fprintf(fout,
            "  <file_info>\n"
            "    <name>%s</name>\n"
            "  </file_info>\n",
            file_infos[i].name
        );
        fprintf(fout, "</scheduler_request>\n");
    }
    return 0;
}

int MSG_FROM_HOST_DESC::parse(XML_PARSER& xp) {
    char buf[256];

    msg_text = "";
    MIOFILE& in = *(xp.f);
    while (in.fgets(buf, sizeof(buf))) {
        if (match_tag(buf, "</msg_from_host>")) return 0;
        if (parse_str(buf, "<variety>", variety, sizeof(variety))) continue;
        msg_text += buf;
    }
    return ERR_XML_PARSE;
}

SCHEDULER_REPLY::SCHEDULER_REPLY() {
    memset(&wreq, 0, sizeof(wreq));
    memset(&disk_limits, 0, sizeof(disk_limits));
    request_delay = 0;
    hostid = 0;
    send_global_prefs = false;
    strcpy(code_sign_key, "");
    strcpy(code_sign_key_signature, "");
    memset(&user, 0, sizeof(user));
    memset(&host, 0, sizeof(host));
    memset(&team, 0, sizeof(team));
    nucleus_only = false;
    project_is_down = false;
    send_msg_ack = false;
    strcpy(email_hash, "");
}

SCHEDULER_REPLY::~SCHEDULER_REPLY() {
}

int SCHEDULER_REPLY::write(FILE* fout, SCHEDULER_REQUEST& sreq) {
    unsigned int i;
    char buf[BLOB_SIZE];

    // Note: at one point we had
    // "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n"
    // after the Content-type (to make it legit XML),
    // but this broke 4.19 clients
    //
    fprintf(fout,
        "Content-type: text/xml\n\n"
        "<scheduler_reply>\n"
        "<scheduler_version>%d</scheduler_version>\n",
        BOINC_MAJOR_VERSION*100+BOINC_MINOR_VERSION
    );
    if (strlen(config.master_url)) {
        fprintf(fout,
            "<master_url>%s</master_url>\n",
            config.master_url
        );
    }
    if (config.ended) {
        fprintf(fout, "   <ended>1</ended>\n");
    }

    // if the scheduler has requested a delay OR the sysadmin has configured
    // the scheduler with a minimum time between RPCs, send a delay request.
    // Make it 1% larger than the min required to take care of time skew.
    // If this is less than one second bigger, bump up by one sec.
    //
    if (request_delay || config.min_sendwork_interval) {
        double min_delay_needed = 1.01*config.min_sendwork_interval;
        if (min_delay_needed < config.min_sendwork_interval+1) {
            min_delay_needed = config.min_sendwork_interval+1;
        }
        if (request_delay<min_delay_needed) {
            request_delay = min_delay_needed; 
        }
        fprintf(fout, "<request_delay>%f</request_delay>\n", request_delay);
    }
    log_messages.printf(MSG_NORMAL,
        "Sending reply to [HOST#%d]: %d results, delay req %.2f\n",
        host.id, wreq.njobs_sent, request_delay
    );

    if (sreq.core_client_version <= 41900) {
        string msg;
        string pri = "low";
        for (i=0; i<messages.size(); i++) {
            USER_MESSAGE& um = messages[i];
            msg += um.message + string(" ");
            if (um.priority == "notice") {
                pri = "notice";
            }
        }
        if (messages.size()>0) {
            // any newlines will break message printing under 4.19 and under!
            // replace them with spaces.
            //
            while (1) {
                string::size_type pos = msg.find("\n", 0);
                if (pos == string::npos) break;
                msg.replace(pos, 1, " ");
            }
            fprintf(fout,
                "<message priority=\"%s\">%s</message>\n",
                pri.c_str(), msg.c_str()
            );
        }
    } else if (sreq.core_client_version <= 61100) {
        char prio[256];
        for (i=0; i<messages.size(); i++) {
            USER_MESSAGE& um = messages[i];
            strcpy(prio, um.priority.c_str());
            if (!strcmp(prio, "notice")) {
                strcpy(prio, "high");
            }
            fprintf(fout,
                "<message priority=\"%s\">%s</message>\n",
                prio,
                um.message.c_str()
            );
        }
    } else {
        for (i=0; i<messages.size(); i++) {
            USER_MESSAGE& um = messages[i];
            fprintf(fout,
                "<message priority=\"%s\">%s</message>\n",
                um.priority.c_str(),
                um.message.c_str()
            );
        }
    }
    fprintf(fout,
        "<project_name>%s</project_name>\n",
        config.long_name
    );

    if (config.request_time_stats_log) {
        if (!have_time_stats_log()) {
            fprintf(fout, "<send_time_stats_log>1</send_time_stats_log>\n");
        }
    }

    if (project_is_down) {
        fprintf(fout,"<project_is_down/>\n");
        goto end;
    }
    if (config.workload_sim) {
        fprintf(fout, "<send_full_workload/>\n");
    }

    if (nucleus_only) goto end;

    if (strlen(config.symstore)) {
        fprintf(fout, "<symstore>%s</symstore>\n", config.symstore);
    }
    if (config.next_rpc_delay) {
        fprintf(fout, "<next_rpc_delay>%f</next_rpc_delay>\n", config.next_rpc_delay);
    }
    if (user.id) {
        xml_escape(user.name, buf, sizeof(buf));
        fprintf(fout,
            "<userid>%d</userid>\n"
            "<user_name>%s</user_name>\n"
            "<user_total_credit>%f</user_total_credit>\n"
            "<user_expavg_credit>%f</user_expavg_credit>\n"
            "<user_create_time>%d</user_create_time>\n",
            user.id,
            buf,
            user.total_credit,
            user.expavg_credit,
            user.create_time
        );
        // be paranoid about the following to avoid sending null
        //
        if (strlen(email_hash)) {
            fprintf(fout,
                "<email_hash>%s</email_hash>\n",
                email_hash
            );
        }
        if (strlen(user.cross_project_id)) {
            fprintf(fout,
                "<cross_project_id>%s</cross_project_id>\n",
                user.cross_project_id
            );
        }

        if (send_global_prefs) {
            fputs(user.global_prefs, fout);
            fputs("\n", fout);
        }

        // always send project prefs
        //
        fputs(user.project_prefs, fout);
        fputs("\n", fout);

    }
    if (hostid) {
        fprintf(fout,
            "<hostid>%d</hostid>\n",
            hostid
        );
    }
    fprintf(fout,
        "<host_total_credit>%f</host_total_credit>\n"
        "<host_expavg_credit>%f</host_expavg_credit>\n"
        "<host_venue>%s</host_venue>\n"
        "<host_create_time>%d</host_create_time>\n",
        host.total_credit,
        host.expavg_credit,
        host.venue,
        host.create_time
    );

    // might want to send team credit too.
    //
    if (team.id) {
        xml_escape(team.name, buf, sizeof(buf));
        fprintf(fout,
            "<teamid>%d</teamid>\n"
            "<team_name>%s</team_name>\n",
            team.id,
            buf
        );
    } else {
        fprintf(fout,
            "<team_name></team_name>\n"
        );
    }

    // acknowledge results
    //
    for (i=0; i<result_acks.size(); i++) {
        fprintf(fout,
            "<result_ack>\n"
            "    <name>%s</name>\n"
            "</result_ack>\n",
            result_acks[i].c_str()
        );
    }

    // abort results
    //
    for (i=0; i<result_aborts.size(); i++) {
        fprintf(fout,
            "<result_abort>\n"
            "    <name>%s</name>\n"
            "</result_abort>\n",
            result_aborts[i].c_str()
        );
    }

    // abort results not started
    //
    for (i=0; i<result_abort_if_not_starteds.size(); i++) {
        fprintf(fout,
            "<result_abort_if_not_started>\n"
            "    <name>%s</name>\n"
            "</result_abort_if_not_started>\n",
            result_abort_if_not_starteds[i].c_str()
        );
    }

    for (i=0; i<apps.size(); i++) {
        apps[i].write(fout);
    }

    for (i=0; i<app_versions.size(); i++) {
        app_versions[i].write(fout);
    }

    for (i=0; i<wus.size(); i++) {
        fputs(wus[i].xml_doc, fout);
        fputs("\n", fout);  // for old clients
    }

    for (i=0; i<results.size(); i++) {
        results[i].write_to_client(fout);
    }

    if (strlen(code_sign_key)) {
        fputs("<code_sign_key>\n", fout);
        fputs(code_sign_key, fout);
        fputs("\n</code_sign_key>\n", fout);
    }

    if (strlen(code_sign_key_signature)) {
        fputs("<code_sign_key_signature>\n", fout);
        fputs(code_sign_key_signature, fout);
        fputs("</code_sign_key_signature>\n", fout);
    }

    if (send_msg_ack) {
        fputs("<message_ack/>\n", fout);
    }

    for (i=0; i<msgs_to_host.size(); i++) {
        MSG_TO_HOST& md = msgs_to_host[i];
        fprintf(fout, "%s\n", md.xml);
    }

    if (config.non_cpu_intensive) {
        fprintf(fout, "<non_cpu_intensive/>\n");
    }

    if (config.verify_files_on_app_start) {
        fprintf(fout, "<verify_files_on_app_start/>\n");
    }

    for (i=0; i<file_deletes.size(); i++) {
        fprintf(fout,
            "<delete_file_info>%s</delete_file_info>\n",
            file_deletes[i].name
        );
    }

    fprintf(fout,
        "<no_cpu_apps>%d</no_cpu_apps>\n"
        "<no_cuda_apps>%d</no_cuda_apps>\n"
        "<no_ati_apps>%d</no_ati_apps>\n",
        ssp->have_cpu_apps?0:1,
        ssp->have_cuda_apps?0:1,
        ssp->have_ati_apps?0:1
    );
    gui_urls.get_gui_urls(user, host, team, buf);
    fputs(buf, fout);
    if (project_files.text) {
        fputs(project_files.text, fout);
        fprintf(fout, "\n");
    }

end:
    fprintf(fout,
        "</scheduler_reply>\n"
    );
    return 0;
}

// set delay to the MAX of the existing value or the requested value
// never send a delay request longer than two days.
//
void SCHEDULER_REPLY::set_delay(double delay) {
    if (request_delay < delay) {
        request_delay = delay;
    }
    if (request_delay > DELAY_MAX) {
        request_delay = DELAY_MAX;
    }
} 


void SCHEDULER_REPLY::insert_app_unique(APP& app) {
    unsigned int i;
    for (i=0; i<apps.size(); i++) {
        if (app.id == apps[i].id) return;
    }
    apps.push_back(app);
}

void SCHEDULER_REPLY::insert_app_version_unique(APP_VERSION& av) {
    unsigned int i;
    for (i=0; i<app_versions.size(); i++) {
        if (av.id == app_versions[i].id) return;
    }
    app_versions.push_back(av);
}

void SCHEDULER_REPLY::insert_workunit_unique(WORKUNIT& wu) {
    unsigned int i;
    for (i=0; i<wus.size(); i++) {
        if (wu.id == wus[i].id) return;
    }
    wus.push_back(wu);
}

void SCHEDULER_REPLY::insert_result(SCHED_DB_RESULT& result) {
    results.push_back(result);
}

void SCHEDULER_REPLY::insert_message(const char* msg, const char* prio) {
    messages.push_back(USER_MESSAGE(msg, prio));
}

void SCHEDULER_REPLY::insert_message(USER_MESSAGE& um) {
    messages.push_back(um);
}

USER_MESSAGE::USER_MESSAGE(const char* m, const char* p) {
    if (g_request->core_client_version < 61200) {
        char buf[1024];
        strcpy(buf, m);
        strip_translation(buf);
        message = buf;
    } else {
        message = m;
    }
    priority = p;
}

int APP::write(FILE* fout) {
    fprintf(fout,
        "<app>\n"
        "    <name>%s</name>\n"
        "    <user_friendly_name>%s</user_friendly_name>\n"
        "</app>\n",
        name, user_friendly_name
    );
    return 0;
}

int APP_VERSION::write(FILE* fout) {
    char buf[APP_VERSION_XML_BLOB_SIZE];

    strcpy(buf, xml_doc);
    char* p = strstr(buf, "</app_version>");
    if (!p) {
        fprintf(stderr, "ERROR: app version %d XML has no end tag!\n", id);
        return -1;
    }
    *p = 0;
    fputs(buf, fout);
    PLATFORM* pp = ssp->lookup_platform_id(platformid);
    fprintf(fout, "    <platform>%s</platform>\n", pp->name);
    if (strlen(plan_class)) {
        fprintf(fout, "    <plan_class>%s</plan_class>\n", plan_class);
    }
    fprintf(fout,
        "    <avg_ncpus>%f</avg_ncpus>\n"
        "    <max_ncpus>%f</max_ncpus>\n"
        "    <flops>%f</flops>\n",
        bavp->host_usage.avg_ncpus,
        bavp->host_usage.max_ncpus,
        bavp->host_usage.projected_flops
    );
    if (strlen(bavp->host_usage.cmdline)) {
        fprintf(fout,
            "    <cmdline>%s</cmdline>\n",
            bavp->host_usage.cmdline
        );
    }
    if (bavp->host_usage.ncudas) {
        fprintf(fout,
            "    <coproc>\n"
            "        <type>CUDA</type>\n"
            "        <count>%f</count>\n"
            "    </coproc>\n",
            bavp->host_usage.ncudas
        );
    }
    if (bavp->host_usage.natis) {
        fprintf(fout,
            "    <coproc>\n"
            "        <type>ATI</type>\n"
            "        <count>%f</count>\n"
            "    </coproc>\n",
            bavp->host_usage.natis
        );
    }
    if (bavp->host_usage.gpu_ram) {
        fprintf(fout,
            "    <gpu_ram>%f</gpu_ram>\n",
            bavp->host_usage.gpu_ram
        );
    }
    fputs("</app_version>\n", fout);
    return 0;
}

int SCHED_DB_RESULT::write_to_client(FILE* fout) {
    char buf[BLOB_SIZE];

    strcpy(buf, xml_doc_in);
    char* p = strstr(buf, "</result>");
    if (!p) {
        fprintf(stderr, "ERROR: result %d XML has no end tag!\n", id);
        return -1;
    }
    *p = 0;
    fputs(buf, fout);
    fputs("\n", fout);  // for old clients

    APP_VERSION* avp = bav.avp;
    CLIENT_APP_VERSION* cavp = bav.cavp;
    if (avp) {
        PLATFORM* pp = ssp->lookup_platform_id(avp->platformid);
        fprintf(fout,
            "    <platform>%s</platform>\n"
            "    <version_num>%d</version_num>\n"
            "    <plan_class>%s</plan_class>\n",
            pp->name, avp->version_num, avp->plan_class
        );
    } else if (cavp) {
        fprintf(fout,
            "    <platform>%s</platform>\n"
            "    <version_num>%d</version_num>\n"
            "    <plan_class>%s</plan_class>\n",
            cavp->platform, cavp->version_num, cavp->plan_class
        );
    }

    fputs("</result>\n", fout);
    return 0;
}

int SCHED_DB_RESULT::parse_from_client(XML_PARSER& xp) {
    double dtemp;
    bool btemp;
    string stemp;
    int itemp;

    // should be non-zero if exit_status is not found
    exit_status = ERR_NO_EXIT_STATUS;
    memset(this, 0, sizeof(*this));
    while (!xp.get_tag()) {
        if (xp.match_tag("/result")) {
            return 0;
        }
        if (xp.parse_str("name", name, sizeof(name))) continue;
        if (xp.parse_int("state", client_state)) continue;
        if (xp.parse_double("final_cpu_time", cpu_time)) continue;
        if (xp.parse_double("final_elapsed_time", elapsed_time)) continue;
        if (xp.parse_int("exit_status", exit_status)) continue;
        if (xp.parse_int("app_version_num", app_version_num)) continue;
        if (xp.match_tag("file_info")) {
            string s;
            xp.copy_element(s);
            safe_strcat(xml_doc_out, s.c_str());
            continue;
        }
        if (xp.match_tag("stderr_out" )) {
            copy_element_contents(xp.f->f, "</stderr_out>", stderr_out, sizeof(stderr_out));
            continue;
        }
        if (xp.parse_string("platform", stemp)) continue;
        if (xp.parse_int("version_num", itemp)) continue;
        if (xp.parse_string("plan_class", stemp)) continue;
        if (xp.parse_double("completed_time", dtemp)) continue;
        if (xp.parse_string("file_name", stemp)) continue;
        if (xp.match_tag("file_ref")) {
            xp.copy_element(stemp);
            continue;
        }
        if (xp.parse_string("open_name", stemp)) continue;
        if (xp.parse_bool("ready_to_report", btemp)) continue;
        if (xp.parse_double("report_deadline", dtemp)) continue;
        if (xp.parse_string("wu_name", stemp)) continue;

        // deprecated stuff
        if (xp.parse_double("fpops_per_cpu_sec", dtemp)) continue;
        if (xp.parse_double("fpops_cumulative", dtemp)) continue;
        if (xp.parse_double("intops_per_cpu_sec", dtemp)) continue;
        if (xp.parse_double("intops_cumulative", dtemp)) continue;

        log_messages.printf(MSG_NORMAL,
            "RESULT::parse_from_client(): unrecognized: %s\n",
            xp.parsed_tag
        );
    }
    return ERR_XML_PARSE;
}

int HOST::parse(XML_PARSER& xp) {
    p_ncpus = 1;
    double dtemp;
    string stemp;
    while (!xp.get_tag()) {
        if (xp.match_tag("/host_info")) return 0;
        if (xp.parse_int("timezone", timezone)) continue;
        if (xp.parse_str("domain_name", domain_name, sizeof(domain_name))) continue;
        if (xp.parse_str("ip_addr", last_ip_addr, sizeof(last_ip_addr))) continue;
        if (xp.parse_str("host_cpid", host_cpid, sizeof(host_cpid))) continue;
        if (xp.parse_int("p_ncpus", p_ncpus)) continue;
        if (xp.parse_str("p_vendor", p_vendor, sizeof(p_vendor))) continue;
        if (xp.parse_str("p_model", p_model, sizeof(p_model))) continue;
        if (xp.parse_double("p_fpops", p_fpops)) continue;
        if (xp.parse_double("p_iops", p_iops)) continue;
        if (xp.parse_double("p_membw", p_membw)) continue;
        if (xp.parse_str("os_name", os_name, sizeof(os_name))) continue;
        if (xp.parse_str("os_version", os_version, sizeof(os_version))) continue;
        if (xp.parse_double("m_nbytes", m_nbytes)) continue;
        if (xp.parse_double("m_cache", m_cache)) continue;
        if (xp.parse_double("m_swap", m_swap)) continue;
        if (xp.parse_double("d_total", d_total)) continue;
        if (xp.parse_double("d_free", d_free)) continue;
        if (xp.parse_double("n_bwup", n_bwup)) continue;
        if (xp.parse_double("n_bwdown", n_bwdown)) continue;
        if (xp.parse_str("p_features", p_features, sizeof(p_features))) continue;
        if (xp.parse_str("virtualbox_version", virtualbox_version, sizeof(virtualbox_version))) continue;
        if (xp.parse_bool("p_vm_extensions_disabled", p_vm_extensions_disabled)) continue;

        // parse deprecated fields to avoid error messages
        //
        if (xp.parse_double("p_calculated", dtemp)) continue;
        if (xp.match_tag("p_fpop_err")) continue;
        if (xp.match_tag("p_iop_err")) continue;
        if (xp.match_tag("p_membw_err")) continue;

        // fields reported by 5.5+ clients, not currently used
        //
        if (xp.parse_string("p_capabilities", stemp)) continue;
        if (xp.parse_string("accelerators", stemp)) continue;

#if 1
        // not sure where these fields belong in the above categories
        //
        if (xp.parse_string("cpu_caps", stemp)) continue;
        if (xp.parse_string("cache_l1", stemp)) continue;
        if (xp.parse_string("cache_l2", stemp)) continue;
        if (xp.parse_string("cache_l3", stemp)) continue;
#endif

        log_messages.printf(MSG_NORMAL,
            "HOST::parse(): unrecognized: %s\n", xp.parsed_tag
        );
    }
    return ERR_XML_PARSE;
}


int HOST::parse_time_stats(XML_PARSER& xp) {
    while (!xp.get_tag()) {
        if (xp.match_tag("/time_stats")) return 0;
        if (xp.parse_double("on_frac", on_frac)) continue;
        if (xp.parse_double("connected_frac", connected_frac)) continue;
        if (xp.parse_double("active_frac", active_frac)) continue;
#if 0
        if (xp.match_tag("outages")) continue;
        if (xp.match_tag("outage")) continue;
        if (xp.match_tag("start")) continue;
        if (xp.match_tag("end")) continue;
        log_messages.printf(MSG_NORMAL,
            "HOST::parse_time_stats(): unrecognized: %s\n",
            xp.parsed_tag
        );
#endif
    }
    return ERR_XML_PARSE;
}

int HOST::parse_net_stats(XML_PARSER& xp) {
    double dtemp;
    while (!xp.get_tag()) {
        if (xp.match_tag("/net_stats")) return 0;
        if (xp.parse_double("bwup", n_bwup)) continue;
        if (xp.parse_double("bwdown", n_bwdown)) continue;

        // items reported by 5.10+ clients, not currently used
        //
        if (xp.parse_double("avg_time_up", dtemp)) continue;
        if (xp.parse_double("avg_up", dtemp)) continue;
        if (xp.parse_double("avg_time_down", dtemp)) continue;
        if (xp.parse_double("avg_down", dtemp)) continue;

        log_messages.printf(MSG_NORMAL,
            "HOST::parse_net_stats(): unrecognized: %s\n",
            xp.parsed_tag
        );
    }
    return ERR_XML_PARSE;
}

int HOST::parse_disk_usage(XML_PARSER& xp) {
    while (!xp.get_tag()) {
        if (xp.match_tag("/disk_usage")) return 0;
        if (xp.parse_double("d_boinc_used_total", d_boinc_used_total)) continue;
        if (xp.parse_double("d_boinc_used_project", d_boinc_used_project)) continue;
        log_messages.printf(MSG_NORMAL,
            "HOST::parse_disk_usage(): unrecognized: %s\n",
            xp.parsed_tag
        );
    }
    return ERR_XML_PARSE;
}

void GLOBAL_PREFS::parse(const char* buf, const char* venue) {
    char buf2[BLOB_SIZE];
    double dtemp;

    defaults();

    if (parse_double(buf, "<mod_time>", mod_time)) {
        // mod_time is outside of venue
        if (mod_time > dtime()) mod_time = dtime();
    }
    extract_venue(buf, venue, buf2);
    parse_double(buf2, "<disk_max_used_gb>", disk_max_used_gb);
    parse_double(buf2, "<disk_max_used_pct>", disk_max_used_pct);
    parse_double(buf2, "<disk_min_free_gb>", disk_min_free_gb);
    parse_double(buf2, "<work_buf_min_days>", work_buf_min_days);
    if (parse_double(buf2, "<ram_max_used_busy_pct>", dtemp)) {
        ram_max_used_busy_frac = dtemp/100.;
    }
    if (parse_double(buf2, "<ram_max_used_idle_pct>", dtemp)) {
        ram_max_used_idle_frac = dtemp/100.;
    }
    parse_double(buf2, "<max_ncpus_pct>", max_ncpus_pct);
}

void GLOBAL_PREFS::defaults() {
    memset(this, 0, sizeof(GLOBAL_PREFS));
}

void GUI_URLS::init() {
    text = 0;
    read_file_malloc(config.project_path("gui_urls.xml"), text);
}

void GUI_URLS::get_gui_urls(USER& user, HOST& host, TEAM& team, char* buf) {
    bool found;
    char userid[256], teamid[256], hostid[256], weak_auth[256], rss_auth[256];
    strcpy(buf, "");
    if (!text) return;
    strcpy(buf, text);

    sprintf(userid, "%d", user.id);
    sprintf(hostid, "%d", host.id);
    if (user.teamid) {
        sprintf(teamid, "%d", team.id);
    } else {
        strcpy(teamid, "0");
        while (remove_element(buf, "<ifteam>", "</ifteam>")) {
            continue;
        }
    }

    get_weak_auth(user, weak_auth);
    get_rss_auth(user, rss_auth);
    while (1) {
        found = false;
        found |= str_replace(buf, "<userid/>", userid);
        found |= str_replace(buf, "<user_name/>", user.name);
        found |= str_replace(buf, "<hostid/>", hostid);
        found |= str_replace(buf, "<teamid/>", teamid);
        found |= str_replace(buf, "<team_name/>", team.name);
        found |= str_replace(buf, "<authenticator/>", user.authenticator);
        found |= str_replace(buf, "<weak_auth/>", weak_auth);
        found |= str_replace(buf, "<rss_auth/>", rss_auth);
        if (!found) break;
    }
}

void PROJECT_FILES::init() {
    text = 0;
    read_file_malloc(config.project_path("project_files.xml"), text);
}

void get_weak_auth(USER& user, char* buf) {
    char buf2[256], out[256];
    sprintf(buf2, "%s%s", user.authenticator, user.passwd_hash);
    md5_block((unsigned char*)buf2, strlen(buf2), out);
    sprintf(buf, "%d_%s", user.id, out);
}

void get_rss_auth(USER& user, char* buf) {
    char buf2[256], out[256];
    sprintf(buf2, "%s%s%s", user.authenticator, user.passwd_hash, "notify_rss");
    md5_block((unsigned char*)buf2, strlen(buf2), out);
    sprintf(buf, "%d_%s", user.id, out);
}

void read_host_app_versions() {
    DB_HOST_APP_VERSION hav;
    char clause[256];

    sprintf(clause, "where host_id=%d", g_reply->host.id);
    while (!hav.enumerate(clause)) {
        g_wreq->host_app_versions.push_back(hav);
    }
    g_wreq->host_app_versions_orig = g_wreq->host_app_versions;
}

DB_HOST_APP_VERSION* gavid_to_havp(int gavid) {
    for (unsigned int i=0; i<g_wreq->host_app_versions.size(); i++) {
        DB_HOST_APP_VERSION& hav = g_wreq->host_app_versions[i];
        if (hav.app_version_id == gavid) return &hav;
    }
    return NULL;
}

void write_host_app_versions() {
    for (unsigned int i=0; i<g_wreq->host_app_versions.size(); i++) {
        DB_HOST_APP_VERSION& hav = g_wreq->host_app_versions[i];
        DB_HOST_APP_VERSION& hav_orig = g_wreq->host_app_versions_orig[i];

        int retval = hav.update_scheduler(hav_orig);
        if (retval) {
            log_messages.printf(MSG_CRITICAL,
                "CRITICAL: hav.update_sched() error: %s\n", boincerror(retval)
            );
        }
    }
}

DB_HOST_APP_VERSION* BEST_APP_VERSION::host_app_version() {
    if (cavp) {
        return gavid_to_havp(
            generalized_app_version_id(host_usage.resource_type(), appid)
        );
    } else {
        return gavid_to_havp(avp->id);
    }
}

// return some HAV for which quota was exceeded
//
DB_HOST_APP_VERSION* quota_exceeded_version() {
    for (unsigned int i=0; i<g_wreq->host_app_versions.size(); i++) {
        DB_HOST_APP_VERSION& hav = g_wreq->host_app_versions[i];
        if (hav.daily_quota_exceeded) return &hav;
    }
    return NULL;
}

double capped_host_fpops() {
    double x = g_request->host.p_fpops;
    if (x <= 0) {
        return ssp->perf_info.host_fpops_50_percentile;
    }
    if (x > ssp->perf_info.host_fpops_95_percentile*1.1) {
        return ssp->perf_info.host_fpops_95_percentile*1.1;
    }
    return x;
}

//@ADDED

AVAILABILITY_RECORD::AVAILABILITY_RECORD(){
	this->hour = -1;
	this->minutes = -1;
	this->availability = -1;
}

AVAILABILITY_RECORD::AVAILABILITY_RECORD(const AVAILABILITY_RECORD& source){
	this->year = source.year;
	this->month = source.month;
	this->day = source.day;
	this->hour = source.hour;
	this->minutes = source.minutes;
	this->availability = source.availability;
}

AVAILABILITY_RECORD::AVAILABILITY_RECORD(int &year, int &month, int &day, int &hour, int &minutes, double &availability){

	this->year = year;
	this->month = month;
	this->day = day;
	this->hour = hour;
	this->minutes = minutes;
	this->availability = availability;
}

void* AVAILABILITY_RECORD::parse(XML_PARSER& xp){

    //int tempHour, tempMinutes;
    //double tempAvailability;
	AVAILABILITY_RECORD* tempReading;

    std::vector<AVAILABILITY_RECORD>* readings = new std::vector<AVAILABILITY_RECORD>();

    //for each "record" tag read hour, min and avail. store in struct. save to array

    while (!xp.get_tag()) {

        if (xp.match_tag("/host_availability")) {

        	//final procedures. return control by passing pointer to vector or NULL for failure

            return readings;
        }

        if (xp.match_tag("record")){

        	tempReading = AVAILABILITY_RECORD::parse_record(xp);

        	if(tempReading == NULL){
        		return (void*)ERR_XML_PARSE;
        	}else{
        		readings->push_back(*tempReading);
        	}
        	continue;
        }

        //should not reach here, if everything else (above) is correct
        log_messages.printf(MSG_NORMAL,
            "ThermalReading::parse(): unrecognized: %s\n",
            xp.parsed_tag
        );
    }

    return (void*)ERR_XML_PARSE;
}

AVAILABILITY_RECORD* AVAILABILITY_RECORD::parse_record(XML_PARSER& xp){

    int tempHour, tempMinutes;
    double tempAvailability;
    int tempday, tempmonth, tempyear;

    AVAILABILITY_RECORD* tempReading;
    //read record, return object

	tempHour = tempMinutes = -1;
	tempAvailability = -1;
	tempday = tempmonth = tempyear = -1;

    while (!xp.get_tag()) {

        if (xp.match_tag("/record")) {

        	//final procedures. return control by passing pointer to object or NULL for failure
        	//modify: check if values are different from -1; add to object; clean variables (assign -1); return control
        	if (tempHour == -1 || tempMinutes == -1 || tempAvailability == -1 || tempday == -1 || tempmonth == -1 || tempyear == -1){

        		tempHour = tempMinutes = -1;
        		tempAvailability = -1;
        		tempday = tempmonth = tempyear = -1;

        		//TODO: INCOMPLETE RECORD ERROR (ADD TO LIST OF ERRORS)
        		return NULL;

        	}else{

        		tempReading = new AVAILABILITY_RECORD(tempyear,
        				tempmonth,
        				tempday,
        				tempHour,
        				tempMinutes,
        				tempAvailability);

        		//reset values
        		tempHour = tempMinutes = -1;
        		tempAvailability = -1;
        		tempday = tempmonth = tempyear = -1;

        		return tempReading;
        	}
        }

        if (xp.parse_double("availability", tempAvailability)) continue;

        if(xp.match_tag("time")){
        	while(!xp.get_tag()){
        		if (xp.match_tag("/time")){
        			break;
        		}
        		if(xp.parse_int("hour", tempHour))continue;
        		if(xp.parse_int("minute", tempMinutes)) continue;
        	}
        	continue;
        }

        if(xp.match_tag("date")){
        	while(!xp.get_tag()){
        		if (xp.match_tag("/date")){
        			break;
        		}
        		if(xp.parse_int("day", tempday))continue;
        		if(xp.parse_int("month", tempmonth)) continue;
        		if(xp.parse_int("year", tempyear))continue;
        	}
        	continue;
        }

        //should not reach here, if everything else (above) is correct
        log_messages.printf(MSG_NORMAL,
            "AVAILABILITY_RECORD::parse_record(): unrecognized: %s\n", xp.parsed_tag
        );
    }

    return NULL;
}

//EXTERNAL SCHEDULER

const std::string EXTERNAL_SCHEDULER_MANAGER::WQ_FILE = "_WQ";
const std::string EXTERNAL_SCHEDULER_MANAGER::WQ_LOCK_FILE = "_WQ.lock";
const std::string EXTERNAL_SCHEDULER_MANAGER::XS_LOCK_FILE = "_XS.lock";
const std::string EXTERNAL_SCHEDULER_MANAGER::XS_DIRECTORY = "xscheduler";
const std::string EXTERNAL_SCHEDULER_MANAGER::LAST_XS_RUN = "xs_last_run";

const std::string EXTERNAL_SCHEDULER_MANAGER::xsm_output_data_file = "xs_input.xml";
const std::string EXTERNAL_SCHEDULER_MANAGER::xsm_input_data_file = "xs_output.xml";


bool EXTERNAL_SCHEDULER_MANAGER::initialize()
{
	log_messages.printf(MSG_NORMAL,
			"[XSM] Initializing external scheduler manager\n");


	return (config.use_xs && this->file_exists(config.xs_executable));
}

bool EXTERNAL_SCHEDULER_MANAGER::file_exists(std::string file_name)
{
	log_messages.printf(MSG_NORMAL,
			"[XSM] Checking if file %s exists\n", file_name.c_str());

	/*std::ifstream finput(file_name.c_str());

	return finput;*/

    if (FILE *file = fopen(file_name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

bool EXTERNAL_SCHEDULER_MANAGER::delete_file(std::string file_name)
{
	log_messages.printf(MSG_NORMAL, "[XSM] Deleting file %s\n", file_name.c_str());

	int result;

#if defined(__linux__)
#include <unistd.h>

	result = unlink(file_name.c_str());

#else

	result = std::remove(file_name.c_str());

#endif

	if(result == 0){
		return true;
	}else{
		return false;
	}
}

int EXTERNAL_SCHEDULER_MANAGER::file_size(const char* filename)
{
	std::fstream file(filename, std::ios::in);
	file.seekg(0, std::ios::end);
	int sizeInBytes = file.tellg();

	file.close();

	return sizeInBytes;
}

bool EXTERNAL_SCHEDULER_MANAGER::save_request(FILE* fout, SCHEDULER_REQUEST* request)
{
	if (request == NULL) return false;

	fprintf(fout,
			"			<scheduler_request>\n"
			"				<authenticator>%s</authenticator>\n"
			"				<platform>\n"
			"					<name>%s</name>\n"
			"				</platform>\n"
			"				<alt_platforms>\n",
							request->authenticator,
							request->platform.name
	);

	for(uint ialt_platform = 0; ialt_platform < request->alt_platforms.size(); ialt_platform++)
	{

	fprintf(fout,
			"					<alt_platform>%s</alt_platform>\n",
								request->alt_platforms.at(ialt_platform).name
	);

	}	//end of alt platforms



	fprintf(fout,
			"				</alt_platforms>\n"
			"				<platforms>\n"
	);


	for(uint iplatform = 0; iplatform < request->platforms.list.size(); iplatform++)
	{

	fprintf(fout,
			"					<platform>\n"
			"						<id>%d</id>\n"
			"						<create_time>%d</create_time>\n"
			"						<name>%s</name>\n"
			"						<user_friendly_name>%s</user_friendly_name>\n"
			"						<deprecated>%d</deprecated>\n"
			"					</platform>\n",
									request->platforms.list.at(iplatform)->id,
									request->platforms.list.at(iplatform)->create_time,
									request->platforms.list.at(iplatform)->name,
									request->platforms.list.at(iplatform)->user_friendly_name,
									request->platforms.list.at(iplatform)->deprecated

	);

	}	//end of platforms

	fprintf(fout,
			"				</platforms>\n"
			"				<cross_project_id>%s</cross_project_id>\n"
			"				<hostid>%d</hostid>\n"
			"				<core_client_major_version>%d</core_client_major_version>\n"
			"				<core_client_minor_version>%d</core_client_minor_version>\n"
			"				<core_client_release>%d</core_client_release>\n"
			"				<core_client_version>%d</core_client_version>\n"
			"				<rpr_seqno>%d</rpr_seqno>\n"
			"				<work_req_seconds>%f</work_req_seconds>\n"
			"				<cpu_req_secs>%f</cpu_req_secs>\n"
			"				<cpu_req_instances>%f</cpu_req_instances>\n"
			"				<resource_share_fraction>%f</resource_share_fraction>\n"
			"				<rrs_fraction>%f</rrs_fraction>\n"
			"				<prrs_fraction>%f</prrs_fraction>\n"
			"				<cpu_estimated_delay>%f</cpu_estimated_delay>\n"
			"				<duration_correction_factor>%f</duration_correction_factor>\n"
			"				<global_prefs_xml>%s</global_prefs_xml>\n"
			"				<working_global_prefs_xml>\n%s\n</working_global_prefs_xml>\n"
			"				<code_sign_key>%s</code_sign_key>\n"
			"				<client_app_versions>\n",
							request->cross_project_id,
							request->hostid,
							request->core_client_major_version,
							request->core_client_minor_version,
							request->core_client_release,
							request->core_client_version,
							request->rpc_seqno,
							request->work_req_seconds,
							request->cpu_req_secs,
							request->cpu_req_instances,
							request->resource_share_fraction,
							request->rrs_fraction,
							request->prrs_fraction,
							request->cpu_estimated_delay,
							request->duration_correction_factor,
							request->global_prefs_xml,
							request->working_global_prefs_xml,
							request->code_sign_key
	);

	for(uint iclient_app_version = 0; iclient_app_version < request->client_app_versions.size(); iclient_app_version++)
	{


		CLIENT_APP_VERSION* cav = &request->client_app_versions.at(iclient_app_version);

	fprintf(fout,
			"					<client_app_version>\n"
			"						<app_name>%s</app_name>\n"
			"						<platform>%s</platform>\n"
			"						<version_num>%d</version_num>\n"
			"						<plan_class>%s</plan_class>\n"
			"						<host_usage>\n"
			"							<ncudas>%f</ncudas>\n"
			"							<natis>%f</natis>\n"
			"							<gpu_ram>%f</gpu_ram>\n"
			"							<avg_ncpus>%f</avg_ncpus>\n"
			"							<max_ncpus>%f</max_ncpus>\n"
			"							<projected_flops>%f</projected_flops>\n"
			"							<peak_flops>%f</peak_flops>\n"
			"							<cmdline>%s</cmdline>\n"
			"						</host_usage>\n"
			"						<rsc_fpops_scale>%f</rsc_fpops_scale>\n",
									cav->app_name,
									cav->platform,
									cav->version_num,
									cav->plan_class,
										cav->host_usage.ncudas,
										cav->host_usage.natis,
										cav->host_usage.gpu_ram,
										cav->host_usage.avg_ncpus,
										cav->host_usage.max_ncpus,
										cav->host_usage.projected_flops,
										cav->host_usage.peak_flops,
										cav->host_usage.cmdline,
									cav->rsc_fpops_scale
	);

	if(cav->app != NULL){

	fprintf(fout,

			"						<app>\n"
			"							<id>%d</id>\n"
			"							<create_time>%d</create_time>\n"
			"							<name>%s</name>\n"
			"							<min_version>%d</min_version>\n"
			"							<deprecated>%d</deprecated>\n"
			"							<user_friendly_name>%s</user_friendly_name>\n"
			"							<homogenenous_redundancy>%d</homogenenous_redundancy>\n"
			"							<weight>%f</weight>\n"
			"							<beta>%d</beta>\n"
			"							<target_nresults>%d</target_nresults>\n"
			"							<min_avg_pfc>%f</min_avg_pfc>\n"
			"							<host_scale_check>%d</host_scale_check>\n"
			"							<homogoneous_app_version>%d</homogoneous_app_version>\n"
			"						</app>\n",
										cav->app->id,
										cav->app->create_time,
										cav->app->name,
										cav->app->min_version,
										cav->app->deprecated ? 1 : 0,
										cav->app->user_friendly_name,
										cav->app->homogeneous_redundancy,
										cav->app->weight,
										cav->app->beta ? 1 : 0,
										cav->app->target_nresults,
										cav->app->min_avg_pfc,
										cav->app->host_scale_check ? 1 : 0,
										cav->app->homogeneous_app_version ? 1 : 0
	);

	}

	fprintf(fout,
	"					</client_app_version>\n"
	);

	}

	fprintf(fout,
			"				</client_app_versions>\n"
			"				<global_prefs>\n"
			"					<mod_time>%f</mod_time>\n"
			"					<disk_max_used_gb>%f</disk_max_used_gb>\n"
			"					<disk_max_used_pct>%f</disk_max_used_pct>\n"
			"					<disk_min_free_gd>%f</disk_min_free_gd>\n"
			"					<work_buf_min_days>%f</work_buf_min_days>\n"
			"					<ram_max_used_busy_frac>%f</ram_max_used_busy_frac>\n"
			"					<ram_max_used_idle_frac>%f</ram_max_used_idle_frac>\n"
			"					<max_ncpus_pct>%f</max_ncpus_pct>\n"
			"				</global_prefs>\n"
			"				<global_prefs_source_mail_hash>%s</global_prefs_source_mail_hash>\n",
								request->global_prefs.mod_time,
								request->global_prefs.disk_max_used_gb,
								request->global_prefs.disk_max_used_pct,
								request->global_prefs.disk_min_free_gb,
								request->global_prefs.work_buf_min_days,
								request->global_prefs.ram_max_used_busy_frac,
								request->global_prefs.ram_max_used_idle_frac,
								request->global_prefs.max_ncpus_pct,
							request->global_prefs_source_email_hash

	);


	fprintf(fout,
			"				<host_info>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<userid>%d</userid>\n"
			"					<rpc_seqno>%d</rpc_seqno>\n"
			"					<rpc_time>%d</rpc_time>\n"
			"					<total_credit>%f</total_credit>\n"
			"					<expavg_credit>%f</expavg_credit>\n"
			"					<expavg_time>%f</expavg_time>\n"
			"					<timezone>%d</timezone>\n"
			"					<domain_name>%s</domain_name>\n"
			"					<serialnum>%s</serialnum>\n"
			"					<last_ip_addr>%s</last_ip_addr>\n"
			"					<nsame_ip_addr>%d</nsame_ip_addr>\n"
			"					<on_frac>%f</on_frac>\n"
			"					<connected_frac>%f</connected_frac>\n"
			"					<active_frac>%f</active_frac>\n"
			"					<cpu_efficiency>%f</cpu_efficiency>\n"
			"					<duration_correction_factor>%f</duration_correction_factor>\n"
			"					<p_ncpus>%d</p_ncpus>\n"
			"					<p_vendor>%s</p_vendor>\n"
			"					<p_model>%s</p_model>\n"
			"					<p_fpops>%f</p_fpops>\n"
			"					<p_iops>%f</p_iops>\n"
			"					<p_membw>%f</p_membw>\n"
			"					<os_name>%s</os_name>\n"
			"					<os_version>%s</os_version>\n"
			"					<m_nbytes>%f</m_nbytes>\n"
			"					<m_cache>%f</m_cache>\n"
			"					<m_swap>%f</m_swap>\n"
			"					<d_total>%f</d_total>\n"
			"					<d_free>%f</d_free>\n"
			"					<d_boinc_used_total>%f</d_boinc_used_total>\n"
			"					<d_boinc_used_project>%f</d_boinc_used_project>\n"
			"					<d_boinc_max>%f</d_boinc_max>\n"
			"					<n_bwup>%f</n_bwup>\n"
			"					<n_bwdown>%f</n_bwdown>\n"
			"					<credit_per_cpu_sec>%f</credit_per_cpu_sec>\n"
			"					<venue>%s</venue>\n"
			"					<nresults_today>%d</nresults_today>\n"
			"					<avg_turnaround>%f</avg_turnaround>\n"
			"					<host_cpid>%s</host_cpid>\n"
			"					<external_ip_addr>%s</external_ip_addr>\n"
			"					<_max_results_day>%d</_max_results_day>\n"
			"					<_error_rate>%f</_error_rate>\n"
			"					<p_features>%s</p_features>\n"
			"					<virtualbox_version>%s</virtualbox_version>\n"
			"					<p_vm_extensions_disabled>%d</p_vm_extensions_disabled>\n"
			"				</host_info>\n",
								request->host.id,
								request->host.create_time,
								request->host.userid,
								request->host.rpc_seqno,
								request->host.rpc_time,
								request->host.total_credit,
								request->host.expavg_credit,
								request->host.expavg_time,
								request->host.timezone,
								request->host.domain_name,
								request->host.serialnum,
								request->host.last_ip_addr,
								request->host.nsame_ip_addr,
								request->host.on_frac,
								request->host.connected_frac,
								request->host.active_frac,
								request->host.cpu_efficiency,
								request->host.duration_correction_factor,
								request->host.p_ncpus,
								request->host.p_vendor,
								request->host.p_model,
								request->host.p_fpops,
								request->host.p_iops,
								request->host.p_membw,
								request->host.os_name,
								request->host.os_version,
								request->host.m_nbytes,
								request->host.m_cache,
								request->host.m_swap,
								request->host.d_total,
								request->host.d_free,
								request->host.d_boinc_used_total,
								request->host.d_boinc_used_project,
								request->host.d_boinc_max,
								request->host.n_bwup,
								request->host.n_bwdown,
								request->host.credit_per_cpu_sec,
								request->host.venue,
								request->host.nresults_today,
								request->host.avg_turnaround,
								request->host.host_cpid,
								request->host.external_ip_addr,
								request->host._max_results_day,
								request->host._error_rate,
								request->host.p_features,
								request->host.virtualbox_version,
								request->host.p_vm_extensions_disabled ? 1 : 0
	);

	if(request->coprocs.have_ati() || request->coprocs.have_nvidia())
	{
		fprintf(fout,
				"				<coprocs>\n"
		);

	}

	//
	//
	// NVIDIA COPROC
	//

		COPROC_NVIDIA* coproc_nvidia = &(request->coprocs.nvidia);

		if(coproc_nvidia == NULL) goto END_OF_COPROC_NVIDIA;

		if(request->coprocs.have_nvidia() == false) goto END_OF_COPROC_NVIDIA;

		//if(request->coprocs.have_nvidia())
		//{
		fprintf(fout,
				"						<coproc_cuda>\n"
		);
		//}

	fprintf(fout,
			"							<type>%s</type>\n"
			"							<count>%d</count>\n"
			"							<peak_flops>%f</peak_flops>\n"
			"							<used>%f</used>\n"
			"							<have_cuda>%d</have_cuda>\n"
			"							<have_cal>%d</have_cal>\n"
			"							<have_opencl>%d</have_opencl>\n"
			"							<available_ram>%f</available_ram>\n"
			"							<specified_in_config>%d</specified_in_config>\n"
			"							<req_secs>%f</req_secs>\n"
			"							<req_instances>%f</req_instances>\n"
			"							<estimated_delay>%f</estimated_delay>\n"
			"							<usages>\n",
										coproc_nvidia->type,
										coproc_nvidia->count,
										coproc_nvidia->peak_flops,
										coproc_nvidia->used,
										coproc_nvidia->have_cuda ? 1 : 0,
										coproc_nvidia->have_cal ? 1 : 0,
									    coproc_nvidia->have_opencl ? 1 : 0,
									 	coproc_nvidia->available_ram,
										coproc_nvidia->specified_in_config ? 1 : 0,
										coproc_nvidia->req_secs,
										coproc_nvidia->req_instances,
										coproc_nvidia->estimated_delay
	);

	for(int icoproc_usage = 0; icoproc_usage < coproc_nvidia->count; icoproc_usage++)
	{

	fprintf(fout,
			"								<usage>%f</usage>\n",
											coproc_nvidia->usage[icoproc_usage]
	);

	}	//end of coproc usage

	fprintf(fout,
			"							</usages>\n"
			"							<pending_usages>\n"
	);

	for(int icoproc_pending_usage = 0; icoproc_pending_usage < coproc_nvidia->count; icoproc_pending_usage++)
	{

	fprintf(fout,
			"								<pending_usage>%f</pending_usage>\n",
											coproc_nvidia->pending_usage[icoproc_pending_usage]
	);

	}	//end of coproc pending usage

	fprintf(fout,
			"							</pending_usages>\n"
			"							<device_nums>\n"
	);

	for(int icoproc_device_num = 0; icoproc_device_num < coproc_nvidia->count; icoproc_device_num++)
	{

	fprintf(fout,
			"								<device_num>%d</device_num>\n",
											coproc_nvidia->device_nums[icoproc_device_num]
	);

	}	//end of coproc device num



	fprintf(fout,
			"							</device_nums>\n"
			"							<opencl_device_ids>\n"
	);


	fprintf(fout,
			"							</opencl_device_ids>\n"
			"							<opencl_device_count>%d</opencl_device_count>\n"
			"							<running_graphics_apps>\n",
										coproc_nvidia->opencl_device_count
	);

	for(int icoproc_running_gfx_app = 0; icoproc_running_gfx_app < coproc_nvidia->count; icoproc_running_gfx_app++)
	{

	fprintf(fout,
			"								<running_graphics_app>%d</running_graphics_app>\n",
											coproc_nvidia->running_graphics_app[icoproc_running_gfx_app] ? 1 : 0

	);

	}		//end of coproc running graphics application

	fprintf(fout,
			"							</running_graphics_apps>\n"
			"							<available_ram_temps>\n"
	);

	fprintf(fout,
			"								<available_ram_temp>%f</available_ram_temp>\n",
											coproc_nvidia->available_ram
	);

	fprintf(fout,
			"							</available_ram_temps>\n"
			"							<last_print_time>%f</last_print_time>\n"
			"							<opencl_prop>\n"
			"								<device_id>%s</device_id>\n"
			"								<name>%s</name>\n"
			"								<vendor>%s</vendor>\n"
			"								<vendor_id>%d</vendor_id>\n"
			"								<available>%d</available>\n"
			"								<half_fp_config>%d</half_fp_config>\n"
			"								<single_fp_config>%d</single_fp_config>\n"
			"								<double_fp_config>%d</double_fp_config>\n"
			"								<endian_little>%d</endian_little>\n"
			"								<execution_capabilities>%d</execution_capabilities>\n"
			"								<extensions>%s</extensions>\n"
			"								<global_mem_size>%f</global_mem_size>\n"
			"								<local_mem_size>%f</local_mem_size>\n"
			"								<max_clock_frequency>%d</max_clock_frequency>\n"
			"								<max_compute_units>%d</max_compute_units>\n",
										coproc_nvidia->last_print_time,
											coproc_nvidia->opencl_prop.device_id,
											coproc_nvidia->opencl_prop.name,
											coproc_nvidia->opencl_prop.vendor,
											coproc_nvidia->opencl_prop.vendor_id,
											coproc_nvidia->opencl_prop.available,
											coproc_nvidia->opencl_prop.half_fp_config,
											coproc_nvidia->opencl_prop.single_fp_config,
											coproc_nvidia->opencl_prop.double_fp_config,
											coproc_nvidia->opencl_prop.endian_little,
											coproc_nvidia->opencl_prop.execution_capabilities,
											coproc_nvidia->opencl_prop.extensions,
											coproc_nvidia->opencl_prop.global_mem_size,
											coproc_nvidia->opencl_prop.local_mem_size,
											coproc_nvidia->opencl_prop.max_clock_frequency,
											coproc_nvidia->opencl_prop.max_compute_units
	);

	fprintf(fout,
		"									<opencl_platform_version>%s</opencl_platform_version>\n",
											coproc_nvidia->opencl_prop.opencl_platform_version
	);


	fprintf(fout,
		"									<opencl_device_version>%s</opencl_device_version>\n",
											coproc_nvidia->opencl_prop.opencl_device_version
	);

	fprintf(fout,
			"								<opencl_device_version_int>%d</opencl_device_version_int>\n",
											coproc_nvidia->opencl_prop.opencl_device_version_int
	);

	fprintf(fout,
		"									<opencl_driver_version>%s</opencl_driver_version>\n",
											coproc_nvidia->opencl_prop.opencl_device_version
	);


	fprintf(fout,
			"								<device_num>%d</device_num>\n"
			"								<peak_flops>%f</peak_flops>\n"
			"								<is_used>%d</is_used>\n"
			"							</opencl_prop>\n",
										coproc_nvidia->opencl_prop.device_num,
										coproc_nvidia->opencl_prop.peak_flops,
										coproc_nvidia->opencl_prop.is_used


	);

	fprintf(fout,
			"						<cuda_version>%d</cuda_version>\n"
			"						<display_driver_version>%d</display_driver_version>\n"
			"						<prop>\n"
			"							<name>%s</name>\n"
			"							<deviceHandle>%d</deviceHandle>\n"
			"							<totalGlobalMem>%d</totalGlobalMem>\n"
			"							<sharedMemPerBlock>%d</sharedMemPerBlock>\n"
			"							<regsPerBlock>%d</regsPerBlock>\n"
			"							<warpSize>%d</warpSize>\n"
			"							<memPitch>%d</memPitch>\n"
			"							<maxThreadsPerBlock>%d</maxThreadsPerBlock>\n"
			"							<maxThreadsDim>%d %d %d</maxThreadsDim>\n"
			"							<maxGridSize>%d %d %d</maxGridSize>\n"
			"							<clockRate>%d</clockRate>\n"
			"							<totalConstMem>%d</totalConstMem>\n"
			"							<major>%d</major>\n"
			"							<minor>%d</minor>\n"
			"							<textureAlignment>%d</textureAlignment>\n"
			"							<deviceOverlap>%d</deviceOverlap>\n"
			"							<multiProcessorCount>%d</multiProcessorCount>\n"
			"							<dtotalGlobalMem>%f</dtotalGlobalMem>\n"
			"						</prop>\n",
									coproc_nvidia->cuda_version,
									coproc_nvidia->display_driver_version,
										coproc_nvidia->prop.name,
										coproc_nvidia->prop.deviceHandle,
										coproc_nvidia->prop.totalGlobalMem,
										coproc_nvidia->prop.sharedMemPerBlock,
										coproc_nvidia->prop.regsPerBlock,
										coproc_nvidia->prop.warpSize,
										coproc_nvidia->prop.memPitch,
										coproc_nvidia->prop.maxThreadsPerBlock,
										coproc_nvidia->prop.maxThreadsDim[0],
										coproc_nvidia->prop.maxThreadsDim[1],
										coproc_nvidia->prop.maxThreadsDim[2],
										coproc_nvidia->prop.maxGridSize[0],
										coproc_nvidia->prop.maxGridSize[1],
										coproc_nvidia->prop.maxGridSize[2],
										coproc_nvidia->prop.clockRate,
										coproc_nvidia->prop.totalConstMem,
										coproc_nvidia->prop.major,
										coproc_nvidia->prop.minor,
										coproc_nvidia->prop.textureAlignment,
										coproc_nvidia->prop.deviceOverlap,
										coproc_nvidia->prop.multiProcessorCount,
										coproc_nvidia->prop.dtotalGlobalMem

	);

	//if(request->coprocs.have_nvidia())
	//{
	fprintf(fout,
			"						</coproc_cuda>\n"
	);
	//}

	END_OF_COPROC_NVIDIA:

	//
	//
	// END OF NVIDIA COPROC

	COPROC_ATI* coproc_ati = &(request->coprocs.ati);

	if(coproc_ati == NULL) goto END_OF_COPROC_ATI;

	if(request->coprocs.have_ati() == false) goto END_OF_COPROC_ATI;

	//
	 //*
	 //* ATI/AMD COPROC

	//if(request->coprocs.have_ati())
	//{
	fprintf(fout,
			"						<coproc_ati>\n"
	);
	//}

	fprintf(fout,
			"							<type>%s</type>\n"
			"							<count>%d</count>\n"
			"							<peak_flops>%f</peak_flops>\n"
			"							<used>%f</used>\n"
			"							<have_cuda>%d</have_cuda>\n"
			"							<have_cal>%d</have_cal>\n"
			"							<have_opencl>%d</have_opencl>\n"
			"							<available_ram>%f</available_ram>\n"
			"							<specified_in_config>%d</specified_in_config>\n"
			"							<req_secs>%f</req_secs>\n"
			"							<req_instances>%f</req_instances>\n"
			"							<estimated_delay>%f</estimated_delay>\n"
			"							<usages>\n",
										coproc_ati->type,
										coproc_ati->count,
										coproc_ati->peak_flops,
										coproc_ati->used,
										coproc_ati->have_cuda ? 1 : 0,
										coproc_ati->have_cal ? 1 : 0,
										coproc_ati->have_opencl ? 1 : 0,
										coproc_ati->available_ram,
										coproc_ati->specified_in_config ? 1 : 0,
										coproc_ati->req_secs,
										coproc_ati->req_instances,
										coproc_ati->estimated_delay
	);

	for(int icoproc_usage = 0; icoproc_usage < coproc_ati->count; icoproc_usage++)
	{

	fprintf(fout,
			"								<usage>%f</usage>\n",
											coproc_ati->usage[icoproc_usage]
	);

	}	//end of coproc usage

	fprintf(fout,
			"							</usages>\n"
			"							<pending_usages>\n"
	);

	for(int icoproc_pending_usage = 0; icoproc_pending_usage < coproc_ati->count; icoproc_pending_usage++)
	{

	fprintf(fout,
			"								<pending_usage>%f</pending_usage>\n",
											coproc_ati->pending_usage[icoproc_pending_usage]
	);

	}	//end of coproc pending usage

	fprintf(fout,
			"							</pending_usages>\n"
			"							<device_nums>\n"
	);

	for(int icoproc_device_num = 0; icoproc_device_num < coproc_ati->count; icoproc_device_num++)
	{

	fprintf(fout,
			"								<device_num>%d</device_num>\n",
											coproc_ati->device_nums[icoproc_device_num]
	);

	}	//end of coproc device num



	fprintf(fout,
			"							</device_nums>\n"
			"							<opencl_device_ids>\n"
	);


	fprintf(fout,
			"							</opencl_device_ids>\n"
			"							<opencl_device_count>%d</opencl_device_count>\n"
			"							<running_graphics_apps>\n",
										coproc_ati->opencl_device_count
	);

	for(int icoproc_running_gfx_app = 0; icoproc_running_gfx_app < coproc_ati->count; icoproc_running_gfx_app++)
	{

	fprintf(fout,
			"								<running_graphics_app>%d</running_graphics_app>\n",
											coproc_ati->running_graphics_app[icoproc_running_gfx_app] ? 1 : 0

	);

	}		//end of coproc running graphics application

	fprintf(fout,
			"							</running_graphics_apps>\n"
			"							<available_ram_temps>\n"
	);

	fprintf(fout,
			"								<available_ram_temp>%f</available_ram_temp>\n",
											coproc_ati->available_ram
	);

	fprintf(fout,
			"							</available_ram_temps>\n"
			"							<last_print_time>%f</last_print_time>\n"
			"							<opencl_prop>\n"
			"								<device_id>%s</device_id>\n"
			"								<name>%s</name>\n"
			"								<vendor>%s</vendor>\n"
			"								<vendor_id>%d</vendor_id>\n"
			"								<available>%d</available>\n"
			"								<half_fp_config>%d</half_fp_config>\n"
			"								<single_fp_config>%d</single_fp_config>\n"
			"								<double_fp_config>%d</double_fp_config>\n"
			"								<endian_little>%d</endian_little>\n"
			"								<execution_capabilities>%d</execution_capabilities>\n"
			"								<extensions>%s</extensions>\n"
			"								<global_mem_size>%d</global_mem_size>\n"
			"								<local_mem_size>%d</local_mem_size>\n"
			"								<max_clock_frequency>%d</max_clock_frequency>\n"
			"								<max_compute_units>%d</max_compute_units>\n",
										coproc_ati->last_print_time,
											coproc_ati->opencl_prop.device_id,
											coproc_ati->opencl_prop.name,
											coproc_ati->opencl_prop.vendor,
											coproc_ati->opencl_prop.vendor_id,
											coproc_ati->opencl_prop.available,
											coproc_ati->opencl_prop.half_fp_config,
											coproc_ati->opencl_prop.single_fp_config,
											coproc_ati->opencl_prop.double_fp_config,
											coproc_ati->opencl_prop.endian_little,
											coproc_ati->opencl_prop.execution_capabilities,
											coproc_ati->opencl_prop.extensions,
											coproc_ati->opencl_prop.global_mem_size,
											coproc_ati->opencl_prop.local_mem_size,
											coproc_ati->opencl_prop.max_clock_frequency,
											coproc_ati->opencl_prop.max_compute_units
	);

	fprintf(fout,
		"									<opencl_platform_version>%s</opencl_platform_version>\n",
											coproc_ati->opencl_prop.opencl_platform_version
	);

	fprintf(fout,
		"									<opencl_device_version>%s</opencl_device_version>\n",
											coproc_ati->opencl_prop.opencl_device_version
	);

	fprintf(fout,
			"								<opencl_device_version_int>%d</opencl_device_version_int>\n",
											coproc_ati->opencl_prop.opencl_device_version_int
	);

	fprintf(fout,
		"									<opencl_driver_version>%s</opencl_driver_version>\n",
											coproc_ati->opencl_prop.opencl_device_version
	);

	fprintf(fout,
			"								<device_num>%d</device_num>\n"
			"								<peak_flops>%f</peak_flops>\n"
			"								<is_used>%d</is_used>\n"
			"							</opencl_prop>\n",
			//"						</coproc>\n",
											coproc_ati->opencl_prop.device_num,
											coproc_ati->opencl_prop.peak_flops,
											coproc_ati->opencl_prop.is_used


	);

	fprintf(fout,
			"						<name>%s</name>\n"
			"						<version>%s</version>\n"
			"						<version_num>%d</version_num>\n"
			"						<atirt_detected>%d</atirt_detected>\n"
			"						<amdrt_detected>%d</amdrt_detected>\n"
			"						<attribs>\n"
			"							<struct_size>%d</struct_size>\n"
			"							<target>%d</target>\n"
			"							<localRAM>%d</localRAM>\n"
			"							<uncachedRemoteRAM>%d</uncachedRemoteRAM>\n"
			"							<cachedRemoteRAM>%d</cachedRemoteRAM>\n"
			"							<engineClock>%d</engineClock>\n"
			"							<memoryClock>%d</memoryClock>\n"
			"							<wavefrontSize>%d</wavefrontSize>\n"
			"							<numberOfSIMD>%d</numberOfSIMD>\n"
			"							<doublePrecision>%d</doublePrecision>\n"
			"							<localDataShare>%d</localDataShare>\n"
			"							<globalDataShare>%d</globalDataShare>\n"
			"							<globalGPR>%d</globalGPR>\n"
			"							<computeShader>%d</computeShader>\n"
			"							<memExport>%d</memExport>\n"
			"							<pitch_alignment>%d</pitch_alignment>\n"
			"							<surface_alignment>%d</surface_alignment>\n"
			"						</attribs>\n"
			"						<info>\n"
			"							<target>%d</target>\n"
			"							<maxResource1DWidth>%d</maxResource1DWidth>\n"
			"							<maxResource2DWidth>%d</maxResource2DWidth>\n"
			"							<maxResource2DHeight>%d</maxResource2DHeight>\n"
			"						</info>\n",
									coproc_ati->name,
									coproc_ati->version,
									coproc_ati->version_num,
									coproc_ati->atirt_detected ? 1 : 0,
									coproc_ati->amdrt_detected ? 1 : 0,
										coproc_ati->attribs.struct_size,
										coproc_ati->attribs.target,
										coproc_ati->attribs.localRAM,
										coproc_ati->attribs.uncachedRemoteRAM,
										coproc_ati->attribs.cachedRemoteRAM,
										coproc_ati->attribs.engineClock,
										coproc_ati->attribs.memoryClock,
										coproc_ati->attribs.wavefrontSize,
										coproc_ati->attribs.numberOfSIMD,
										coproc_ati->attribs.doublePrecision,
										coproc_ati->attribs.localDataShare,
										coproc_ati->attribs.globalDataShare,
										coproc_ati->attribs.globalGPR,
										coproc_ati->attribs.computeShader,
										coproc_ati->attribs.memExport,
										coproc_ati->attribs.pitch_alignment,
										coproc_ati->attribs.surface_alignment,
									coproc_ati->info.target,
									coproc_ati->info.maxResource1DWidth,
									coproc_ati->info.maxResource2DWidth,
									coproc_ati->info.maxResource2DHeight

	);

	if(request->coprocs.have_ati())
	{
	fprintf(fout,
			"						</coproc_ati>\n"
	);
	}

	END_OF_COPROC_ATI:

	if(request->coprocs.have_ati() || request->coprocs.have_nvidia())
	{
	fprintf(fout,
			"				</coprocs>\n"
	);

	}


	//
	//
	// END OF ATI/AMD COPROC
	//

	if(request->results.size())
	{
	fprintf(fout,
			"				<results>\n"
	);
	}

	for(int iresult = 0; iresult < request->results.size(); iresult++)
	{

		SCHED_DB_RESULT* db_result = &(request->results.at(iresult));

		if(db_result == NULL) continue;		//not likely to occur.

	fprintf(fout,
			"					<result>\n"
			"						<wu_name>%s</wu_name>\n"
			"						<units>%d</units>\n"
			"						<platform_name>%s</platform_name>\n",
									db_result->wu_name,
									db_result->units,
									db_result->platform_name
	);

	fprintf(fout,
			"						<best_app_version>\n"
			"							<appid>%d</appid>\n"
			"							<for_64b_jobs>%d</for_64b_jobs>\n"
			"							<present>%d</present>\n",
										db_result->bav.appid,
										db_result->bav.for_64b_jobs ? 1 : 0,
										db_result->bav.present ? 1 : 0
	);

	if (db_result->bav.cavp != NULL){

	fprintf(fout,
			"							<client_app_version>\n"
			"								<app_name>%s</app_name>\n"
			"								<platform>%s</platform>\n"
			"								<version_num>%d</version_num>\n"
			"								<plan_class>%s</plan_class>\n"
			"								<host_usage>\n"
			"									<ncudas>%f</ncudas>\n"
			"									<natis>%f</natis>\n"
			"									<gpu_ram>%f</gpu_ram>\n"
			"									<avg_ncpus>%f</avg_ncpus>\n"
			"									<max_ncpus>%f</max_ncpus>\n"
			"									<projected_flops>%f</projected_flops>\n"
			"									<peak_flops>%f</peak_flops>\n"
			"									<cmdline>%s</cmdline>\n"
			"								</host_usage>\n"
			"							</client_app_version>\n",
											db_result->bav.cavp->app_name,
											db_result->bav.cavp->platform,
											db_result->bav.cavp->version_num,
											db_result->bav.cavp->plan_class,
											db_result->bav.cavp->host_usage.ncudas,
											db_result->bav.cavp->host_usage.natis,
											db_result->bav.cavp->host_usage.gpu_ram,
											db_result->bav.cavp->host_usage.avg_ncpus,
											db_result->bav.cavp->host_usage.max_ncpus,
											db_result->bav.cavp->host_usage.projected_flops,
											db_result->bav.cavp->host_usage.peak_flops,
											db_result->bav.cavp->host_usage.cmdline
	);
	}

	if (db_result->bav.avp != NULL){

	fprintf(fout,

			"							<app_version>\n"
			"								<id>%d</id>\n"
			"								<create_time>%d</create_time>\n"
			"								<appid>%d</appid>\n"
			"								<version_num>%d</version_num>\n"
			"								<platformid>%d</platformid>\n"
			"								<xml_doc>%s</xml_doc>\n"
			"								<min_core_version>%d</min_core_version>\n"
			"								<max_core_version>%d</max_core_version>\n"
			"								<deprecated>%d</deprecated>\n"
			"								<plan_class>%s</plan_class>\n"
			"								<pfc>\n"
			"									<n>%f</n>\n"
			"									<avg>%f</avg>\n"
			"								</pfc>\n"
			"								<pfc_scale>%f</pfc_scale>\n"
			"								<expavg_credit>%f</expavg_credit>\n"
			"								<expavg_credit>%f</expavg_time>\n"
			"							</app_version>\n",
											db_result->bav.avp->id,
											db_result->bav.avp->create_time,
											db_result->bav.avp->version_num,
											db_result->bav.avp->platformid,
											db_result->bav.avp->xml_doc,
											db_result->bav.avp->min_core_version,
											db_result->bav.avp->max_core_version,
											db_result->bav.avp->deprecated ? 1 : 0,
											db_result->bav.avp->plan_class,
											db_result->bav.avp->pfc.n,
											db_result->bav.avp->pfc.avg,
											db_result->bav.avp->pfc_scale,
											db_result->bav.avp->expavg_credit,
											db_result->bav.avp->expavg_credit
	);
	}

	fprintf(fout,

			"							<host_usage>\n"
			"								<ncudas>%f</ncudas>\n"
			"								<natis>%f</natis>\n"
			"								<gpu_ram>%f</gpu_ram>\n"
			"								<avg_ncpus>%f</avg_ncpus>\n"
			"								<max_ncpus>%f</max_ncpus>\n"
			"								<projected_flops>%f</projected_flops>\n"
			"								<peak_flops>%f</peak_flops>\n"
			"								<cmdline>%f</cmdline>\n"
			"							</host_usage>\n"
			"							<reliable>%d</reliable>\n"
			"							<trusted>%d</trusted>\n"
			"						</best_app_version>\n"
			"					</result>\n",
											db_result->bav.host_usage.ncudas,
											db_result->bav.host_usage.natis,
											db_result->bav.host_usage.gpu_ram,
											db_result->bav.host_usage.avg_ncpus,
											db_result->bav.host_usage.max_ncpus,
											db_result->bav.host_usage.projected_flops,
											db_result->bav.host_usage.peak_flops,
											db_result->bav.host_usage.cmdline,
										db_result->bav.reliable ? 1 : 0,
										db_result->bav.trusted ? 1 : 0
	);/**/

	}	// end of db result

	if(request->results.size())
	{
	fprintf(fout,
			"				</results>\n"
	);
	}

	if(request->file_xfer_results.size())
	{
	fprintf(fout,
			"				<file_xfer_results>\n"
	);
	}

	for(int icompleted_results = 0; icompleted_results < request->file_xfer_results.size(); icompleted_results++)
	{
		RESULT* res = &(request->file_xfer_results.at(icompleted_results));

		if(res == NULL ) continue;

	fprintf(fout,
			"					<file_xfer_result>\n"
			"						<id>%d</id>\n"
			"						<create_time>%d</create_time>\n"
			"						<workunitid>%d</workunitid>\n"
			"						<server_state>%d</server_state>\n"
			"						<outcome>%d</outcome>\n"
			"						<client_state>%d</client_state>\n"
			"						<hostid>%d</hostid>\n"
			"						<userid>%d</userid>\n"
			"						<report_deadline>%d</report_deadline>\n"
			"						<sent_time>%d</sent_time>\n"
			"						<received_time>%d</received_time>\n"
			"						<name>%s</name>\n"
			"						<cpu_time>%f</cpu_time>\n"
			"						<xml_doc_in>%s</xml_doc_in>\n"
			"						<xml_doc_out>%s</xml_doc_out>\n"
			"						<stderr_out>%s</stderr_out>\n"
			"						<batch>%d</batch>\n"
			"						<file_delete_state>%d</file_delete_state>\n"
			"						<validate_state>%d</validate_state>\n"
			"						<claimed_credit>%f</claimed_credit>\n"
			"						<granted_credit>%f</granted_credit>\n"
			"						<opaque>%f</opaque>\n"
			"						<random>%d</random>\n"
			"						<app_version_num>%d</app_version_num>\n"
			"						<appid>%d</appid>\n"
			"						<exit_status>%d</exit_status>\n"
			"						<teamid>%d</teamid>\n"
			"						<priority>%d</priority>\n"
			"						<mod_time>%s</mod_time>\n"
			"						<elapsed_time>%f</elapsed_time>\n"
			"						<flops_estimate>%f</flops_estimate>\n"
			"						<app_version_id>%d</app_version_id>\n"
			"						<runtime_outlier>%d</runtime_outlier>\n"
			"					</file_xfer_result>\n",
									res->id,
									res->create_time,
									res->workunitid,
									res->server_state,
									res->outcome,
									res->client_state,
									res->hostid,
									res->userid,
									res->report_deadline,
									res->sent_time,
									res->received_time,
									res->name,
									res->cpu_time,
									res->xml_doc_in,
									res->xml_doc_out,
									res->stderr_out,
									res->batch,
									res->file_delete_state,
									res->validate_state,
									res->claimed_credit,
									res->granted_credit,
									res->opaque,
									res->random,
									res->app_version_num,
									res->appid,
									res->exit_status,
									res->teamid,
									res->priority,
									res->mod_time,
									res->elapsed_time,
									res->flops_estimate,
									res->app_version_id,
									res->runtime_outlier ? 1 : 0
	);

	}	//end of result

	if(request->file_xfer_results.size())
	{
	fprintf(fout,
			"				</file_xfer_results>\n"
	);
	}

	if(request->msgs_from_host.size())
	{
	fprintf(fout,
			"				<msgs_from_hosts>\n"
	);
	}

	for(int imsg_from_host = 0; imsg_from_host < request->msgs_from_host.size(); imsg_from_host++)
	{

	fprintf(fout,
			"					<msg_from_host>\n"
			"						<variety>%s</variety>\n"
			"						<msg_text>%s</msg_text>\n"
			"					</msg_from_host>\n",
								request->msgs_from_host.at(imsg_from_host).variety,
								request->msgs_from_host.at(imsg_from_host).msg_text.c_str()


	);

	}		//end of message to and from host


	if(request->msgs_from_host.size())
	{
	fprintf(fout,
			"				</msgs_from_hosts>\n"
	);
	}

	if(request->file_infos.size())
	{
	fprintf(fout,
			"				<file_infos>\n"
	);
	}

	for(int ifile_info = 0; ifile_info < request->file_infos.size(); ifile_info++)
	{

	fprintf(fout,
			"					<file_info>\n"
			"						<name>%s</name>\n"
			"					</file_info>\n",
									request->file_infos.at(ifile_info).name
	);

	}	//end of file info

	if(request->file_infos.size())
	{
	fprintf(fout,
			"				</file_infos>\n"
	);
	}

	if(request->file_delete_candidates.size())
	{
    fprintf(fout,
			"				<file_delete_candidates>\n"
	);
	}

	for(int idelete_cadidate = 0; idelete_cadidate < request->file_delete_candidates.size(); idelete_cadidate++)
	{

	fprintf(fout,
			"					<file_delete_candidate>\n"
			"						<name>%s</name>\n"
			"					</file_delete_candidate>\n",
									request->file_delete_candidates.at(idelete_cadidate).name
	);

	}	//end of file delete candidate

	if(request->file_delete_candidates.size())
	{
	fprintf(fout,
			"				</file_delete_candidates>\n"
	);
	}

	if(request->files_not_needed.size())
	{
    fprintf(fout,
			"				<files_not_needed>\n"
	);
	}

	for(int ifile_not_needed = 0; ifile_not_needed < request->files_not_needed.size(); ifile_not_needed++)
	{

	fprintf(fout,
			"					<file_not_needed>\n"
			"						<name>%s</name>\n"
			"					</file_not_needed>\n",
									request->files_not_needed.at(ifile_not_needed).name
	);

	}	//end of file note needed

	if(request->files_not_needed.size())
	{
	fprintf(fout,
			"				</files_not_needed>\n"
	);
	}

	if(request->other_results.size())
	{
	fprintf(fout,
			"				<other_results>\n"
	);
	}

	for(int iother_result = 0; iother_result < request->other_results.size(); iother_result++)
	{

	fprintf(fout,
			"					<other_result>\n"
			"						<name>%s</name>\n"
			"						<app_version>%d</app_version>\n"
			"						<plan_class>%s</plan_class>\n"
			"						<have_plan_class>%d</have_plan_class>\n"
			"						<abort>%d</abort>\n"
			"						<abort_if_not_started>%d</abort_if_not_started>\n"
			"						<reason>%d</reason>\n"
			"					</other_result>\n",
									request->other_results.at(iother_result).name,
									request->other_results.at(iother_result).app_version,
									request->other_results.at(iother_result).plan_class,
									request->other_results.at(iother_result).have_plan_class ? 1 : 0,
									request->other_results.at(iother_result).abort ? 1 : 0,
									request->other_results.at(iother_result).abort_if_not_started ? 1 : 0,
									request->other_results.at(iother_result).reason
	);

	}	//end of other results


	if(request->other_results.size())
	{
	fprintf(fout,
			"				</other_results>\n"

    );
	}


	if(request->ip_results.size())
	{
	fprintf(fout,
			"				<in_progress_results>\n"
	);
	}

	for(int iip_result = 0; iip_result < request->ip_results.size(); iip_result++)
	{

	fprintf(fout,
			"					<ip_result>\n"
			"						<name>%s</name>\n"
			"						<computation_deadline>%f</computation_deadline>\n"
			"						<report_deadline>%f</report_deadline>\n"
			"						<cpu_time_remaining>%f</cpu_time_remaining>\n"
			"						<misses_deadline>%d</misses_deadline>\n"
			"						<estimated_completion_time>%f</estimated_completion_time>\n"
			"					</ip_result>\n",
									request->ip_results.at(iip_result).name,
									request->ip_results.at(iip_result).computation_deadline,
									request->ip_results.at(iip_result).report_deadline,
									request->ip_results.at(iip_result).cpu_time_remaining,
									request->ip_results.at(iip_result).misses_deadline ? 1 : 0,
									request->ip_results.at(iip_result).estimated_completion_time
	);

	}

	if(request->ip_results.size())
	{
	fprintf(fout,
			"				</in_progress_results>\n"
	);
	}

	fprintf(fout,
			"				<have_other_results_list>%d</have_other_results_list>\n"
			"				<have_ip_results_list>%d</have_ip_results_list>\n"
			"				<have_time_stats_log>%d</have_time_stats_log>\n"
			"				<client_cap_plan_class>%d</client_cap_plan_class>\n"
			"				<sandbox>%d</sandbox>\n"
			"				<allow_multiple_clients>%d</allow_multiple_clients>\n"
			"				<using_weak_auth>%d</using_weak_auth>\n"
			"				<last_rpc_dayofyear>%d</last_rpc_dayofyear>\n"
			"				<current_rpc_dayofyear>%d</current_rpc_dayofyear>\n"
			"				<client_opaque>%s</client_opaque>\n",
							request->have_other_results_list ? 1 : 0,
							request->have_ip_results_list ? 1 : 0,
							request->have_time_stats_log ? 1 : 0,
							request->client_cap_plan_class ? 1 : 0,
							request->sandbox,
							request->allow_multiple_clients,
							request->using_weak_auth ? 1 : 0,
							request->last_rpc_dayofyear,
							request->current_rpc_dayofyear,
							request->client_opaque.c_str()



	);


	if(request->host_availability_records.size())
	{
		fprintf(fout,
				"				<host_availability>\n"
		);

	}

	for(int irecord = 0; irecord < request->host_availability_records.size(); irecord++)
	{

		AVAILABILITY_RECORD* ar = &(request->host_availability_records.at(irecord));

		if(ar == NULL) continue;	//not likely

	fprintf(fout,
			"					<record>\n"
			"						<time>\n"
			"							<hour>%d</hour>\n"
			"							<minute>%d</minute>\n"
			"						</time>\n"
			"						<date>\n"
			"							<year>%d</year>\n"
			"							<month>%d</month>\n"
			"							<day>%d</day>\n"
			"						</date>\n"
			"						<availability>%f</availability>\n"
			"					</record>\n",
										ar->hour,
										ar->minutes,
										ar->year,
										ar->month,
										ar->day,
										ar->availability

	);

	}	//end of availability records

	if(request->host_availability_records.size())
	{
	fprintf(fout,
			"				</host_availability>\n"
	);
	}

	fprintf(fout,
			"			</scheduler_request>\n"
	);

		//end of request

    return true;

}

bool EXTERNAL_SCHEDULER_MANAGER::parse_request(XML_PARSER* xp, SCHEDULER_REQUEST* request)
{
    const char* p = request->parse(*xp);

    if (!p){

    	log_messages.printf(MSG_NORMAL, "[XSM] Request parsed successfully\n");

    	return true;
    }

    log_messages.printf(MSG_NORMAL, "[XSM] Failure while parsing request: %s\n", p);

	return false;
}

bool EXTERNAL_SCHEDULER_MANAGER::create_file(std::string file_name)
{
	log_messages.printf(MSG_NORMAL, "[XSM] Creating file %s\n", file_name.c_str());

	std::ofstream foutput;
	foutput.open(file_name.c_str(), std::ios::binary);

	bool result = foutput.is_open();

	foutput.close();

	return result;
}

bool EXTERNAL_SCHEDULER_MANAGER::is_host_on_ready_queue(){

	bool found = false;

	log_messages.printf(MSG_NORMAL, "[XSM] Checking [HOST%d] on ready queue\n",
			g_reply->host.id);

	for (int i = 0; i < ssp->max_wu_results; i++){
		if (ssp->wu_results[i].state == g_reply->host.id){
			found = true;
			break;
		}
	}

	return found;
}

bool EXTERNAL_SCHEDULER_MANAGER::is_host_on_waiting_queue()
{
	bool found = false;

	//if (this->read_waiting_queue() == NULL){
	//	log_messages.printf(MSG_NORMAL, "Read WQ successfully.\n");
	//}else{
	//	return false;
	//}

	int limit = this->waiting_queue.size();

	//std::string list = "";

	//for (int i = 0; i < this->waiting_queue.size(); i++){
	//	list += this->waiting_queue[i].host.id;
	//}

	//log_messages.printf(MSG_NORMAL, "Hosts on waiting queue: %s\n",
	//		list.c_str());

	//because the HOST structure does not parse host id from a request,
	//we can only use the variable hostid to check the host
	//this should be ok, becuase:
	//1. Only authenticated users and hosts are on the waiting queue
	//2. A host is only removed from the waiting queue to be assigned work, and its ID is used

	for (int i = 0; i < limit; i++){
		if (this->waiting_queue[i].hostid == g_reply->host.id){
			found = true;
			break;
		}
	}



	//for (int i = 0; i < ssp->max_xs_waiting_queue_units; i++){
	//	if (ssp->xs_waiting_queue[i].hostid != 0){
	//		if (ssp->xs_waiting_queue[i].hostid == g_reply->host.id){
	//			found = true;
	//			break;
	//		}
	//	}
	//}

	return found;
}

bool EXTERNAL_SCHEDULER_MANAGER::add_host_to_waiting_queue()
{
	SCHEDULER_REQUEST* rq = g_request;

	//request->host structure is incomplete: not all fields are parsed from the it.
	//however:
	//- request->hostid is valid at this point and it should be used to check if the host is on the waiting queue
	//we assign it the values from g_reply for the XS to have full data for analysis

	rq->host = g_reply->host;

	log_messages.printf(MSG_NORMAL, "[XSM] Adding [HOST%d] to WAITING QUEUE.\n",
			rq->hostid);


	//bool added = false;

	//lock_sema();

	//for (int i = 0; i < ssp->max_xs_waiting_queue_units; i++){
	//	if (ssp->xs_waiting_queue[i].hostid == 0){
	//		SCHEDULER_REQUEST::clone(rq, ssp->xs_waiting_queue[i]);
	//		added = true;
	//		break;
	//	}
	//}

	//unlock_sema();

	this->waiting_queue.push_back(*rq);

	return true;
}


//static void processRequest();
void EXTERNAL_SCHEDULER_MANAGER::notify_back_off(std::string message, int back_off_period)
{
	g_reply->insert_message(message.c_str(), "low");
	g_reply->set_delay(back_off_period);

	if (!config.msg_to_host) {
		log_messages.printf(MSG_NORMAL, "[XSM] ", message.c_str());
    }
}

bool EXTERNAL_SCHEDULER_MANAGER::is_waiting_queue_in_use()
{
	//std::string filedir(this->xs_directory() + "/" + WQ_LOCK_FILE);

	//return this->file_exists(filedir);

	return ssp->waiting_queue_busy;
}

bool EXTERNAL_SCHEDULER_MANAGER::lock_waiting_queue()
{
	//std::string filedir(this->xs_directory() + "/" + WQ_LOCK_FILE);

	//int iter = 0;
	//while(this->file_exists(filedir) && (this->is_xs_running() == false))
	//{
		//sleep(1);
		//boinc_sleep(1);
	//	log_messages.printf(MSG_NORMAL,
	//				"LOOP TO LOCK WQ [ITER%d]\n",
	//				++iter);

	//	continue;
	//}

	//log_messages.printf(MSG_NORMAL,
	//					"BROKE FREE FROM LOOP TO LOCK WQ]\n");

	//if(this->file_exists(filedir) == false)
	//{
	//	return this->create_file(filedir);
	//}

	//it is possible that the XS started while we were waiting

	while(ssp->waiting_queue_busy == true && ssp->xs_busy == false)
	{
		//wait
		continue;
	}

	if (ssp->waiting_queue_busy == false){
		lock_sema();
		ssp->waiting_queue_busy = true;
		unlock_sema();
		log_messages.printf(MSG_NORMAL,
					"[XSM] WAITING QUEUE was successfully locked\n");
		return true;
	}

	log_messages.printf(MSG_NORMAL,
							"[XSM] WAITING QUEUE failed to be locked by this process\n");

	return false;
}

bool EXTERNAL_SCHEDULER_MANAGER::lock_xs()
{
	log_messages.printf(MSG_NORMAL, "[XSM] Locking XS\n");

	//std::string xs_file(this->xs_directory() + "/" + XS_LOCK_FILE);

	//if(this->file_exists(xs_file) == false)
	//{
	//	return this->create_file(xs_file);
	//}

	lock_sema();
	ssp->xs_busy = true;
	unlock_sema();

	return true;
}

bool EXTERNAL_SCHEDULER_MANAGER::is_xs_running()
{
	//std::string xs_file(this->xs_directory() + "/" + XS_LOCK_FILE);

	//return this->file_exists(xs_file);
	return ssp->xs_busy;
}

bool EXTERNAL_SCHEDULER_MANAGER::run_xs()
{
	log_messages.printf(MSG_NORMAL, "[XSM] Calling the external scheduler \"%s\"\n",
			config.xs_executable);

	this->dump_xs_data();

	//TODO: pass xs_input file as parameter to XS
	std::string exec(config.xs_executable);
	exec.append(" ");
	exec.append(this->xs_directory().c_str());
	exec.append("/");
	exec.append(this->xsm_output_data_file);
	exec.append(" ");
	exec.append(this->xs_directory().c_str());
	exec.append("/");
	exec.append(this->xsm_input_data_file);

	log_messages.printf(MSG_NORMAL, "[XSM] %s \n",
			exec.c_str());

	int r = system(exec.c_str());

	if(r == -1){
		log_messages.printf(MSG_NORMAL, "[XSM]  Failed to call \"%s\"\n",
					config.xs_executable);
	}else{
		log_messages.printf(MSG_NORMAL, "[XSM] Call output is \"%d\"\n",
							r);
	}

	this->log_xs();

	return this->update_queues();
}

bool EXTERNAL_SCHEDULER_MANAGER::log_xs()
{
	time_t now = time(NULL);

	log_messages.printf(MSG_NORMAL, "[XSM] Logging external scheduler's last call at time \"%d\"\n",
			now);

	std::string file_name(this->xs_directory() + "/" + LAST_XS_RUN);

	std::ofstream foutput;
	foutput.open(file_name.c_str());
	std::stringstream time_now;
	time_now << now;

	foutput.write(time_now.str().c_str(), std::char_traits<char>::length(time_now.str().c_str()));

	foutput.close();

	return foutput;
}

bool EXTERNAL_SCHEDULER_MANAGER::update_request()
{
	this->remove_host_from_waiting_queue();

	log_messages.printf(MSG_NORMAL, "[XSM] Updating request for [HOST%d].\n",
			g_reply->host.id);

	return this->add_host_to_waiting_queue();
}

bool EXTERNAL_SCHEDULER_MANAGER::remove_host_from_waiting_queue()
{
	vector<SCHEDULER_REQUEST>::iterator it;

	for (it = this->waiting_queue.begin(); it != this->waiting_queue.end(); it++){
		if ((*it).hostid == g_reply->host.id){
			this->waiting_queue.erase(it);

			log_messages.printf(MSG_NORMAL, "[XSM] Removed [HOST%d] from WAITING QUEUE.\n",
					g_reply->host.id);

			return true;
		}
	}

	log_messages.printf(MSG_NORMAL, "[XSM] Failed to remove [HOST%d] from WAITING QUEUE.\n",
			g_reply->host.id);

	return false;
	//bool removed = false;

	//for (int i = 0; i < ssp->max_xs_waiting_queue_units; i++){
	//	if (ssp->xs_waiting_queue[i].hostid != 0){
	//		if (ssp->xs_waiting_queue[i].hostid == g_reply->host.id){
	//			ssp->xs_waiting_queue[i].clear();
	//			removed = true;
	//			break;
	//		}
	//	}
	//}

	//return removed;
}

bool EXTERNAL_SCHEDULER_MANAGER::is_process_xs()
{
	bool process_xs = false;

	log_messages.printf(MSG_NORMAL, "[XSM] Checking if WAITING QUEUE can be processed\n");

	if (config.xs_run_interval >= 0){

		//use time & size
		time_t now = time(NULL);
		time_t then;
		time_t difference;

		std::string last_run_file_name(this->xs_directory() + "/" + LAST_XS_RUN);
		std::string slast_run;

		if(this->file_exists(last_run_file_name))
		{
			std::ifstream last_run_file(last_run_file_name.c_str());

			if(last_run_file.is_open())
			{
				std::getline(last_run_file, slast_run);

				then = (time_t)atoi(slast_run.c_str());
				difference = now - then;

				if (difference > config.xs_run_interval){
					process_xs = true;
				}
			}
		}else{//log timestamp for the first time
			std::ofstream last_run_file(last_run_file_name.c_str());

			if(last_run_file.is_open())
			{
				last_run_file << now;

				last_run_file.close();
			}
		}

	}else{
		//use size only
		if (this->waiting_queue.size() >= config.xs_wq_threshold){
			process_xs = true;
		}
		//int count = 0;

		//for (int i = 0; i < ssp->max_xs_waiting_queue_units; i++){
		//	if (ssp->xs_waiting_queue[i].hostid != 0){
		//		count++;

		//		if (count > config.xs_wq_threshold){
		//			process_xs = true;
		//			break;
		//		}
		//	}
		//}
	}

	return process_xs;
}

bool EXTERNAL_SCHEDULER_MANAGER::release_waiting_queue()
{
	//std::string wq_file(this->xs_directory() + "/" + WQ_LOCK_FILE);

	//return this->delete_file(wq_file);

	log_messages.printf(MSG_NORMAL, "[XSM] Releasing WAITING QUEUE\n");

	lock_sema();
	ssp->waiting_queue_busy = false;
	unlock_sema();

	return true;
}

bool EXTERNAL_SCHEDULER_MANAGER::release_xs()
{
	///std::string xs_file(this->xs_directory() + "/" + XS_LOCK_FILE);

	//return this->delete_file(xs_file);

	log_messages.printf(MSG_NORMAL, "[XSM] Releasing EXTERNAL SCHEDULER\n");

	lock_sema();
	ssp->xs_busy = false;
	unlock_sema();

	return true;
}

const char* EXTERNAL_SCHEDULER_MANAGER::read_waiting_queue()
{
	log_messages.printf(MSG_NORMAL,
			"[XSM] Trying to read WAITING QUEUE from file\n");

	std::string filedir(this->xs_directory() + "/" + WQ_FILE);

	if(this->file_exists(filedir)){
		FILE* file;
		file = fopen(filedir.c_str(), "r");

	    MIOFILE mf;
	    XML_PARSER xp(&mf);
	    mf.init_file(file);

	    if (xp.get_tag()) {
	        return "xp.get_tag() failed";
	    }
	    if (xp.match_tag("?xml")) {
	        xp.get_tag();
	    }

	    if (!xp.match_tag("requests")) return "no start tag on readWQ\n";

	    int turn = 1;

	    while (1)
	    {
	        SCHEDULER_REQUEST* request = new SCHEDULER_REQUEST();

	        bool r = this->parse_request(&xp, request);

	        if(!r){
	        	log_messages.printf(MSG_NORMAL, "[XSM] Getting tag from read_waiting_queue's main loop. R: %d.\n",
	        			r ? 1 : 0);
				if(xp.match_tag("/requests")){
					log_messages.printf(MSG_NORMAL,
							"[XSM] WAITING QUEUE parsed successfully.\n");
					return NULL;
				}
				else{
					return "error";
				}

	        }else{
	        	this->waiting_queue.push_back(*request);
	        }

	        log_messages.printf(MSG_NORMAL, "[XSM] Turn %d completed.\n",
	        		turn);
	        turn++;
	        log_messages.printf(MSG_NORMAL, "[XSM] Read host's authenticator: %s .\n",
	        		request->authenticator);
	    }
	}

	log_messages.printf(MSG_NORMAL,"[XSM] WAITING QUEUE [FILE:%s] was not found\n",
			filedir.c_str());

	return "no waiting queue file found";
}

bool EXTERNAL_SCHEDULER_MANAGER::write_waiting_queue()
{
	std::string wq_file(this->xs_directory() + "/" + WQ_FILE);

	int count = 0;

	//lock_sema();

	//for (int i = 0; i < ssp->max_xs_waiting_queue_units; i++){
	//	if (ssp->xs_waiting_queue[i].hostid != 0){
	//		count++;
	//	}
	//}

	if(this->waiting_queue.size())
	{
		FILE* fout;

		fout = fopen(wq_file.c_str(), "w");

		fprintf(fout,
				"<requests>\n"
		);

		for(int i = 0; i < this->waiting_queue.size(); i++)
		{
			this->save_request(fout, &this->waiting_queue[i]);
			//if (ssp->xs_waiting_queue[i].hostid != 0){
			//	this->save_request(fout, &ssp->xs_waiting_queue[i]);
			//}
		}

		fprintf(fout,
				"</requests>\n"
		);

	    copy_stream(fout, stdout);

		fclose(fout);

		//unlock_sema();

		return true;
	}

	//this->delete_file(wq_file);

	//unlock_sema();

	return false;
}

bool EXTERNAL_SCHEDULER_MANAGER::update_queues()
{
	log_messages.printf(MSG_NORMAL, "[XSM] Updating queues\n");

	//1. read from xs_output file,
	if (this->read_xs_data()){

		std::vector<RESULT_ASSIGNMENT>::iterator itassignment;
		std::vector<SCHEDULER_REQUEST>::iterator j;

		//Because the HOST structure is not fully parsed, we have to use the request's hostid variable to compare hosts

		for(itassignment = this->assignments.begin(); itassignment !=  this->assignments.end(); itassignment++) {
			for(j = waiting_queue.begin(); j < waiting_queue.end(); j++){
				if((*itassignment).host_id == (*j).hostid){
					waiting_queue.erase(j);
					j--;

					//we should be able to break from the cycle at this point
					//but in case there are double entries for the same host
					//we'll keep looping
				}
			}

			//for (int i = 0; i < ssp->max_xs_waiting_queue_units; i++){
			//	if (ssp->xs_waiting_queue[i].hostid != 0){
			//		if (ssp->xs_waiting_queue[i].hostid == (*itassignment).host_id){
			//			ssp->xs_waiting_queue[i].clear();
			//		}
			//	}
			//}
		}

		this->write_waiting_queue();

		for (itassignment = this->assignments.begin(); itassignment !=  this->assignments.end(); itassignment++){
			log_messages.printf(MSG_NORMAL, "[XSM] XS assigned result %i from workunit %i to host %i\n",
					(*itassignment).result_id, (*itassignment).workunit_id, (*itassignment).host_id);
		}


		//mark assgined jobs on ssp | MATCH WORKUNIT & RESULT ID, ASSIGN HOST ID TO WU_RESULT
		lock_sema();

		std::vector<RESULT_ASSIGNMENT>::iterator lassignment;

		for (lassignment = this->assignments.begin(); lassignment !=  this->assignments.end(); lassignment++){
			for (int iwuresult = 0; iwuresult < ssp->max_wu_results; iwuresult++){

				//log_messages.printf(MSG_NORMAL, "CHECKING WORK UNIT %i\n", iwuresult);

				if ((*lassignment).workunit_id == ssp->wu_results[iwuresult].workunit.id){

					log_messages.printf(MSG_NORMAL, "[XSM] WORKUNIT %i found match for host %i.\n",
							ssp->wu_results[iwuresult].workunit.id,(*lassignment).host_id);

					if ((*lassignment).result_id == ssp->wu_results[iwuresult].resultid){


						log_messages.printf(MSG_NORMAL, "[XSM] RESULT %i found match for host %i.\n",
								ssp->wu_results[iwuresult].resultid,(*lassignment).host_id);

						ssp->wu_results[iwuresult].state = (*lassignment).host_id;

						log_messages.printf(MSG_NORMAL, "[XSM] Marked WU_RESULT for host %i.\n",
								(*lassignment).host_id);
					}
				}
			}
		}

		//for (int iwuresult = 0; iwuresult < ssp->max_wu_results; iwuresult++){

			//if (ssp->wu_results[iwuresult].state == WR_STATE_PRESENT){

				//log_messages.printf(MSG_NORMAL, "CHECKING WORK UNIT %i:\n",
				//		iwuresult);
				//log_messages.printf(MSG_NORMAL, "result id: %i | wu_id: %i | state %i | app name: %s\n",
				//		ssp->wu_results[iwuresult].resultid,
				//		ssp->wu_results[iwuresult].workunit.id,
				//		ssp->wu_results[iwuresult].state,
				//		ssp->wu_results[iwuresult].workunit.app_name);
			//}

		//}

		unlock_sema();

		log_messages.printf(MSG_NORMAL, "[XSM] Queues were updated successfully.\n");

		return true;
	}

	log_messages.printf(MSG_NORMAL, "[XSM] Failed to update queues.\n");
	return false;
}

void EXTERNAL_SCHEDULER_MANAGER::dump_xs_data()
{
	FILE* fout;

	std::string filename(this->xs_directory() + "/" + xsm_output_data_file);

	fout = fopen(filename.c_str(), "w");

	//only send work that is available

	lock_sema();

	//
	// SHARED MEMMORY
	//
	fprintf(fout,
			"<xs_input>\n"
			"	<project>\n"
	);

	fprintf(fout,
			"		<shared_mem>\n"
			"			<ready>%d</ready>\n"
			"			<ss_size>%d</ss_size>\n"
			"			<platform_size>%d</platform_size>\n"
			"			<app_size>%d</app_size>\n"
			"			<app_version_size>%d</app_version_size>\n"
			"			<assignment_size>%d</assignment_size>\n"
			"			<wu_result_size>%d</wu_result_size>\n"
			"			<nplatforms>%d</nplatforms>\n"
			"			<napps>%d</napps>\n"
			"			<app_weight_sum>%f</app_weight_sum>\n"
			"			<napp_versions>%d</napp_versions>\n"
			"			<nassignments>%d</nassignments>\n"
			"			<max_platforms>%d</max_platforms>\n"
			"			<max_apps>%d</max_apps>\n"
			"			<max_app_versions>%d</max_app_versions>\n"
			"			<max_assignments>%d</max_assignments>\n"
			"			<max_wu_results>%d</max_wu_results>\n"
			"			<have_cpu_apps>%d</have_cpu_apps>\n"
			"			<have_cuda_apps>%d</have_cuda_apps>\n"
			"			<have_ati_apps>%d</have_ati_apps>\n"
			"			<perf_info>\n"
			"				<host_fpops_mean>%f</host_fpops_mean>\n"
			"				<host_fpops_stddev>%f</host_fpops_stddev>\n"
			"				<host_fpops_50_percentile>%f</host_fpops_50_percentile>\n"
			"				<host_fpops_95_percentile>%f</host_fpops_95_percentile>\n"
			"			</perf_info>\n"
			"			<platforms>\n",
						ssp->ready? 1 : 0,
						ssp->ss_size,
						ssp->platform_size,
						ssp->app_size,
						ssp->app_version_size,
						ssp->assignment_size,
						ssp->wu_result_size,
						ssp->nplatforms,
						ssp->napps,
						ssp->app_weight_sum,
						ssp->napp_versions,
						ssp->nassignments,
						ssp->max_platforms,
						ssp->max_apps,
						ssp->max_app_versions,
						ssp->max_assignments,
						ssp->max_wu_results,
						ssp->have_cpu_apps ? 1 : 0,
						ssp->have_cuda_apps ? 1 : 0,
						ssp->have_ati_apps ? 1 : 0,
								ssp->perf_info.host_fpops_mean,
								ssp->perf_info.host_fpops_stddev,
								ssp->perf_info.host_fpops_50_percentile,
								ssp->perf_info.host_fpops_95_percentile
	);

	for (int iplatform = 0; iplatform < ssp->nplatforms; iplatform++)
	{
		if(&(ssp->platforms[iplatform]) == NULL) continue;


	fprintf(fout,
			"				<platform>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<name>%s</name>\n"
			"					<user_friendly_name>%s</user_friendly_name>\n"
			"					<deprecated>%d</deprecated>\n"
			"				</platform>\n",
								ssp->platforms[iplatform].id,
								ssp->platforms[iplatform].create_time,
								ssp->platforms[iplatform].name,
								ssp->platforms[iplatform].user_friendly_name,
								ssp->platforms[iplatform].deprecated
	);

	}

	fprintf(fout,
			"			</platforms>\n"
			"			<apps>\n"

	);

	for (int iapp = 0; iapp < ssp->napps; iapp++)
	{
		if(&(ssp->apps[iapp]) == NULL) continue;

	fprintf(fout,
			"				<app>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<name>%s</name>\n"
			"					<min_version>%d</min_version>\n"
			"					<deprecated>%d</deprecated>\n"
			"					<user_friendly_name>%s</user_friendly_name>\n"
			"					<homogeneous_redundency>%d</homogeneous_redundency>\n"
			"					<weight>%f</weight>\n"
			"					<beta>%d</beta>\n"
			"					<target_nresults>%d</target_nresults>\n"
			"					<min_avg_pfc>%f</min_avg_pfc>\n"
			"					<host_scale_check>%d</host_scale_check>\n"
			"					<homogeneous_app_version>%d</homogeneous_app_version>\n"
			"				</app>\n",
								ssp->apps[iapp].id,
								ssp->apps[iapp].create_time,
								ssp->apps[iapp].name,
								ssp->apps[iapp].min_version,
								ssp->apps[iapp].deprecated ? 1 : 0,
								ssp->apps[iapp].user_friendly_name,
								ssp->apps[iapp].homogeneous_redundancy,
								ssp->apps[iapp].weight,
								ssp->apps[iapp].beta ? 1 : 0,
								ssp->apps[iapp].target_nresults,
								ssp->apps[iapp].min_avg_pfc,
								ssp->apps[iapp].host_scale_check ? 1 : 0,
								ssp->apps[iapp].homogeneous_app_version ? 1 : 0

	);

	}

	fprintf(fout,
			"			</apps>\n"
			"			<app_versions>\n"
	);

	for (int iapp_version = 0; iapp_version < ssp->napp_versions; iapp_version++)
	{
		if(&(ssp->app_versions[iapp_version]) == NULL) continue;

	fprintf(fout,
			"				<app_version>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<appid>%d</appid>\n"
			"					<version_num>%d</version_num>\n"
			"					<platformid>%d</platformid>\n"
			"					<xml_doc>%s</xml_doc>\n"
			"					<min_core_version>%d</min_core_version>\n"
			"					<max_core_version>%d</max_core_version>\n"
			"					<deprecated>%d</deprecated>\n"
			"					<plan_class>%s</plan_class>\n"
			"					<pfc>\n"
			"						<n>%f</n>\n"
			"						<avg>%f</avg>\n"
			"					</pfc>\n"
			"					<pfc_scale>%f</pfc_scale>\n"
			"					<expavg_credit>%f</expavg_credit>\n"
			"					<expavg_time>%f</expavg_time>\n"
			"				</app_version>\n",
								ssp->app_versions[iapp_version].id,
								ssp->app_versions[iapp_version].create_time,
								ssp->app_versions[iapp_version].appid,
								ssp->app_versions[iapp_version].version_num,
								ssp->app_versions[iapp_version].platformid,
								ssp->app_versions[iapp_version].xml_doc,
								ssp->app_versions[iapp_version].min_core_version,
								ssp->app_versions[iapp_version].max_core_version,
								ssp->app_versions[iapp_version].deprecated ? 1 : 0,
								ssp->app_versions[iapp_version].plan_class,
									ssp->app_versions[iapp_version].pfc.n,
									ssp->app_versions[iapp_version].pfc.avg,
								ssp->app_versions[iapp_version].pfc_scale,
								ssp->app_versions[iapp_version].expavg_credit,
								ssp->app_versions[iapp_version].expavg_time
	);

	}

	fprintf(fout,

			"			</app_versions>\n"
			"			<assignments>\n"
	);


	for (int iassignment = 0; iassignment < ssp->nassignments; iassignment++)
	{
		if(&(ssp->assignments[iassignment]) == NULL) continue;

	fprintf(fout,
			"				<assignment>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<target_id>%d</target_id>\n"
			"					<target_type>%d</target_type>\n"
			"					<multi>%d</multi>\n"
			"					<workunitid>%d</workunitid>\n"
			"					<result_id>%d</result_id>\n"
			"				</assignment>\n",
								ssp->assignments[iassignment].id,
								ssp->assignments[iassignment].create_time,
								ssp->assignments[iassignment].target_id,
								ssp->assignments[iassignment].target_type,
								ssp->assignments[iassignment].multi,
								ssp->assignments[iassignment].workunitid,
								ssp->assignments[iassignment].resultid
	);

	}

	fprintf(fout,
			"			</assignments>\n"
			"			<wu_results>\n"
	);

	for (int iwu_result = 0; iwu_result < ssp->max_wu_results; iwu_result++)
	{
		if(&(ssp->wu_results[iwu_result]) == NULL) continue;

		//if it' has been assigned to a host
		//if(ssp->wu_results[iwu_result].state > 0) continue;

		//Print out WU_RESULTs that are available for scheduling
		if(ssp->wu_results[iwu_result].state != WR_STATE_PRESENT ) continue;

	fprintf(fout,
			"				<wu_result>\n"
			"					<state>%d</state>\n"
			"					<infeasible_count>%d</infeasible_count>\n"
			"					<need_reliable>%d</need_reliable>\n"
			"					<workunit>\n"
			"						<id>%d</id>\n"
			"						<create_time>%d</create_time>\n"
			"						<appid>%d</appid>\n"
			"						<name>%s</name>\n"
			"						<xml_doc>%s</xml_doc>\n"
			"						<batch>%d</batch>\n"
			"						<rsc_fpops_est>%f</rsc_fpops_est>\n"
			"						<rsc_fpops_bound>%f</rsc_fpops_bound>\n"
			"						<rsc_memory_bound>%f</rsc_memory_bound>\n"
			"						<rsc_disk_bound>%f</rsc_disk_bound>\n"
			"						<need_validate>%d</need_validate>\n"
			"						<canonical_resultid>%d</canonical_resultid>\n"
			"						<canonical_credit>%f</canonical_credit>\n"
			"						<transition_time>%d</transition_time>\n"
			"						<delay_bound>%d</delay_bound>\n"
			"						<error_mask>%d</error_mask>\n"
			"						<file_delete_state>%d</file_delete_state>\n"
			"						<assimilate_state>%d</assimilate_state>\n"
			"						<hr_class>%d</hr_class>\n"
			"						<opaque>%f</opaque>\n"
			"						<min_quorum>%d</min_quorum>\n"
			"						<target_nresults>%d</target_nresults>\n"
			"						<max_error_results>%d</max_error_results>\n"
			"						<max_total_results>%d</max_total_results>\n"
			"						<max_success_results>%d</max_success_results>\n"
			"						<result_template_file>%s</result_template_file>\n"
			"						<priority>%d</priority>\n"
			"						<mod_time>%s</mod_time>\n"
			"						<rsc_bandwidth_bound>%f</rsc_bandwidth_bound>\n"
			"						<fileset_id>%d</fileset_id>\n"
			"						<app_version_id>%d</app_version_id>\n"
			"						<app_name>%s</app_name>\n"
			"					</workunit>\n"
			"					<resultid>%d</resultid>\n"
			"					<time_added_to_shared_memory>%d</time_added_to_shared_memory>\n"
			"					<res_priority>%d</res_priority>\n"
			"					<res_server_state>%d</res_server_state>\n"
			"					<res_report_deadline>%f</res_report_deadline>\n"
			"					<fpops_size>%f</fpops_size>\n"
			"				</wu_result>\n",
								ssp->wu_results[iwu_result].state,
								ssp->wu_results[iwu_result].need_reliable ? 1 : 0,
								ssp->wu_results[iwu_result].infeasible_count,
									ssp->wu_results[iwu_result].workunit.id,
									ssp->wu_results[iwu_result].workunit.create_time,
									ssp->wu_results[iwu_result].workunit.appid,
									ssp->wu_results[iwu_result].workunit.name,
									ssp->wu_results[iwu_result].workunit.xml_doc,
									ssp->wu_results[iwu_result].workunit.batch,
									ssp->wu_results[iwu_result].workunit.rsc_fpops_est,
									ssp->wu_results[iwu_result].workunit.rsc_fpops_bound,
									ssp->wu_results[iwu_result].workunit.rsc_memory_bound,
									ssp->wu_results[iwu_result].workunit.rsc_disk_bound,
									ssp->wu_results[iwu_result].workunit.need_validate ? 1 : 0,
									ssp->wu_results[iwu_result].workunit.canonical_resultid,
									ssp->wu_results[iwu_result].workunit.canonical_credit,
									ssp->wu_results[iwu_result].workunit.transition_time,
									ssp->wu_results[iwu_result].workunit.delay_bound,
									ssp->wu_results[iwu_result].workunit.error_mask,
									ssp->wu_results[iwu_result].workunit.file_delete_state,
									ssp->wu_results[iwu_result].workunit.assimilate_state,
									ssp->wu_results[iwu_result].workunit.hr_class,
									ssp->wu_results[iwu_result].workunit.opaque,
									ssp->wu_results[iwu_result].workunit.min_quorum,
									ssp->wu_results[iwu_result].workunit.target_nresults,
									ssp->wu_results[iwu_result].workunit.max_error_results,
									ssp->wu_results[iwu_result].workunit.max_total_results,
									ssp->wu_results[iwu_result].workunit.max_success_results,
									ssp->wu_results[iwu_result].workunit.result_template_file,
									ssp->wu_results[iwu_result].workunit.priority,
									ssp->wu_results[iwu_result].workunit.mod_time,
									ssp->wu_results[iwu_result].workunit.rsc_bandwidth_bound,
									ssp->wu_results[iwu_result].workunit.fileset_id,
									ssp->wu_results[iwu_result].workunit.app_version_id,
									ssp->wu_results[iwu_result].workunit.app_name,
								ssp->wu_results[iwu_result].resultid,
								ssp->wu_results[iwu_result].time_added_to_shared_memory,
								ssp->wu_results[iwu_result].res_priority,
								ssp->wu_results[iwu_result].res_server_state,
								ssp->wu_results[iwu_result].res_report_deadline,
								ssp->wu_results[iwu_result].fpops_size
	);

	}

	unlock_sema();

	fprintf(fout,
			"			</wu_results>\n"
			"		</shared_mem>\n"
			"	</project>\n"
			"	<clients>\n"
			"		<requests>\n"
	);

	//hosts on WQ

	std::vector<SCHEDULER_REQUEST> list_hosts;

	list_hosts.insert(list_hosts.end(), this->waiting_queue.begin(), this->waiting_queue.end());
	//for (int i = 0; i < ssp->max_xs_waiting_queue_units; i++){
	//		if (ssp->xs_waiting_queue[i].hostid != 0){
	//			list_hosts.push_back(ssp->xs_waiting_queue[i]);
	//		}
	//}

	for (int irq = 0; irq < list_hosts.size(); irq++)
	{
		SCHEDULER_REQUEST* request = &(list_hosts.at(irq));

		this->save_request(fout, request);

	}	//end of request

//	unlock_sema();

	fprintf(fout,
			"		</requests>\n"
			"	</clients>\n"
			"</xs_input>\n"
	);


    copy_stream(fout, stdout);

    log_messages.printf(MSG_NORMAL, "[XSM] Successfully dumped XS data\n");

    fclose(fout);
}

bool EXTERNAL_SCHEDULER_MANAGER::read_xs_data()
{
	const char* result = this->parse_xs_data(this->assignments);

	log_messages.printf(MSG_NORMAL, "[XSM] read_xs_data result:  %s.\n",
			result);

	if(!result)	{

		std::string file_name(this->xs_directory() + "/" + this->xsm_input_data_file);

		this->delete_file(file_name);

		return true;
	}

	return false;
}

std::string EXTERNAL_SCHEDULER_MANAGER::xs_directory(){

	std::string address(config.project_path(XS_DIRECTORY.c_str()),
			std::char_traits<char>::length(config.project_path(XS_DIRECTORY.c_str())));

	return address;
}

const char* EXTERNAL_SCHEDULER_MANAGER::parse_xs_data(std::vector<RESULT_ASSIGNMENT> &list_host_assignment )
{
	//read input data from the external scheduler
	//should be a sequence of hosts ids
	//update pq by getting SCHED_REQ data from WQ. (append items to current PQ)

	std::string in_file(this->xs_directory() + "/" + xsm_input_data_file);

	if (this->file_exists(in_file) == false) return "no file";

	FILE* fin = fopen(in_file.c_str(), "r");

    MIOFILE mf;
    XML_PARSER xp(&mf);
    mf.init_file(fin);

    int hostid;
    int workunitid;
    int resultid;

    if (xp.get_tag()) {
        return "xp.get_tag() failed";
    }
    if (xp.match_tag("?xml")) {
        xp.get_tag();
    }

    if (!xp.match_tag("xs_output")) return "no start tag";

    while (!xp.get_tag()) {
        if (xp.match_tag("/xs_output")) {	//End of request file

        	log_messages.printf(MSG_NORMAL, "[XSM] Read external schdeduler's data\n");

        	fclose(fin);

            return NULL;
        }

        if (xp.match_tag("assignment")){

        	while(!xp.get_tag()){
        		if (xp.match_tag("/assignment")) {
        			list_host_assignment.push_back(*(new RESULT_ASSIGNMENT(hostid, workunitid, resultid)));
        			break;
        		}
        		if(xp.parse_int("host_id", hostid)){
        			log_messages.printf(MSG_NORMAL, "[XSM] Found host id %d\n", hostid);
        			continue;
        		}
        		if(xp.parse_int("wu_id", workunitid)) continue;
        		if(xp.parse_int("result_id", resultid)) continue;
        	}
        }
    }


    log_messages.printf(MSG_NORMAL, "[XSM] Failed to read external data\n");

    return "error";
}

EXTERNAL_SCHEDULER_MANAGER::EXTERNAL_SCHEDULER_MANAGER()
{
}

EXTERNAL_SCHEDULER_MANAGER::~EXTERNAL_SCHEDULER_MANAGER(){}

EXTERNAL_SCHEDULER_MANAGER::RESULT_ASSIGNMENT::RESULT_ASSIGNMENT(){
	this->host_id = this->workunit_id = this->result_id = -1;
}

EXTERNAL_SCHEDULER_MANAGER::RESULT_ASSIGNMENT::RESULT_ASSIGNMENT(int hostid, int workunitid, int resultid){
	this->host_id = hostid;
	this->workunit_id = workunitid;
	this->result_id = resultid;
}

EXTERNAL_SCHEDULER_MANAGER::RESULT_ASSIGNMENT::~RESULT_ASSIGNMENT(){}




const char *BOINC_RCSID_ea659117b3 = "$Id: sched_types.cpp 25017 2012-01-09 17:35:48Z davea $";


