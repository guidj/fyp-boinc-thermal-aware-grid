
void testDumpXS()
{
	FILE* fout;

	fout = fopen("trial.xml", "w");

	//lock ssp

	//dump data

	//
	// SHARED MEMMORY
	//
	fprintf(fout,
			"<ext_sched_data>\n"
			"	<project>\n"
	);

	fprintf(fout,
			"		<shared_mem>\n"
			"			<ready>%d</ready>\n"
			"			<ss_size>%d</ss_size>\n"
			"			<platform_size>%d</platform_size>\n"
			"			<app_size>%d</app_size>\n"
			"			<app_version_size>%d</app_version_size>\n"
			"			<assignment_size>%d</assignment_size>\n"
			"			<wu_result_size>%d</wu_result_size>\n"
			"			<nplatforms>%d</nplatforms>\n"
			"			<napps>%d</napps>\n"
			"			<app_weight_sum>%f</app_weight_sum>\n"
			"			<napp_versions>%d</napp_versions>\n"
			"			<nassignments>%d</nassignments>\n"
			"			<max_platforms>%d</max_platforms>\n"
			"			<max_apps>%d</max_apps>\n"
			"			<max_app_versions>%d</max_app_versions>\n"
			"			<max_assignments>%d</max_assignments>\n"
			"			<max_wu_results>%d</max_wu_results>\n"
			"			<have_cpu_apps>%d</have_cpu_apps>\n"
			"			<have_cuda_apps>%d</have_cuda_apps>\n"
			"			<have_ati_apps>%d</have_ati_apps>\n"
			"			<perf_info>\n"
			"				<host_fpops_mean>%f</host_fpops_mean>\n"
			"				<host_fpops_stddev>%f</host_fpops_stddev>\n"
			"				<host_fpops_50_percentile>%f</host_fpops_50_percentile>\n"
			"				<host_fpops_95_percentile>%f</host_fpops_95_percentile>\n"
			"			</perf_info>\n"
			"			<platforms>\n",
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
						1,
								1,
								1,
								1,
								1
	);

	for (int iplatform = 0; iplatform < 2; iplatform++)
	{
		//if(&(ssp->platforms[iplatform]) == NULL) continue;

	fprintf(fout,
			"				<platform>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<name>%s</name>\n"
			"					<user_friendly_name>%s</user_friendly_name>\n"
			"					<deprecated>%d</deprecated>\n"
			"				</platform>\n",
								1,
								1,
								"name",
								"user friendly name",
								1
	);

	}

	fprintf(fout,
			"			</platforms>\n"
			"			<apps>\n"

	);

	for (int iapp = 0; iapp < 2; iapp++)
	{
		//if(&(ssp->apps[iapp]) == NULL) continue;

	fprintf(fout,
			"				<app>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<name>%s</name>\n"
			"					<min_version>%d</min_version>\n"
			"					<deprecated>%d</deprecated>\n"
			"					<user_friendly_name>%s</user_friendly_name>\n"
			"					<homogeneous_redundency>%d</homogeneous_redundency>\n"
			"					<weight>%f</weight>\n"
			"					<beta>%d</beta>\n"
			"					<target_nresults>%d</target_nresults>\n"
			"					<min_avg_pfc>%f</min_avg_pfc>\n"
			"					<host_scale_check>%d</host_scale_check>\n"
			"					<homogeneous_app_version>%d</homogeneous_app_version>\n"
			"				</app>\n",
								1,
								1,
								"name",
								1,
								1,
								"user friendly name",
								1,
								1,
								1,
								1,
								1,
								1,
								1

	);

	}

	fprintf(fout,
			"			</apps>\n"
			"			<app_versions>\n"
	);

	for (int iapp_version = 0; iapp_version < 2; iapp_version++)
	{
		//if(&(ssp->app_versions[iapp_version]) == NULL) continue;

	fprintf(fout,
			"				<app_version>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<appid>%d</appid>\n"
			"					<version_num>%d</version_num>\n"
			"					<platformid>%d</platformid>\n"
			"					<xml_doc>%s</xml_doc>\n"
			"					<min_core_version>%d</min_core_version>\n"
			"					<max_core_version>%d</max_core_version>\n"
			"					<deprecated>%d</deprecated>\n"
			"					<plan_class>%s</plan_class>\n"
			"					<pfc>\n"
			"						<n>%f</n>\n"
			"						<avg>%f</avg>\n"
			"					</pfc>\n"
			"					<pfc_scale>%f</pfc_scale>\n"
			"					<expavg_credit>%f</expavg_credit>\n"
			"					<expavg_time>%f</expavg_time>\n"
			"				</app_version>\n",
								1,
								1,
								1,
								1,
								1,
								"xml",
								1,
								1,
								1,
								"class",
									1,
									1,
								1,
								1,
								1
	);

	}

	fprintf(fout,

			"			</app_versions>\n"
			"			<assignments>\n"
	);


	for (int iassignment = 0; iassignment < 2; iassignment++)
	{
		//if(&(ssp->assignments[iassignment]) == NULL) continue;

	fprintf(fout,
			"				<assignment>\n"
			"					<id>%d</id>\n"
			"					<create_time>%d</create_time>\n"
			"					<target_id>%d</target_id>\n"
			"					<target_type>%d</target_type>\n"
			"					<multi>%d</multi>\n"
			"					<workunitid>%d</workunitid>\n"
			"					<result_id>%d</result_id>\n"
			"				</assignment>\n",
								1,
								1,
								1,
								1,
								1,
								1,
								1
	);

	}

	fprintf(fout,
			"			</assignments>\n"
			"			<wu_results>\n"
	);

	for (int iwu_result = 0; iwu_result < 0; iwu_result++)
	{
		//if(&(ssp->wu_results[iwu_result]) == NULL) continue;

	fprintf(fout,
			"				<wu_result>\n"
			"					<state>%d</state>\n"
			"					<infeasible_count>%d</infeasible_count>\n"
			"					<need_reliable>%d</need_reliable>\n",
										1,
										1,
										1
	);

	fprintf(fout,
			"					<workunit>\n"
			"						<id>%d</id>\n"
			"						<create_time>%d</create_time>\n"
			"						<appid>%d</appid>\n"
			"						<name>%s</name>\n"
			"						<xml_doc>%s</xml_doc>\n"
			"						<batch>%d</batch>\n"
			"						<rsc_fpops_est>%f</rsc_fpops_est>\n"
			"						<rsc_fpops_bound>%f</rsc_fpops_bound>\n"
			"						<rsc_memory_bound>%f</rsc_memory_bound>\n"
			"						<rsc_disk_bound>%f</rsc_disk_bound>\n"
			"						<need_validate>%d</need_validate>\n"
			"						<canonical_resultid>%d</canonical_resultid>\n"
			"						<canonical_credit>%f</canonical_credit>\n"
			"						<transition_time>%d</transition_time>\n"
			"						<delay_bound>%d</delay_bound>\n"
			"						<error_mask>%d</error_mask>\n"
			"						<file_delete_state>%d</file_delete_state>\n"
			"						<assimilate_state>%d</assimilate_state>\n"
			"						<hr_class>%d</hr_class>\n"
			"						<opaque>%f</opaque>\n"
			"						<min_quorum>%d</min_quorum>\n"
			"						<target_nresults>%d</target_nresults>\n"
			"						<max_error_results>%d</max_error_results>\n"
			"						<max_total_results>%d</max_total_results>\n"
			"						<max_success_results>%d</max_success_results>\n",
									1,
									1,
									1,
									"name",
									"xml",
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1,
									1
	);

	//
	// issue: segmentation fault when writing out app_name's value
	//

	std::string word = "long-word";

	fprintf(fout,
			"						<result_template_file>%s</result_template_file>\n"
			"						<priority>%d</priority>\n"
			"						<mod_time>%s</mod_time>\n"
			"						<rsc_bandwidth_bound>%f</rsc_bandwidth_bound>\n"
			"						<fileset_id>%d</fileset_id>\n"
			"						<app_version_id>%d</app_version_id>\n"
			"						<app_name>%s</app_name>\n"
			"					</workunit>\n",
									"house",
									1,
									"mod",
									1,
									1,
									1,
									"long-word"
	);

	fprintf(fout,
			"					<resultid>%d</resultid>\n"
			"					<time_added_to_shared_memory>%d</time_added_to_shared_memory>\n"
			"					<res_priority>%d</res_priority>\n"
			"					<res_server_state>%d</res_server_state>\n"
			"					<res_report_deadline>%f</res_report_deadline>\n"
			"					<fpops_size>%f</fpops_size>\n"
			"				</wu_result>\n",
								1,
								1,
								1,
								1,
								1,
								1
	);

	}

	//
	// end of "leave as it is"
	//

	fprintf(fout,
			"			</wu_results>\n"
			"		</shared_mem>\n"
			"	</project>\n"
			"	<clients>\n"
			"		<requests>\n"
	);

	//hosts on WQ

	/*for (int irq = 0; irq < this->waiting_queue.size(); irq++)
	{

		SCHEDULER_REQUEST* request = &this->waiting_queue.at(irq);

		fprintf(fout,
				"			<scheduler_request>\n"
				"				<authenticator>%s</authenticator>\n"
				"				<platform>\n"
				"					<name>%s</name>\n"
				"				</platform>\n"
				"				<alt_platforms>\n",
								request->authenticator,
								request->platform.name
		);

		for(int ialt_platform = 0; ialt_platform < request->alt_platforms.size(); ialt_platform++)
		{

		fprintf(fout,
				"					<alt_platform>%s</alt_platform>\n",
									request->alt_platforms.at(ialt_platform).name
		);

		}	//end of alt platforms



		fprintf(fout,
				"				</alt_platforms>\n"
				"				<platforms>\n"
		);


		/*for(int iplatform = 0; iplatform < request->platforms.list.size(); iplatform++)
		{

		fprintf(fout,
				"					<platform>\n"
				"						<id>%d</id>\n"
				"						<create_time>%d</create_time>\n"
				"						<name>%s</name>\n"
				"						<user_friendly_name>%s</user_friendly_name>\n"
				"						<deprecated>%d</deprecated>\n"
				"					</platform>\n",
										request->platforms.list.at(iplatform)->id,
										request->platforms.list.at(iplatform)->create_time,
										request->platforms.list.at(iplatform)->name,
										request->platforms.list.at(iplatform)->user_friendly_name,
										request->platforms.list.at(iplatform)->deprecated

		);

		}	//end of platforms


		fprintf(fout,
				"				</platforms>\n"
				"				<cross_project_id>%s</cross_project_id>\n"
				"				<hostid>%d</hostid>\n"
				"				<core_client_major_version>%d</core_client_major_version>\n"
				"				<core_client_minor_version>%d</core_client_minor_version>\n"
				"				<core_client_release>%d</core_client_release>\n"
				"				<core_client_version>%d</core_client_version>\n"
				"				<rpr_seqno>%d</rpr_seqno>\n"
				"				<work_req_seconds>%f</work_req_seconds>\n"
				"				<cpu_req_secs>%f</cpu_req_secs>\n"
				"				<cpu_req_instances>%f</cpu_req_instances>\n"
				"				<resource_share_fraction>%f</resource_share_fraction>\n"
				"				<rrs_fraction>%f</rrs_fraction>\n"
				"				<prrs_fraction>%f</prrs_fraction>\n"
				"				<cpu_estimated_delay>%f</cpu_estimated_delay>\n"
				"				<duration_correction_factor>%f</duration_correction_factor>\n"
				"				<global_prefs_xml>%s</global_prefs_xml>\n"
				"				<working_global_prefs_xml>%s</working_global_prefs_xml>\n"
				"				<code_sign_key>%s</code_sign_key>\n"
				"				<client_app_versions>\n",
								request->cross_project_id,
								request->hostid,
								request->core_client_major_version,
								request->core_client_minor_version,
								request->core_client_release,
								request->core_client_version,
								request->rpc_seqno,
								request->work_req_seconds,
								request->cpu_req_secs,
								request->cpu_req_instances,
								request->resource_share_fraction,
								request->rrs_fraction,
								request->prrs_fraction,
								request->cpu_estimated_delay,
								request->duration_correction_factor,
								request->global_prefs_xml,
								request->working_global_prefs_xml,
								request->code_sign_key
		);

		for(int iclient_app_version = 0; iclient_app_version < request->client_app_versions.size(); iclient_app_version++)
		{


			CLIENT_APP_VERSION* cav = &request->client_app_versions.at(iclient_app_version);

		fprintf(fout,
				"					<client_app_version>\n"
				"						<app_name>%s</app_name>\n"
				"						<platform>%s</platform>\n"
				"						<version_num>%d</version_num>\n"
				"						<plan_class>%s</plan_class>\n"
				"						<host_usage>\n"
				"							<ncudas>%f</ncudas>\n"
				"							<natis>%f</natis>\n"
				"							<gpu_ram>%f</gpu_ram>\n"
				"							<avg_ncpus>%f</avg_ncpus>\n"
				"							<max_ncpus>%f</max_ncpus>\n"
				"							<projected_flops>%f</projected_flops>\n"
				"							<peak_flops>%f</peak_flops>\n"
				"							<cmdline>%s</cmdline>\n"
				"						</host_usage>\n"
				"						<rsc_fpops_scale>%f</rsc_fpops_scale>\n"
				"						<app>\n"
				"							<id>%d</id>\n"
				"							<create_time>%d</create_time>\n"
				"							<name>%s</name>\n"
				"							<min_version>%d</min_version>\n"
				"							<deprecated>%d</deprecated>\n"
				"							<user_friendly_name>%s</user_friendly_name>\n"
				"							<homogenenous_redundancy>%d</homogenenous_redundancy>\n"
				"							<weight>%f</weight>\n"
				"							<beta>%d</beta>\n"
				"							<target_nresults>%d</target_nresults>\n"
				"							<min_avg_pfc>%f</min_avg_pfc>\n"
				"							<host_scale_check>%d</host_scale_check>\n"
				"							<homogoneous_app_version>%d</homogoneous_app_version>\n"
				"						</app>\n"
				"					</client_app_version>\n",
										cav->app_name,
										cav->platform,
										cav->version_num,
										cav->plan_class,
											cav->host_usage.ncudas,
											cav->host_usage.natis,
											cav->host_usage.gpu_ram,
											cav->host_usage.avg_ncpus,
											cav->host_usage.max_ncpus,
											cav->host_usage.projected_flops,
											cav->host_usage.peak_flops,
											cav->host_usage.cmdline,
										cav->rsc_fpops_scale,
											cav->app->id,
											cav->app->create_time,
											cav->app->name,
											cav->app->min_version,
											cav->app->deprecated ? 1 : 0,
											cav->app->user_friendly_name,
											cav->app->homogeneous_redundancy,
											cav->app->weight,
											cav->app->beta ? 1 : 0,
											cav->app->target_nresults,
											cav->app->min_avg_pfc,
											cav->app->host_scale_check ? 1 : 0,
											cav->app->homogeneous_app_version ? 1 : 0
		);

		}

		fprintf(fout,
				"				</client_app_versions>\n"
				"				<global_prefs>\n"
				"					<mod_time>%f</mod_time>\n"
				"					<disk_max_used_gb>%f</disk_max_used_gb>\n"
				"					<disk_max_used_pct>%f</disk_max_used_pct>\n"
				"					<disk_min_free_gd>%f</disk_min_free_gd>\n"
				"					<work_buf_min_days>%f</work_buf_min_days>\n"
				"					<ram_max_used_busy_frac>%f</ram_max_used_busy_frac>\n"
				"					<ram_max_used_idle_frac>%f</ram_max_used_idle_frac>\n"
				"					<max_ncpus_pct>%f</max_ncpus_pct>\n"
				"				</global_prefs>\n"
				"				<global_prefs_source_mail_hash>%s</global_prefs_source_mail_hash>\n",
									request->global_prefs.mod_time,
									request->global_prefs.disk_max_used_gb,
									request->global_prefs.disk_max_used_pct,
									request->global_prefs.disk_min_free_gb,
									request->global_prefs.work_buf_min_days,
									request->global_prefs.ram_max_used_busy_frac,
									request->global_prefs.ram_max_used_idle_frac,
									request->global_prefs.max_ncpus_pct,
								request->global_prefs_source_email_hash

		);


		fprintf(fout,
				"				<host_info>\n"
				"					<id>%d</id>\n"
				"					<create_time>%d</create_time>\n"
				"					<userid>%d</userid>\n"
				"					<rpc_seqno>%d</rpc_seqno>\n"
				"					<rpc_time>%d</rpc_time>\n"
				"					<total_credit>%f</total_credit>\n"
				"					<expavg_credit>%f</expavg_credit>\n"
				"					<expavg_time>%f</expavg_time>\n"
				"					<timezone>%d</timezone>\n"
				"					<domain_name>%s</domain_name>\n"
				"					<serialnum>%s</serialnum>\n"
				"					<last_ip_addr>%s</last_ip_addr>\n"
				"					<nsame_ip_addr>%d</nsame_ip_addr>\n"
				"					<on_frac>%f</on_frac>\n"
				"					<connected_frac>%f</connected_frac>\n"
				"					<active_frac>%f</active_frac>\n"
				"					<cpu_efficiency>%f</cpu_efficiency>\n"
				"					<duration_correction_factor>%f</duration_correction_factor>\n"
				"					<p_ncpus>%d</p_ncpus>\n"
				"					<p_vendor>%s</p_vendor>\n"
				"					<p_model>%s</p_model>\n"
				"					<p_fpops>%f</p_fpops>\n"
				"					<p_iops>%f</p_iops>\n"
				"					<p_membw>%f</p_membw>\n"
				"					<os_name>%s</os_name>\n"
				"					<os_version>%s</os_version>\n"
				"					<m_nbytes>%f</m_nbytes>\n"
				"					<m_cache>%f</m_cache>\n"
				"					<m_swap>%f</m_swap>\n"
				"					<d_total>%f</d_total>\n"
				"					<d_free>%f</d_free>\n"
				"					<d_boinc_used_total>%f</d_boinc_used_total>\n"
				"					<d_boinc_used_project>%f</d_boinc_used_project>\n"
				"					<d_boinc_max>%f</d_boinc_max>\n"
				"					<n_bwup>%f</n_bwup>\n"
				"					<n_bwdown>%f</n_bwdown>\n"
				"					<credit_per_cpu_sec>%f</credit_per_cpu_sec>\n"
				"					<venue>%s</venue>\n"
				"					<nresults_today>%d</nresults_today>\n"
				"					<avg_turnaround>%f</avg_turnaround>\n"
				"					<host_cpid>%s</host_cpid>\n"
				"					<external_ip_addr>%s</external_ip_addr>\n"
				"					<_max_results_day>%d</_max_results_day>\n"
				"					<_error_rate>%f</_error_rate>\n"
				"					<p_features>%s</p_features>\n"
				"					<virtualbox_version>%s</virtualbox_version>\n"
				"					<p_vm_extensions_disabled>%d</p_vm_extensions_disabled>\n"
				"				</host_info>\n",
									request->host.id,
									request->host.create_time,
									request->host.userid,
									request->host.rpc_seqno,
									request->host.rpc_time,
									request->host.total_credit,
									request->host.expavg_credit,
									request->host.expavg_time,
									request->host.timezone,
									request->host.domain_name,
									request->host.serialnum,
									request->host.last_ip_addr,
									request->host.nsame_ip_addr,
									request->host.on_frac,
									request->host.connected_frac,
									request->host.active_frac,
									request->host.cpu_efficiency,
									request->host.duration_correction_factor,
									request->host.p_ncpus,
									request->host.p_vendor,
									request->host.p_model,
									request->host.p_fpops,
									request->host.p_iops,
									request->host.p_membw,
									request->host.os_name,
									request->host.os_version,
									request->host.m_nbytes,
									request->host.m_cache,
									request->host.m_swap,
									request->host.d_total,
									request->host.d_free,
									request->host.d_boinc_used_total,
									request->host.d_boinc_used_project,
									request->host.d_boinc_max,
									request->host.n_bwup,
									request->host.n_bwdown,
									request->host.credit_per_cpu_sec,
									request->host.venue,
									request->host.nresults_today,
									request->host.avg_turnaround,
									request->host.host_cpid,
									request->host.external_ip_addr,
									request->host._max_results_day,
									request->host._error_rate,
									request->host.p_features,
									request->host.virtualbox_version,
									request->host.p_vm_extensions_disabled ? 1 : 0
		);

		if(request->coprocs.have_ati() || request->coprocs.have_nvidia())
		{
			fprintf(fout,
					"				<coprocs>\n"
			);

		}

		//
		//
		// NVIDIA COPROC
		//

		if(request->coprocs.have_nvidia())
		{
		fprintf(fout,
				"						<coproc_cuda>\n"
		);
		}

			COPROC_NVIDIA* coproc = &(request->coprocs.nvidia);

			if(coproc == NULL) goto END_OF_COPROC_NVIDIA;

			if(!request->coprocs.have_nvidia()) goto END_OF_COPROC_NVIDIA;

		fprintf(fout,
				"							<type>%s</type>\n"
				"							<count>%d</count>\n"
				"							<peak_flops>%f</peak_flops>\n"
				"							<used>%f</used>\n"
				"							<have_cuda>%d</have_cuda>\n"
				"							<have_cal>%d</have_cal>\n"
				"							<have_opencl>%d</have_opencl>\n"
				"							<available_ram>%f</available_ram>\n"
				"							<specified_in_config>%d</specified_in_config>\n"
				"							<req_secs>%f</req_secs>\n"
				"							<req_instances>%f</req_instances>\n"
				"							<estimated_delay>%f</estimated_delay>\n"
				"							<usages>\n",
											coproc->type,
											coproc->count,
											coproc->peak_flops,
											coproc->used,
											coproc->have_cuda ? 1 : 0,
											coproc->have_cal ? 1 : 0,
											coproc->have_opencl ? 1 : 0,
											coproc->available_ram,
											coproc->specified_in_config ? 1 : 0,
											coproc->req_secs,
											coproc->req_instances,
											coproc->estimated_delay
		);

		for(int icoproc_usage = 0; icoproc_usage < coproc->count; icoproc_usage++)
		{

		fprintf(fout,
				"								<usage>%f</usage>\n",
												coproc->usage[icoproc_usage]
		);

		}	//end of coproc usage

		fprintf(fout,
				"							</usages>\n"
				"							<pending_usages>\n"
		);

		for(int icoproc_pending_usage = 0; icoproc_pending_usage < coproc->count; icoproc_pending_usage++)
		{

		fprintf(fout,
				"								<pending_usage>%f</pending_usage>\n",
												coproc->pending_usage[icoproc_pending_usage]
		);

		}	//end of coproc pending usage

		fprintf(fout,
				"							</pending_usages>\n"
				"							<device_nums>\n"
		);

		for(int icoproc_device_num = 0; icoproc_device_num < coproc->count; icoproc_device_num++)
		{

		fprintf(fout,
				"								<device_num>%d</device_num>\n",
												coproc->device_nums[icoproc_device_num]
		);

		}	//end of coproc device num



		fprintf(fout,
				"							</device_nums>\n"
				"							<opencl_device_ids>\n"
		);


		fprintf(fout,
				"							</opencl_device_ids>\n"
				"							<opencl_device_count>%d</opencl_device_count>\n"
				"							<running_graphics_apps>\n",
											coproc->opencl_device_count
		);

		for(int icoproc_running_gfx_app = 0; icoproc_running_gfx_app < coproc->count; icoproc_running_gfx_app++)
		{

		fprintf(fout,
				"								<running_graphics_app>%d</running_graphics_app>\n",
												coproc->running_graphics_app[icoproc_running_gfx_app] ? 1 : 0

		);

		}		//end of coproc running graphics application

		fprintf(fout,
				"							</running_graphics_apps>\n"
				"							<available_ram_temps>\n"
		);

		fprintf(fout,
				"								<available_ram_temp>%f</available_ram_temp>\n",
												coproc->available_ram
		);

		fprintf(fout,
				"							</available_ram_temps>\n"
				"							<last_print_time>%f</last_print_time>\n"
				"							<opencl_prop>\n"
				"								<device_id>%s</device_id>\n"
				"								<name>%s</name>\n"
				"								<vendor>%s</vendor>\n"
				"								<vendor_id>%d</vendor_id>\n"
				"								<available>%d</available>\n"
				"								<half_fp_config>%d</half_fp_config>\n"
				"								<single_fp_config>%d</single_fp_config>\n"
				"								<double_fp_config>%d</double_fp_config>\n"
				"								<endian_little>%d</endian_little>\n"
				"								<execution_capabilities>%d</execution_capabilities>\n"
				"								<extensions>%s</extensions>\n"
				"								<global_mem_size>%f</global_mem_size>\n"
				"								<local_mem_size>%f</local_mem_size>\n"
				"								<max_clock_frequency>%d</max_clock_frequency>\n"
				"								<max_compute_units>%d</max_compute_units>\n",
											coproc->last_print_time,
												coproc->opencl_prop.device_id,
												coproc->opencl_prop.name,
												coproc->opencl_prop.vendor,
												coproc->opencl_prop.vendor_id,
												coproc->opencl_prop.available,
												coproc->opencl_prop.half_fp_config,
												coproc->opencl_prop.single_fp_config,
												coproc->opencl_prop.double_fp_config,
												coproc->opencl_prop.endian_little,
												coproc->opencl_prop.execution_capabilities,
												coproc->opencl_prop.extensions,
												coproc->opencl_prop.global_mem_size,
												coproc->opencl_prop.local_mem_size,
												coproc->opencl_prop.max_clock_frequency,
												coproc->opencl_prop.max_compute_units
		);

		fprintf(fout,
			"									<opencl_platform_version>%s</opencl_platform_version>\n",
												coproc->opencl_prop.opencl_platform_version
		);


		fprintf(fout,
			"									<opencl_device_version>%s</opencl_device_version>\n",
												coproc->opencl_prop.opencl_device_version
		);

		fprintf(fout,
				"								<opencl_device_version_int>%d</opencl_device_version_int>\n",
												coproc->opencl_prop.opencl_device_version_int
		);

		fprintf(fout,
			"									<opencl_driver_version></opencl_driver_version>\n",
												coproc->opencl_prop.opencl_device_version
		);


		fprintf(fout,
				"								<device_num>%d</device_num>\n"
				"								<peak_flops>%f</peak_flops>\n"
				"								<is_used>%d</is_used>\n"
				"							</opencl_prop>\n"
				"						</coproc>\n",
												coproc->opencl_prop.device_num,
												coproc->opencl_prop.peak_flops,
												coproc->opencl_prop.is_used


		);

		fprintf(fout,
				"						<cuda_version>%d</cuda_version>\n"
				"						<display_driver_version>%d</display_driver_version>\n"
				"						<prop>\n"
				"							<name>%s</name>\n"
				"							<deviceHandle>%d</deviceHandle>\n"
				"							<totalGlobalMem>%d</totalGlobalMem>\n"
				"							<sharedMemPerBlock>%d</sharedMemPerBlock>\n"
				"							<regsPerBlock>%d</regsPerBlock>\n"
				"							<warpSize>%d</warpSize>\n"
				"							<memPitch>%d</memPitch>\n"
				"							<maxThreadsPerBlock>%d</maxThreadsPerBlock>\n"
				"							<maxThreadsDim>%d %d %d</maxThreadsDim>\n"
				"							<maxGridSize>%d %d %d</maxGridSize>\n"
				"							<clockRate>%d</clockRate>\n"
				"							<totalConstMem>%d</totalConstMem>\n"
				"							<major>%d</major>\n"
				"							<minor>%d</minor>\n"
				"							<textureAlignment>%d</textureAlignment>\n"
				"							<deviceOverlap>%d</deviceOverlap>\n"
				"							<multiProcessorCount>%d</multiProcessorCount>\n"
				"							<dtotalGlobalMem>%f</dtotalGlobalMem>\n"
				"						</prop>\n",
										coproc->cuda_version,
										coproc->display_driver_version,
											coproc->prop.name,
											coproc->prop.deviceHandle,
											coproc->prop.totalGlobalMem,
											coproc->prop.sharedMemPerBlock,
											coproc->prop.regsPerBlock,
											coproc->prop.warpSize,
											coproc->prop.memPitch,
											coproc->prop.maxThreadsPerBlock,
											coproc->prop.maxThreadsDim[0],
											coproc->prop.maxThreadsDim[1],
											coproc->prop.maxThreadsDim[2],
											coproc->prop.maxGridSize[0],
											coproc->prop.maxGridSize[1],
											coproc->prop.maxGridSize[2],
											coproc->prop.clockRate,
											coproc->prop.totalConstMem,
											coproc->prop.major,
											coproc->prop.minor,
											coproc->prop.textureAlignment,
											coproc->prop.deviceOverlap,
											coproc->prop.multiProcessorCount,
											coproc->prop.dtotalGlobalMem

		);

		if(request->coprocs.have_nvidia())
		{
		fprintf(fout,
				"						</coproc_cuda>\n"
		);
		}

		END_OF_COPROC_NVIDIA:

		//
		//
		// END OF NVIDIA COPROC

		COPROC_ATI* coproc_ati = &(request->coprocs.ati);

		if(coproc_ati == NULL) goto END_OF_COPROC_ATI;

		if(!request->coprocs.have_ati()) goto END_OF_COPROC_ATI;

		//
		//
		// ATI/AMD COPROC

		if(request->coprocs.have_ati())
		{
		fprintf(fout,
				"						<coproc_ati>\n"
		);
		}

		fprintf(fout,
				"							<type>%s</type>\n"
				"							<count>%d</count>\n"
				"							<peak_flops>%f</peak_flops>\n"
				"							<used>%f</used>\n"
				"							<have_cuda>%d</have_cuda>\n"
				"							<have_cal>%d</have_cal>\n"
				"							<have_opencl>%d</have_opencl>\n"
				"							<available_ram>%f</available_ram>\n"
				"							<specified_in_config>%d</specified_in_config>\n"
				"							<req_secs>%f</req_secs>\n"
				"							<req_instances>%f</req_instances>\n"
				"							<estimated_delay>%f</estimated_delay>\n"
				"							<usages>\n",
											coproc_ati->type,
											coproc_ati->count,
											coproc_ati->peak_flops,
											coproc_ati->used,
											coproc_ati->have_cuda ? 1 : 0,
											coproc_ati->have_cal ? 1 : 0,
											coproc_ati->have_opencl ? 1 : 0,
											coproc_ati->available_ram,
											coproc_ati->specified_in_config ? 1 : 0,
											coproc_ati->req_secs,
											coproc_ati->req_instances,
											coproc_ati->estimated_delay
		);

		for(int icoproc_usage = 0; icoproc_usage < coproc_ati->count; icoproc_usage++)
		{

		fprintf(fout,
				"								<usage>%f</usage>\n",
												coproc_ati->usage[icoproc_usage]
		);

		}	//end of coproc usage

		fprintf(fout,
				"							</usages>\n"
				"							<pending_usages>\n"
		);

		for(int icoproc_pending_usage = 0; icoproc_pending_usage < coproc_ati->count; icoproc_pending_usage++)
		{

		fprintf(fout,
				"								<pending_usage>%f</pending_usage>\n",
												coproc_ati->pending_usage[icoproc_pending_usage]
		);

		}	//end of coproc pending usage

		fprintf(fout,
				"							</pending_usages>\n"
				"							<device_nums>\n"
		);

		for(int icoproc_device_num = 0; icoproc_device_num < coproc_ati->count; icoproc_device_num++)
		{

		fprintf(fout,
				"								<device_num>%d</device_num>\n",
												coproc_ati->device_nums[icoproc_device_num]
		);

		}	//end of coproc device num



		fprintf(fout,
				"							</device_nums>\n"
				"							<opencl_device_ids>\n"
		);


		fprintf(fout,
				"							</opencl_device_ids>\n"
				"							<opencl_device_count>%d</opencl_device_count>\n"
				"							<running_graphics_apps>\n",
											coproc_ati->opencl_device_count
		);

		for(int icoproc_running_gfx_app = 0; icoproc_running_gfx_app < coproc_ati->count; icoproc_running_gfx_app++)
		{

		fprintf(fout,
				"								<running_graphics_app>%d</running_graphics_app>\n",
												coproc_ati->running_graphics_app[icoproc_running_gfx_app] ? 1 : 0

		);

		}		//end of coproc running graphics application

		fprintf(fout,
				"							</running_graphics_apps>\n"
				"							<available_ram_temps>\n"
		);

		fprintf(fout,
				"								<available_ram_temp>%f</available_ram_temp>\n",
												coproc_ati->available_ram
		);

		fprintf(fout,
				"							</available_ram_temps>\n"
				"							<last_print_time>%f</last_print_time>\n"
				"							<opencl_prop>\n"
				"								<device_id>%s</device_id>\n"
				"								<name>%s</name>\n"
				"								<vendor>%s</vendor>\n"
				"								<vendor_id>%d</vendor_id>\n"
				"								<available>%d</available>\n"
				"								<half_fp_config>%d</half_fp_config>\n"
				"								<single_fp_config>%d</single_fp_config>\n"
				"								<double_fp_config>%d</double_fp_config>\n"
				"								<endian_little>%d</endian_little>\n"
				"								<execution_capabilities>%d</execution_capabilities>\n"
				"								<extensions>%s</extensions>\n"
				"								<global_mem_size>%d</global_mem_size>\n"
				"								<local_mem_size>%d</local_mem_size>\n"
				"								<max_clock_frequency>%d</max_clock_frequency>\n"
				"								<max_compute_units>%d</max_compute_units>\n",
											coproc_ati->last_print_time,
												coproc_ati->opencl_prop.device_id,
												coproc_ati->opencl_prop.name,
												coproc_ati->opencl_prop.vendor,
												coproc_ati->opencl_prop.vendor_id,
												coproc_ati->opencl_prop.available,
												coproc_ati->opencl_prop.half_fp_config,
												coproc_ati->opencl_prop.single_fp_config,
												coproc_ati->opencl_prop.double_fp_config,
												coproc_ati->opencl_prop.endian_little,
												coproc_ati->opencl_prop.execution_capabilities,
												coproc_ati->opencl_prop.extensions,
												coproc_ati->opencl_prop.global_mem_size,
												coproc_ati->opencl_prop.local_mem_size,
												coproc_ati->opencl_prop.max_clock_frequency,
												coproc_ati->opencl_prop.max_compute_units
		);

		fprintf(fout,
			"									<opencl_platform_version>%s</opencl_platform_version>\n",
												coproc_ati->opencl_prop.opencl_platform_version
		);

		fprintf(fout,
			"									<opencl_device_version>%s</opencl_device_version>\n",
												coproc_ati->opencl_prop.opencl_device_version
		);

		fprintf(fout,
				"								<opencl_device_version_int>%d</opencl_device_version_int>\n",
												coproc_ati->opencl_prop.opencl_device_version_int
		);

		fprintf(fout,
			"									<opencl_driver_version>%s</opencl_driver_version>\n",
												coproc_ati->opencl_prop.opencl_device_version
		);

		fprintf(fout,
				"								<device_num>%d</device_num>\n"
				"								<peak_flops>%f</peak_flops>\n"
				"								<is_used>%d</is_used>\n"
				"							</opencl_prop>\n"
				"						</coproc>\n",
												coproc_ati->opencl_prop.device_num,
												coproc_ati->opencl_prop.peak_flops,
												coproc_ati->opencl_prop.is_used


		);

		fprintf(fout,
				"						<name>%s</name>\n"
				"						<version>%s</version>\n"
				"						<version_num>%d</version_num>\n"
				"						<atirt_detected>%d</atirt_detected>\n"
				"						<amdrt_detected>%d</amdrt_detected>\n"
				"						<attribs>\n"
				"							<struct_size>%d</struct_size>\n"
				"							<target>%d</target>\n"
				"							<localRAM>%d</localRAM>\n"
				"							<uncachedRemoteRAM>%d</uncachedRemoteRAM>\n"
				"							<cachedRemoteRAM>%d</cachedRemoteRAM>\n"
				"							<engineClock>%d</engineClock>\n"
				"							<memoryClock>%d</memoryClock>\n"
				"							<wavefrontSize>%d</wavefrontSize>\n"
				"							<numberOfSIMD>%d</numberOfSIMD>\n"
				"							<doublePrecision>%d</doublePrecision>\n"
				"							<localDataShare>%d</localDataShare>\n"
				"							<globalDataShare>%d</globalDataShare>\n"
				"							<globalGPR>%d</globalGPR>\n"
				"							<computeShader>%d</computeShader>\n"
				"							<memExport>%d</memExport>\n"
				"							<pitch_alignment>%d</pitch_alignment>\n"
				"							<surface_alignment>%d</surface_alignment>\n"
				"						</attribs>\n"
				"						<info>\n"
				"							<target>%d</target>\n"
				"							<maxResource1DWidth>%d</maxResource1DWidth>\n"
				"							<maxResource2DWidth>%d</maxResource2DWidth>\n"
				"							<maxResource2DHeight>%d</maxResource2DHeight>\n"
				"						</info>\n",
										coproc_ati->name,
										coproc_ati->version,
										coproc_ati->version_num,
										coproc_ati->atirt_detected ? 1 : 0,
										coproc_ati->amdrt_detected ? 1 : 0,
											coproc_ati->attribs.struct_size,
											coproc_ati->attribs.target,
											coproc_ati->attribs.localRAM,
											coproc_ati->attribs.uncachedRemoteRAM,
											coproc_ati->attribs.cachedRemoteRAM,
											coproc_ati->attribs.engineClock,
											coproc_ati->attribs.memoryClock,
											coproc_ati->attribs.wavefrontSize,
											coproc_ati->attribs.numberOfSIMD,
											coproc_ati->attribs.doublePrecision,
											coproc_ati->attribs.localDataShare,
											coproc_ati->attribs.globalDataShare,
											coproc_ati->attribs.globalGPR,
											coproc_ati->attribs.computeShader,
											coproc_ati->attribs.memExport,
											coproc_ati->attribs.pitch_alignment,
											coproc_ati->attribs.surface_alignment,
										coproc_ati->info.target,
										coproc_ati->info.maxResource1DWidth,
										coproc_ati->info.maxResource2DWidth,
										coproc_ati->info.maxResource2DHeight

		);

		if(request->coprocs.have_ati())
		{
		fprintf(fout,
				"						</coproc_ati>\n"
		);
		}

		END_OF_COPROC_ATI:

		if(request->coprocs.have_ati() || request->coprocs.have_nvidia())
		{
			fprintf(fout,
					"				</coprocs>\n"
			);

		}


		//
		//
		// END OF ATI/AMD COPROC
		//

		if(request->results.size())
		{
			fprintf(fout,
					"				<results>\n"
			);
		}

		for(int iresult = 0; iresult < request->results.size(); iresult++)
		{

			SCHED_DB_RESULT* db_result = &(request->results.at(iresult));

			if(db_result == NULL) continue;		//not likely to occur.

		fprintf(fout,
				"					<result>\n"
				"						<wu_name>%s</wu_name>\n"
				"						<units>%d</units>\n"
				"						<platform_name>%s</platform_name>\n",
										db_result->wu_name,
										db_result->units,
										db_result->platform_name
		);

		fprintf(fout,
				"						<best_app_version>\n"
				"							<appid>%d</appid>\n"
				"							<for_64b_jobs>%d</for_64b_jobs>\n"
				"							<present>%d</present>\n",
											db_result->bav.appid,
											db_result->bav.for_64b_jobs ? 1 : 0,
											db_result->bav.present ? 1 : 0
		);

		fprintf(fout,
				"							<client_app_version>\n"
				"								<app_name>%s</app_name>\n"
				"								<platform>%s</platform>\n"
				"								<version_num>%d</version_num>\n"
				"								<plan_class>%s</plan_class>\n"
				"								<host_usage>\n"
				"									<ncudas>%f</ncudas>\n"
				"									<natis>%f</natis>\n"
				"									<gpu_ram>%f</gpu_ram>\n"
				"									<avg_ncpus>%f</avg_ncpus>\n"
				"									<max_ncpus>%f</max_ncpus>\n"
				"									<projected_flops>%f</projected_flops>\n"
				"									<peak_flops>%f</peak_flops>\n"
				"									<cmdline>%s</cmdline>\n"
				"								</host_usage>\n"
				"							</client_app_version>\n",
												db_result->bav.cavp->app_name,
												db_result->bav.cavp->platform,
												db_result->bav.cavp->version_num,
												db_result->bav.cavp->plan_class,
												db_result->bav.cavp->host_usage.ncudas,
												db_result->bav.cavp->host_usage.natis,
												db_result->bav.cavp->host_usage.gpu_ram,
												db_result->bav.cavp->host_usage.avg_ncpus,
												db_result->bav.cavp->host_usage.max_ncpus,
												db_result->bav.cavp->host_usage.projected_flops,
												db_result->bav.cavp->host_usage.peak_flops,
												db_result->bav.cavp->host_usage.cmdline
		);

		fprintf(fout,

				"							<app_version>\n"
				"								<id>%d</id>\n"
				"								<create_time>%d</create_time>\n"
				"								<appid>%d</appid>\n"
				"								<version_num>%d</version_num>\n"
				"								<platformid>%d</platformid>\n"
				"								<xml_doc>%s</xml_doc>\n"
				"								<min_core_version>%d</min_core_version>\n"
				"								<max_core_version>%d</max_core_version>\n"
				"								<deprecated>%d</deprecated>\n"
				"								<plan_class>%s</plan_class>\n"
				"								<pfc>\n"
				"									<n>%f</n>\n"
				"									<avg>%f</avg>\n"
				"								</pfc>\n"
				"								<pfc_scale>%f</pfc_scale>\n"
				"								<expavg_credit>%f</expavg_credit>\n"
				"								<expavg_credit>%f</expavg_time>\n"
				"							</app_version>\n",
												db_result->bav.avp->id,
												db_result->bav.avp->create_time,
												db_result->bav.avp->version_num,
												db_result->bav.avp->platformid,
												db_result->bav.avp->xml_doc,
												db_result->bav.avp->min_core_version,
												db_result->bav.avp->max_core_version,
												db_result->bav.avp->deprecated ? 1 : 0,
												db_result->bav.avp->plan_class,
												db_result->bav.avp->pfc.n,
												db_result->bav.avp->pfc.avg,
												db_result->bav.avp->pfc_scale,
												db_result->bav.avp->expavg_credit,
												db_result->bav.avp->expavg_credit
		);

		fprintf(fout,

				"							<host_usage>\n"
				"								<ncudas>%f</ncudas>\n"
				"								<natis>%f</natis>\n"
				"								<gpu_ram>%f</gpu_ram>\n"
				"								<avg_ncpus>%f</avg_ncpus>\n"
				"								<max_ncpus>%f</max_ncpus>\n"
				"								<projected_flops>%f</projected_flops>\n"
				"								<peak_flops>%f</peak_flops>\n"
				"								<cmdline>%f</cmdline>\n"
				"							</host_usage>\n"
				"							<reliable>%d</reliable>\n"
				"							<trusted>%d</trusted>\n"
				"						</best_app_version>\n"
				"					</result>\n",
												db_result->bav.host_usage.ncudas,
												db_result->bav.host_usage.natis,
												db_result->bav.host_usage.gpu_ram,
												db_result->bav.host_usage.avg_ncpus,
												db_result->bav.host_usage.max_ncpus,
												db_result->bav.host_usage.projected_flops,
												db_result->bav.host_usage.peak_flops,
												db_result->bav.host_usage.cmdline,
											db_result->bav.reliable ? 1 : 0,
											db_result->bav.trusted ? 1 : 0
		);

		}	// end of db result

		if(request->results.size())
		{
		fprintf(fout,
				"				</results>\n"
		);
		}

		if(request->file_xfer_results.size())
		{
		fprintf(fout,

				"				<file_xfer_results>\n"
		);
		}

		for(int icompleted_results = 0; icompleted_results < request->file_xfer_results.size(); icompleted_results++)
		{
			RESULT* res = &(request->file_xfer_results.at(icompleted_results));

			if(res == NULL ) continue;

		fprintf(fout,
				"					<file_xfer_result>\n"
				"						<id>%d</id>\n"
				"						<create_time>%d</create_time>\n"
				"						<workunitid>%d</workunitid>\n"
				"						<server_state>%d</server_state>\n"
				"						<outcome>%d</outcome>\n"
				"						<client_state>%d</client_state>\n"
				"						<hostid>%d</hostid>\n"
				"						<userid>%d</userid>\n"
				"						<report_deadline>%d</report_deadline>\n"
				"						<sent_time>%d</sent_time>\n"
				"						<received_time>%d</received_time>\n"
				"						<name>%s</name>\n"
				"						<cpu_time>%f</cpu_time>\n"
				"						<xml_doc_in>%s</xml_doc_in>\n"
				"						<xml_doc_out>%s</xml_doc_out>\n"
				"						<stderr_out>%s</stderr_out>\n"
				"						<batch>%d</batch>\n"
				"						<file_delete_state>%d</file_delete_state>\n"
				"						<validate_state>%d</validate_state>\n"
				"						<claimed_credit>%f</claimed_credit>\n"
				"						<granted_credit>%f</granted_credit>\n"
				"						<opaque>%f</opaque>\n"
				"						<random>%d</random>\n"
				"						<app_version_num>%d</app_version_num>\n"
				"						<appid>%d</appid>\n"
				"						<exit_status>%d</exit_status>\n"
				"						<teamid>%d</teamid>\n"
				"						<priority>%d</priority>\n"
				"						<mod_time>%s</mod_time>\n"
				"						<elapsed_time>%f</elapsed_time>\n"
				"						<flops_estimate>%f</flops_estimate>\n"
				"						<app_version_id>%d</app_version_id>\n"
				"						<runtime_outlier>%d</runtime_outlier>\n"
				"					</file_xfer_result>\n",
										res->id,
										res->create_time,
										res->workunitid,
										res->server_state,
										res->outcome,
										res->client_state,
										res->hostid,
										res->userid,
										res->report_deadline,
										res->sent_time,
										res->received_time,
										res->name,
										res->cpu_time,
										res->xml_doc_in,
										res->xml_doc_out,
										res->stderr_out,
										res->batch,
										res->file_delete_state,
										res->validate_state,
										res->claimed_credit,
										res->granted_credit,
										res->opaque,
										res->random,
										res->app_version_num,
										res->appid,
										res->exit_status,
										res->teamid,
										res->priority,
										res->mod_time,
										res->elapsed_time,
										res->flops_estimate,
										res->app_version_id,
										res->runtime_outlier ? 1 : 0
		);

		}	//end of result

		if(request->file_xfer_results.size())
		{
		fprintf(fout,
				"				</file_xfer_results>\n"
		);
		}

		if(request->msgs_from_host.size())
		{
		fprintf(fout,
				"				<msgs_from_hosts>\n"
		);
		}

		for(int imsg_from_host = 0; imsg_from_host < request->msgs_from_host.size(); imsg_from_host++)
		{

		fprintf(fout,
				"					<msg_from_host>\n"
				"						<variety>%s</variety>\n"
				"						<msg_text>%s</msg_text>\n"
				"					</msg_from_host>\n",
									request->msgs_from_host.at(imsg_from_host).variety,
									request->msgs_from_host.at(imsg_from_host).msg_text.c_str()


		);

		}		//end of message to and from host


		if(request->msgs_from_host.size())
		{
		fprintf(fout,
				"				</msgs_from_hosts>\n"
		);
		}

		if(request->file_infos.size())
		{
		fprintf(fout,
				"				<file_infos>\n"
		);
		}

		for(int ifile_info = 0; ifile_info < request->file_infos.size(); ifile_info++)
		{

		fprintf(fout,
				"					<file_info>\n"
				"						<name>%s</name>\n"
				"					</file_info>\n",
										request->file_infos.at(ifile_info).name
		);

		}	//end of file info

		if(request->file_infos.size())
		{
		fprintf(fout,
				"				</file_infos>\n"
		);
		}

		if(request->file_delete_candidates.size())
		{
	    fprintf(fout,
				"				<file_delete_candidates>\n"
		);
		}

		for(int idelete_cadidate = 0; idelete_cadidate < request->file_delete_candidates.size(); idelete_cadidate++)
		{

		fprintf(fout,
				"					<file_delete_candidate>\n"
				"						<name>%s</name>\n"
				"					</file_delete_candidate>\n",
										request->file_delete_candidates.at(idelete_cadidate).name
		);

		}	//end of file delete candidate

		if(request->file_delete_candidates.size())
		{
		fprintf(fout,
				"				</file_delete_candidates>\n"
		);
		}

		if(request->files_not_needed.size())
		{
	    fprintf(fout,
				"				<files_not_needed>\n"
		);
		}

		for(int ifile_not_needed = 0; ifile_not_needed < request->files_not_needed.size(); ifile_not_needed++)
		{

		fprintf(fout,
				"					<file_not_needed>\n"
				"						<name>%s</name>\n"
				"					</file_not_needed>\n",
										request->files_not_needed.at(ifile_not_needed).name
		);

		}	//end of file note needed

		if(request->files_not_needed.size())
		{
		fprintf(fout,
				"				</files_not_needed>\n"
		);
		}

		if(request->other_results.size())
		{
		fprintf(fout,
				"				<other_results>\n"
		);
		}

		for(int iother_result = 0; iother_result < request->other_results.size(); iother_result++)
		{

		fprintf(fout,
				"					<other_result>\n"
				"						<name>%s</name>\n"
				"						<app_version>%d</app_version>\n"
				"						<plan_class>%s</plan_class>\n"
				"						<have_plan_class>%d</have_plan_class>\n"
				"						<abort>%d</abort>\n"
				"						<abort_if_not_started>%d</abort_if_not_started>\n"
				"						<reason>%d</reason>\n"
				"					</other_result>\n",
										request->other_results.at(iother_result).name,
										request->other_results.at(iother_result).app_version,
										request->other_results.at(iother_result).plan_class,
										request->other_results.at(iother_result).have_plan_class ? 1 : 0,
										request->other_results.at(iother_result).abort ? 1 : 0,
										request->other_results.at(iother_result).abort_if_not_started ? 1 : 0,
										request->other_results.at(iother_result).reason
		);

		}	//end of other results


		if(request->other_results.size())
		{
		fprintf(fout,
				"				</other_results>\n"

	    );
		}


		if(request->ip_results.size())
		{
		fprintf(fout,
				"				<in_progress_results>\n"
		);
		}

		for(int iip_result = 0; iip_result < request->ip_results.size(); iip_result++)
		{

		fprintf(fout,
				"					<ip_result>\n"
				"						<name>%s</name>\n"
				"						<computation_deadline>%f</computation_deadline>\n"
				"						<report_deadline>%f</report_deadline>\n"
				"						<cpu_time_remaining>%f</cpu_time_remaining>\n"
				"						<misses_deadline>%d</misses_deadline>\n"
				"						<estimated_completion_time>%f</estimated_completion_time>\n"
				"					</ip_result>\n",
										request->ip_results.at(iip_result).name,
										request->ip_results.at(iip_result).computation_deadline,
										request->ip_results.at(iip_result).report_deadline,
										request->ip_results.at(iip_result).cpu_time_remaining,
										request->ip_results.at(iip_result).misses_deadline ? 1 : 0,
										request->ip_results.at(iip_result).estimated_completion_time
		);

		}

		if(request->ip_results.size())
		{
		fprintf(fout,
				"				</in_progress_results>\n"
		);
		}

		fprintf(fout,
				"				<have_other_results_list>%d</have_other_results_list>\n"
				"				<have_ip_results_list>%d</have_ip_results_list>\n"
				"				<have_time_stats_log>%d</have_time_stats_log>\n"
				"				<client_cap_plan_class>%d</client_cap_plan_class>\n"
				"				<sandbox>%d</sandbox>\n"
				"				<allow_multiple_clients>%d</allow_multiple_clients>\n"
				"				<using_weak_auth>%d</using_weak_auth>\n"
				"				<last_rpc_dayofyear>%d</last_rpc_dayofyear>\n"
				"				<current_rpc_dayofyear>%d</current_rpc_dayofyear>\n"
				"				<client_opaque>%s</client_opaque>\n",
								request->have_other_results_list ? 1 : 0,
								request->have_ip_results_list ? 1 : 0,
								request->have_time_stats_log ? 1 : 0,
								request->client_cap_plan_class ? 1 : 0,
								request->sandbox,
								request->allow_multiple_clients,
								request->using_weak_auth ? 1 : 0,
								request->last_rpc_dayofyear,
								request->current_rpc_dayofyear,
								request->client_opaque.c_str()



		);


		if(request->host_availability_records.size())
		{
			fprintf(fout,
					"				<host_availability>\n"
			);

		}

		for(int irecord = 0; irecord < request->host_availability_records.size(); irecord++)
		{

			AvailabilityRecord* ar = &(request->host_availability_records.at(irecord));

			if(ar == NULL) continue;	//not likely

		fprintf(fout,
				"					<record>\n"
				"						<time>\n"
				"							<hour>%d</hour>\n"
				"							<minute>%d</minute>\n"
				"						</time>\n"
				"						<date>\n"
				"							<year>%d</year>\n"
				"							<month>%d</month>\n"
				"							<day>%d</day>\n"
				"						</date>\n"
				"						<availability></availability>\n"
				"					</record>\n",
											ar->hour,
											ar->minutes,
											ar->year,
											ar->month,
											ar->day

		);

		}	//end of availability records

		if(request->host_availability_records.size() > 0)
		{
		fprintf(fout,
				"				</host_availability>\n"
		);
		}

		fprintf(fout,
				"			</scheduler_request>\n"
		);

	}	//end of request

	*/

	fprintf(fout,
			"		</requests>\n"
			"	</clients>\n"
			"</ext_sched_data>\n"
	);

    copy_stream(fout, stdout);

    log_messages.printf(MSG_NORMAL, "Dumped XS data\n");

    fclose(fout);
}

