## $Id: version.py.in 3856 2004-07-13 10:36:18Z quarl $

# define version numbers using autoconf
BOINC_MAJOR_VERSION = 7
BOINC_MINOR_VERSION = 1
BOINC_VERSION_STRING = "7.1.0"
CLIENT_BIN_FILENAME = "boinc"
PLATFORM = "x86_64-unknown-linux-gnu"
