Thermal Aware Grid (TAG)

TAG project seeks to create a grid framework application to enable pluggable/dynamic green and non-green scheduling
in work distribution at data centers.
TAG is a fork of the renowned BOINC project, from Berkely. It has been modified for 2 purposes:

1. To work with third party scheduling applications, which can be dynamically "deployed" and configured to run
without a server restart
2. To work with sensor based information (temperature at the momenent), and provide this data to third party
scheduling applications to use and evaluate for work assignment.

Guilherme Dinis Jr.
High Performance Computing Services Center
Universiti Teknologi PETRONAS
2014