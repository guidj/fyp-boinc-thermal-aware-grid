"""
This file logs any changes made to source files using Git.
Project "Thermal-Aware Grid" (TAG)

Guilherme Dinis Jr.
High Performance Computing Services Center
Universiti Teknologi PETRONAS
"""

import subprocess
import datetime

cmddiff = "git diff"
date = datetime.datetime.now()
logFileName = "changes/diff_" + str(date.year) + str(date.month) + str(date.day) + "_" + str(date.hour) + str(date.minute) + str(date.second)

f = open(logFileName, 'w')

print "Running command: ", cmddiff

p = subprocess.Popen(cmddiff, shell=True, stdout=f, stderr=subprocess.PIPE)

while True:
    out = p.stderr.read(1)
    if out == '' and p.poll() != None:
        break
    if out != '':
        sys.stdout.write(out)
        sys.stdout.flush()

print "Saving log to '",logFileName,"'\n"
print "Done."
        
f.close()

